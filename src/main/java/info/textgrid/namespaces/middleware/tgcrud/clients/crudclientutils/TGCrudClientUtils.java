/**
 * This software is copyright (c) 2024 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 * 
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (stefan.e.funk@daasi.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.clients.crudclientutils;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.DigestOutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;
import javax.xml.namespace.QName;
import org.apache.commons.codec.binary.Hex;
import org.apache.commons.io.output.CountingOutputStream;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.w3._1999._02._22_rdf_syntax_ns_.RdfType;
import info.textgrid.clients.SearchClient;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.middleware.tgauth.tgauthclient.TGAuthClientCrudUtilities;
import info.textgrid.middleware.tgauth.tgauthclient.TGAuthClientUtilities;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Fixity;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.ObjectType;
import info.textgrid.namespaces.metadata.core._2010.ProvidedType;
import info.textgrid.namespaces.metadata.core._2010.RelationType;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth_crud.PortTgextraCrud;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TextGridFaultType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.namespaces.middleware.tgsearch.ResultType;
import info.textgrid.utils.sesameclient.SesameClient;
import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.ws.rs.client.Entity;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.HttpHeaders;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.ws.Holder;
import junit.framework.AssertionFailedError;

/**
 * TODOLOG
 * 
 **
 * CHANGELOG
 *
 * 2024-04-04 - Funk - Add some more tests for new metadata warning and error validation messages.
 * 
 * 2022-01-14 - Funk - Add PRINT_DATA to REST reading method.
 * 
 * 2021-11-23 - Funk - Remove eXist methods.
 * 
 * 2021-06-21 - Funk - Add test for new HTTP filename headers (content-disposition).
 * 
 * 2020-03-09 - Funk - Reformat code.
 *
 * 2018-01-12 - Funk - Added readTechMD method.
 * 
 * 2017-12-20 - Funk - Added MD5 check.
 * 
 * 2012-09-19 - Funk - Added tgextra endpoint stub generation issues.
 * 
 * 2012-07-26 - Funk - Added RESTful test methods.
 * 
 * 2012-02-07 - Funk - Generating TG-auth stubs now from endpoint.
 * 
 * 2012-01-18 - Funk - Moved to utils package
 * 
 * 2010-10-26 - Funk - First version.
 *
 */

/**
 * <p>
 * Utilities for testing some of the TG services.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2024-04-04
 * @since 2009-08-26
 */

public class TGCrudClientUtils {

  // **
  // STATIC FINALS
  // **

  private static final String NO_URI = null;
  private static final String NO_DATA_FILE = null;
  // private static final URI NO_URI_URI = null;
  private static final boolean NO_REVISION = false;
  private static final String REVISION_SEPARATION_CHAR = ".";
  // private static final String MD5_OF_EMPTY_FILE = "d41d8cd98f00b204e9800998ecf8427e";
  private static final String CHANGE_ACCEPT_STRING = "tg.aggregation";
  private static final String LINE =
      "\t\t------------------------------------------------------------";
  private static final String X_CLACKS_NAME = "X-Clacks-Overhead";
  private static final String X_CLACKS_VALUE = "GNU John Dearheart";
  private static final String PARAM_SID = "sessionId";
  private static final String PARAM_LOG = "logParameter";
  private static final String PARAM_PROJECT = "projectId";

  // **
  // STATIC
  // **

  private static boolean ignoreWarnings = false;

  /**
   * <p>
   * Calls TG-crud#GETVERSION.
   * </p>
   */
  public static String getVersion(TGCrudService tgcrud) {

    System.out.println("\tCalling #GETVERSION");

    // Start #GETVERSION operation and return version string.
    return tgcrud.getVersion();
  }

  /**
   * <p>
   * Calls TG-crud#GETURI.
   * </p>
   * 
   * @param tgcrud
   * @param theSessionId
   * @param theProjectId
   * @param howMany
   * @return
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   */
  public static List<String> getUri(TGCrudService tgcrud, String theSessionId,
      String theLogParameter, String theProjectId, int howMany)
      throws IOException, ObjectNotFoundFault, IoFault, AuthFault {

    System.out.println("\tCalling #GETURI");

    // Start #GETURI operation.
    List<String> uris;
    try {
      uris = tgcrud.getUri(theSessionId, theLogParameter, "", howMany);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    System.out.println("\t\tURIs created: " + uris);

    return uris;
  }

  /**
   * <p>
   * Calls TG-crud#CREATE with NO URI.
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param theProjectId
   * @param metadataFilename
   * @param dataFilename
   * @param expectedWarnings
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  public static MetadataContainerType create(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String theProjectId, String metadataFilename,
      String dataFilename, int expectedWarnings) throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault {
    return create(tgcrud, tgsearch, theSessionId, theLogParameter, theProjectId, metadataFilename,
        dataFilename, NO_URI, NO_URI, NO_URI, NO_URI, NO_URI, NO_REVISION, expectedWarnings);
  }

  /**
   * <p>
   * Calls TG-crud#CREATE with just one URI (for creation).
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param theProjectId
   * @param metadataFilename
   * @param dataFilename
   * @param theUri
   * @param expectedWarnings
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  public static MetadataContainerType create(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String theProjectId, String metadataFilename,
      String dataFilename, String theUri, int expectedWarnings)
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault {
    return create(tgcrud, tgsearch, theSessionId, theLogParameter, theProjectId, metadataFilename,
        dataFilename, theUri, NO_URI, NO_URI, NO_URI, NO_URI, NO_REVISION, expectedWarnings);
  }

  /**
   * <p>
   * Calls TG-crud#CREATE with NO URI and createRevision flag.
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param theProjectId
   * @param metadataFilename
   * @param dataFilename
   * @param createRevision
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  public static MetadataContainerType create(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String theProjectId, String metadataFilename,
      String dataFilename, boolean createRevision, int expectedWarnings)
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault {
    return create(tgcrud, tgsearch, theSessionId, theLogParameter, theProjectId, metadataFilename,
        dataFilename, NO_URI, NO_URI, NO_URI, NO_URI, NO_URI, createRevision, expectedWarnings);
  }

  /**
   * <p>
   * Calls TG-crud#CREATE with URI and createRevision flag.
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param theProjectId
   * @param metadataFilename
   * @param dataFilename
   * @param theUri
   * @param createRevision
   * @param expectedWarnings
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  public static MetadataContainerType create(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String theProjectId, String metadataFilename,
      String dataFilename, String theUri, boolean createRevision, int expectedWarnings)
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault {
    return create(tgcrud, tgsearch, theSessionId, theLogParameter, theProjectId, metadataFilename,
        dataFilename, theUri, NO_URI, NO_URI, NO_URI, NO_URI, createRevision, expectedWarnings);
  }

  /**
   * <p>
   * Calls TG-crud#CREATE with no revision and things.
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param theProjectId
   * @param metadataFilename
   * @param dataFilename
   * @param theUri
   * @param adaptorUri
   * @param xsdUri
   * @param isDerivedUri
   * @param workUri
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  public static MetadataContainerType create(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String theProjectId, String metadataFilename,
      String dataFilename, String theUri, String adaptorUri, String xsdUri, String isDerivedUri,
      String workUri, int expectedWarnings) throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault {
    return create(tgcrud, tgsearch, theSessionId, theLogParameter, theProjectId, metadataFilename,
        dataFilename, theUri, adaptorUri, xsdUri, isDerivedUri, workUri, NO_REVISION,
        expectedWarnings);
  }

  /**
   * <p>
   * Calls TG-crud#CREATE with URIs for creation, adaptor, xsd reference, and createRevision flag.
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param theProjectId
   * @param metadataFilename
   * @param dataFilename
   * @param theUri
   * @param adaptorUri
   * @param xsdUri
   * @param isDerivedUri
   * @param workUri
   * @param createRevision
   * @param expectedWarnings
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  public static MetadataContainerType create(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String theProjectId, String metadataFilename,
      String dataFilename, String theUri, String adaptorUri, String xsdUri, String isDerivedUri,
      String workUri, boolean createRevision, int expectedWarnings)
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault {

    System.out.println("\tCalling #CREATE");
    System.out.println("\t\tdata file:    " + dataFilename);
    System.out.println("\t\tmeta file:    " + metadataFilename);
    System.out.println("\t\tproject:      " + theProjectId);
    System.out.println(LINE);

    // Create metadata from file.
    MetadataContainerType metadata = getMetadataFromFile(metadataFilename);
    // JAXB.marshal(metadata, System.out);

    // Add the XSD URI, adaptor URI, version URIs, and URI of the work, if
    // existing.
    if (adaptorUri != null && !adaptorUri.equals("")) {
      RelationType rel = new RelationType();
      rel.setHasAdaptor(adaptorUri);
      metadata.getObject().setRelations(rel);
    }
    if (xsdUri != null && !xsdUri.equals("")) {
      RelationType rel = new RelationType();
      rel.setHasSchema(xsdUri);
      metadata.getObject().setRelations(rel);
    }
    if (isDerivedUri != null && !isDerivedUri.equals("")) {
      RelationType rel = new RelationType();
      rel.setIsDerivedFrom(isDerivedUri);
      metadata.getObject().setRelations(rel);
    }
    if (workUri != null && !workUri.equals("")) {
      metadata.getObject().getEdition().setIsEditionOf(workUri);
    }

    // Create data from file.
    DataHandler data =
        new DataHandler(new FileDataSource(TGCrudClientUtils.getResource(dataFilename)));

    // Create holder and set metadata.
    Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>(metadata);

    // Start #CREATE operation.
    try {
      tgcrud.create(theSessionId, theLogParameter, theUri, createRevision, theProjectId,
          metadataHolder, data);
    } catch (MetadataParseFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    TGCrudClientUtils.printMetadata(metadataHolder.value.getObject());


    JAXB.marshal(metadata, System.out);


    // Check returned metadata.
    if (TGCrudClientUtils.metadataWarnings(metadataHolder.value, expectedWarnings)) {
      throw new AssertionFailedError();
    }

    // Check revision numbers using tgsearch.
    checkRevisionStatusCreate(metadataHolder.value, tgsearch);

    return metadataHolder.value;
  }

  /**
   * <p>
   * Calls TG-crud#CREATE RESTful.
   * </p>
   * 
   * @param theClient
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param theProjectId
   * @param metadataFilename
   * @param dataFilename
   * @param expectedStatusCode
   * @param expectedWarnings
   * @return
   * @throws IOException
   */
  public static MetadataContainerType createREST(WebTarget theClient, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String theProjectId, String metadataFilename,
      String dataFilename, int expectedStatusCode, int expectedWarnings) throws IOException {

    System.out.println("\tCalling #CREATE RESTful");
    System.out.println("\t\tdata file:    " + dataFilename);
    System.out.println("\t\tmeta file:    " + metadataFilename);
    System.out.println(LINE);

    // Create an HTTP entity.
    Entity<List<Attachment>> entity =
        createMultipartEntityFromFilename(metadataFilename, dataFilename);

    // Assemble and submit request.
    WebTarget target = theClient.path("create").queryParam(PARAM_SID, theSessionId)
        .queryParam(PARAM_LOG, theLogParameter).queryParam(PARAM_PROJECT, theProjectId);

    System.out.println("\t\thttp POST:    " + target.getUri());

    Response response = target.request().post(entity);

    int statusCode = response.getStatus();
    String reasonPhrase = response.getStatusInfo().getReasonPhrase();
    InputStream content = response.readEntity(InputStream.class);

    System.out.println("\t\tstatus code:  " + statusCode + " " + reasonPhrase);
    System.out.println(LINE);

    // Check response and get metadata.
    assertFalse(content == null);
    if (statusCode != Status.OK.getStatusCode()) {
      String contentString = IOUtils.readStringFromStream(content);
      assertFalse("ERROR HTML must NOT be null or empty!",
          contentString == null || contentString.isEmpty());
      System.out.println("\t\tERROR HTML:");
      System.out.println(contentString);
      assertFalse(statusCode != expectedStatusCode);
      return null;
    } else {
      assertFalse(statusCode != expectedStatusCode);
    }

    MetadataContainerType metadata = TGCrudClientUtils.getTextgridMetadata(content);

    // Check expected headers and print metadata.
    checkFilenameAndMimetypeHeader(response, metadata, MediaType.TEXT_XML);
    TGCrudClientUtils.printMetadata(metadata.getObject());

    // Check returned metadata.
    if (TGCrudClientUtils.metadataWarnings(metadata, expectedWarnings)) {
      throw new AssertionFailedError();
    }

    // Check revision numbers using tgsearch.
    checkRevisionStatusCreate(metadata, tgsearch);

    return metadata;
  }

  /**
   * <p>
   * Calls TG-crud#READMETADATA.
   * </p>
   * 
   * @param tgcrud
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @return
   * @throws IOException
   * @throws AuthFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   */
  public static MetadataContainerType readMetadata(TGCrudService tgcrud, String theSessionId,
      String theLogParameter, String theUri)
      throws IOException, AuthFault, IoFault, MetadataParseFault, ObjectNotFoundFault {

    System.out.println("\tCalling #READMETADATA");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Start #READMETADATA operation.
    MetadataContainerType metadata;
    try {
      metadata = tgcrud.readMetadata(theSessionId, theLogParameter, theUri);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (MetadataParseFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    TGCrudClientUtils.printMetadata(metadata.getObject());

    return metadata;
  }

  /**
   * <p>
   * Calls TG-crud#READMETADATA RESTful.
   * </p>
   * 
   * @param theClient
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @return
   * @throws IOException
   */
  public static MetadataContainerType readMetadataREST(WebTarget theClient, String theSessionId,
      String theLogParameter, String theUri) throws IOException {

    System.out.println("\tCalling #READMETADATA RESTful");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Assemble and submit request.
    String path = theUri + "/metadata";
    WebTarget target = theClient.path(path).queryParam(PARAM_SID, theSessionId)
        .queryParam(PARAM_LOG, theLogParameter);

    System.out.println("\t\thttp GET:     " + target.getUri());

    Response response = target.request().get();
    int statusCode = response.getStatus();
    InputStream content = response.readEntity(InputStream.class);

    System.out.println("\t\tstatus code:  " + statusCode);
    System.out.println(LINE);

    // Get and check metadata.
    MetadataContainerType metadata = TGCrudClientUtils.getTextgridMetadata(content);
    if (metadata == null || statusCode != Status.OK.getStatusCode()) {
      assertTrue(false);
    }

    // Check expected headers and print metadata.
    checkFilenameAndMimetypeHeader(response, metadata, MediaType.TEXT_XML);
    TGCrudClientUtils.printMetadata(metadata.getObject());

    return metadata;
  }

  /**
   * <p>
   * Calls TG-crud#READTECHMD (RESTful only).
   * </p>
   * 
   * @param theClient
   * @param theUri
   * @return
   * @throws IOException
   * @throws FileNotFoundException
   */
  public static String readTechmdREST(WebTarget theClient, String theUri)
      throws IOException, FileNotFoundException {

    System.out.println("\tCalling #READTECHMD RESTful");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Assemble and submit request.
    String path = theUri + "/tech";
    WebTarget target = theClient.path(path);

    System.out.println("\t\thttp GET:     " + target.getUri());

    Response response = target.request().get();
    int statusCode = response.getStatus();
    InputStream content = response.readEntity(InputStream.class);

    System.out.println("\t\tstatus code:  " + statusCode);
    System.out.println(LINE);

    // Get and check TECHMD.
    String techmd = IOUtils.readStringFromStream(content);
    if (techmd == null || statusCode != Status.OK.getStatusCode()) {
      if (statusCode == Status.NOT_FOUND.getStatusCode()) {
        throw new FileNotFoundException("TECHMD file not found: " + theUri);
      } else {
        assertTrue("TECHMD file retrieval error: " + statusCode, false);
      }
    }

    System.out.println("\t\tTECHMD:       " + techmd.trim());
    System.out.println(LINE);

    return techmd;
  }

  /**
   * <p>
   * Calls TG-crud#READ.
   * </p>
   * 
   * @param tgcrud
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @return
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ProtocolNotImplementedFault
   * @throws AuthFault
   * @throws NoSuchAlgorithmException
   */
  public static void read(TGCrudService tgcrud, String theSessionId, String theLogParameter,
      String theUri) throws IOException, ObjectNotFoundFault, MetadataParseFault, IoFault,
      ProtocolNotImplementedFault, AuthFault, NoSuchAlgorithmException {

    System.out.println("\tCalling #READ");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Create holders.
    Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
    Holder<DataHandler> dataHolder = new Holder<DataHandler>();

    // Start #READ operation.
    try {
      tgcrud.read(theSessionId, theLogParameter, theUri, metadataHolder, dataHolder);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (MetadataParseFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (ProtocolNotImplementedFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    TGCrudClientUtils.printMetadata(metadataHolder.value.getObject());

    // Compare the size of the returned data with the size given in the metadata returned, and the
    // digest, too.
    checkSizeAndDigest(dataHolder.value, metadataHolder.value);
  }

  /**
   * @param tgcrud
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @return
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ProtocolNotImplementedFault
   * @throws AuthFault
   */
  public static DataHandler simpleRead(TGCrudService tgcrud, String theSessionId,
      String theLogParameter, String theUri) throws IOException, ObjectNotFoundFault,
      MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault {

    // Create holders.
    Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
    Holder<DataHandler> dataHolder = new Holder<DataHandler>();

    // Start #READ operation.
    try {
      tgcrud.read(theSessionId, theLogParameter, theUri, metadataHolder, dataHolder);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (MetadataParseFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (ProtocolNotImplementedFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    return dataHolder.value;
  }

  /**
   * <p>
   * Calls TG-crud#READ RESTful.
   * </p>
   * 
   * @param theClient
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @param expectedStatusCode
   * @param printData
   * @throws IOException
   */
  public static void readREST(WebTarget theClient, String theSessionId, String theLogParameter,
      String theUri, int expectedStatusCode, boolean printData) throws IOException {

    System.out.println("\tCalling #READ RESTful");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Assemble and submit request.
    String path = theUri + "/data";
    WebTarget target = theClient.path(path).queryParam(PARAM_SID, theSessionId)
        .queryParam(PARAM_LOG, theLogParameter);

    System.out.println("\t\thttp GET:     " + target.getUri());

    Response response = target.request().get();
    int statusCode = response.getStatus();
    InputStream content = response.readEntity(InputStream.class);

    System.out.println("\t\tstatus code:  " + statusCode);
    System.out.println(LINE);

    if (printData) {
      System.out.println(org.apache.cxf.helpers.IOUtils.readStringFromStream(content));
      System.out.println(LINE);
    }

    // Check status code.
    if (statusCode != expectedStatusCode) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Just updates metadata without any xsdUri or adaptorUri.
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param metadataFilename
   * @param theGenerated
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws MetadataParseFault
   * @throws UpdateConflictFault
   * @throws AuthFault
   * @throws IoFault
   */
  public static MetadataContainerType updateMetadata(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String metadataFilename,
      GeneratedType theGenerated) throws IOException, ObjectNotFoundFault, MetadataParseFault,
      UpdateConflictFault, AuthFault, IoFault {
    return updateMetadata(tgcrud, tgsearch, theSessionId, theLogParameter, metadataFilename, "", "",
        theGenerated);
  }

  /**
   * <p>
   * Calls TG-crud#UPDATEMETADATA. All updated metadata is tested for the title metadata to start
   * with "UPDATE:"!
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param metadataFilename
   * @param adaptorUri
   * @param xsdUri
   * @param theGenerated
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws MetadataParseFault
   * @throws UpdateConflictFault
   * @throws AuthFault
   * @throws IoFault
   */
  public static MetadataContainerType updateMetadata(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String metadataFilename, String adaptorUri,
      String xsdUri, GeneratedType theGenerated) throws IOException, ObjectNotFoundFault,
      MetadataParseFault, UpdateConflictFault, AuthFault, IoFault {

    System.out.println("\tCalling #UPDATEMETADATA");
    System.out.println("\t\tmeta file:    " + metadataFilename);
    System.out.println("\t\turi:          " + theGenerated.getTextgridUri().getValue());
    System.out.println(LINE);

    // Create metadata from file.
    MetadataContainerType metadata = getMetadataFromFile(metadataFilename);

    // Add the given generated type.
    metadata.getObject().getGeneric().setGenerated(theGenerated);

    // Add the XSD and adaptor URIs if existing.
    if (adaptorUri != null && !adaptorUri.equals("")) {
      metadata.getObject().getRelations().setHasAdaptor(adaptorUri);
    }
    if (xsdUri != null && !xsdUri.equals("")) {
      metadata.getObject().getRelations().setHasSchema(xsdUri);
    }

    // Create holder and set metadata.
    Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>(metadata);

    // Check tgsearch revision list before update.
    List<BigInteger> revisionsBeforeUpdate = checkTgsearchRevisions(
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue(), tgsearch);

    // Start #UPDATEMETADATA operation.
    try {
      tgcrud.updateMetadata(theSessionId, theLogParameter, metadataHolder);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (MetadataParseFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (UpdateConflictFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    // Check if the updated title and subtitle is existing in the metadata file.
    String startsWith = "UPDATEMETADATA:";
    if (!metadataHolder.value.getObject().getGeneric().getProvided().getTitle().get(0)
        .startsWith(startsWith)) {
      System.out.println("Title doesn't start with '" + startsWith + "', it starts with '"
          + metadataHolder.value.getObject().getGeneric().getProvided().getTitle().get(0)
          + "' instead!");
      throw new AssertionFailedError();
    }

    TGCrudClientUtils.printMetadata(metadataHolder.value.getObject());

    // Check tgsearch revision list after update and compare.
    List<BigInteger> revisionsAfterUpdate = checkTgsearchRevisions(
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue(), tgsearch);

    if (!revisionsAfterUpdate.equals(revisionsBeforeUpdate)) {
      System.out.println("ERROR: Revision list after (" + revisionsAfterUpdate
          + ") update is NOT equal to revision list before update metadata ("
          + revisionsBeforeUpdate + ")!");
      assertTrue(false);
    }

    return metadataHolder.value;
  }

  /**
   * <p>
   * Calls TG-crud#UPDATEMETADATA RESTful. All updated metadata is tested for the title metadata to
   * start with "UPDATE:"!
   * </p>
   * 
   * @param theClient
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param metadataFilename
   * @param theGenerated
   * @throws FileNotFoundException
   * @throws IOException
   */
  public static void updateMetadataREST(WebTarget theClient, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String metadataFilename,
      GeneratedType theGenerated) throws FileNotFoundException, IOException {

    System.out.println("\tCalling #UPDATEMETADATA RESTful");
    System.out.println("\t\tmeta file:    " + metadataFilename);
    System.out.println("\t\turi:          " + theGenerated.getTextgridUri().getValue());
    System.out.println(LINE);

    // Create metadata from file.
    MetadataContainerType tgObjectMetadata = getMetadataFromFile(metadataFilename);

    // Add the given generated type.
    tgObjectMetadata.getObject().getGeneric().setGenerated(theGenerated);

    // Create an HTTP entity.
    try (StringWriter metadataStringWriter = new StringWriter()) {
      JAXB.marshal(tgObjectMetadata, metadataStringWriter);
      Entity<List<Attachment>> entity =
          createMultipartEntityFromStringAndFilename(metadataStringWriter.toString(), NO_DATA_FILE);
      metadataStringWriter.close();

      // Check tgsearch revision list before update.
      List<BigInteger> revisionsBeforeUpdate = checkTgsearchRevisions(
          tgObjectMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue(),
          tgsearch);

      // Assemble and submit request.
      String uri =
          tgObjectMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      String path = uri + "/updateMetadata";
      WebTarget target = theClient.path(path).queryParam(PARAM_SID, theSessionId)
          .queryParam(PARAM_LOG, theLogParameter);

      System.out.println("\t\thttp POST:    " + target.getUri());

      Response response = target.request().post(entity);
      int statusCode = response.getStatus();
      InputStream content = response.readEntity(InputStream.class);

      System.out.println("\t\tstatus code:  " + statusCode);
      System.out.println(LINE);

      // Get and check metadata.
      tgObjectMetadata = TGCrudClientUtils.getTextgridMetadata(content);
      if (tgObjectMetadata == null || statusCode != Status.OK.getStatusCode()) {
        assertTrue(false);
      }

      // Check if the updated title and subtitle is existing in the metadata file.
      if (!tgObjectMetadata.getObject().getGeneric().getProvided().getTitle().get(0)
          .startsWith("UPDATEMETADATA:")) {
        throw new AssertionFailedError();
      }

      // Check expected headers and print metadata.
      checkFilenameAndMimetypeHeader(response, tgObjectMetadata, MediaType.TEXT_XML);
      TGCrudClientUtils.printMetadata(tgObjectMetadata.getObject());

      // Check tgsearch revision list after update and compare.
      List<BigInteger> revisionsAfterUpdate = checkTgsearchRevisions(
          tgObjectMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue(),
          tgsearch);

      if (!revisionsAfterUpdate.equals(revisionsBeforeUpdate)) {
        System.out.println("ERROR: Revision list after (" + revisionsAfterUpdate
            + ") update is NOT equal to revision list before update metadata ("
            + revisionsBeforeUpdate + ")!");
        assertTrue(false);
      }
    }
  }

  /**
   * <p>
   * Just updates data without any xsdUri or adaptorUri.
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param metadataFilename
   * @param dataFilename
   * @param theGenerated
   * @param expectedWarnings
   * @return
   * @throws IOException
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws UpdateConflictFault
   * @throws IoFault
   * @throws AuthFault
   */
  public static MetadataContainerType update(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String metadataFilename, String dataFilename,
      GeneratedType theGenerated, int expectedWarnings)
      throws IOException, MetadataParseFault, ObjectNotFoundFault,
      UpdateConflictFault, IoFault, AuthFault {
    return update(tgcrud, tgsearch, theSessionId, theLogParameter, metadataFilename, dataFilename,
        theGenerated, NO_URI, NO_URI, expectedWarnings);
  }

  /**
   * <p>
   * Calls TG-crud#UPDATE. All updated metadata is tested for the title metadata to start with
   * "UPDATE:"!
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param metadataFilename
   * @param dataFilename
   * @param theGenerated
   * @param adaptorUri
   * @param xsdUri
   * @param expectedWarnings
   * @return
   * @throws IOException
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws UpdateConflictFault
   * @throws IoFault
   * @throws AuthFault
   */
  public static MetadataContainerType update(TGCrudService tgcrud, SearchClient tgsearch,
      String theSessionId, String theLogParameter, String metadataFilename, String dataFilename,
      GeneratedType theGenerated, String adaptorUri, String xsdUri, int expectedWarnings)
      throws IOException, MetadataParseFault, ObjectNotFoundFault, UpdateConflictFault, IoFault,
      AuthFault {

    System.out.println("\tCalling #UPDATE");
    System.out.println("\t\tdata file:    " + dataFilename);
    System.out.println("\t\tmeta file:    " + metadataFilename);
    System.out.println("\t\turi:          " + theGenerated.getTextgridUri().getValue());
    System.out.println(LINE);

    // Create metadata from file.
    MetadataContainerType metadata = getMetadataFromFile(metadataFilename);

    // Add the given generated type.
    metadata.getObject().getGeneric().setGenerated(theGenerated);

    // Add the XSD and adaptor URIs if existing.
    if (adaptorUri != null && !adaptorUri.equals("")) {
      metadata.getObject().getRelations().setHasAdaptor(adaptorUri);
    }
    if (xsdUri != null && !xsdUri.equals("")) {
      metadata.getObject().getRelations().setHasSchema(xsdUri);
    }

    // Create data from file.
    DataHandler data =
        new DataHandler(new FileDataSource(TGCrudClientUtils.getResource(dataFilename)));

    // Create holder and set metadata.
    Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>(metadata);

    // Check tgsearch revision list before update.
    List<BigInteger> revisionsBeforeUpdate = checkTgsearchRevisions(
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue(), tgsearch);

    // Start #UPDATE operation.
    try {
      tgcrud.update(theSessionId, theLogParameter, metadataHolder, data);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (MetadataParseFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (UpdateConflictFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    // Check returned metadata.
    if (TGCrudClientUtils.metadataWarnings(metadataHolder.value, expectedWarnings)) {
      TGCrudClientUtils.printMetadata(metadataHolder.value.getObject());
      throw new AssertionFailedError();
    }

    // Check if the updated title and subtitle is existing in the metadata file.
    if (!metadataHolder.value.getObject().getGeneric().getProvided().getTitle().get(0)
        .startsWith("UPDATE:")) {
      throw new AssertionFailedError();
    }

    TGCrudClientUtils.printMetadata(metadataHolder.value.getObject());

    // Check tgsearch revision list after update and compare.
    List<BigInteger> revisionsAfterUpdate = checkTgsearchRevisions(
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue(), tgsearch);

    if (!revisionsAfterUpdate.equals(revisionsBeforeUpdate)) {
      System.out.println("ERROR: Revision list after (" + revisionsAfterUpdate
          + ") update is NOT equal to revision list before update (" + revisionsBeforeUpdate
          + ")!");
      assertTrue(false);
    }

    return metadataHolder.value;
  }

  /**
   * <p>
   * Calls TG-crud#UPDATE RESTful. All updated metadata is tested for the title metadata to start
   * with "UPDATE:"!
   * </p>
   * 
   * @param theClient
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param metadataFilename
   * @param dataFilename
   * @param theGenerated
   * @param expectedWarnings
   * @throws IOException
   */
  public static void updateREST(WebTarget theClient, SearchClient tgsearch, String theSessionId,
      String theLogParameter, String metadataFilename, String dataFilename,
      GeneratedType theGenerated, int expectedWarnings) throws IOException {

    System.out.println("\tCalling #UPDATE RESTful");
    System.out.println("\t\tdata file:    " + dataFilename);
    System.out.println("\t\tmeta file:    " + metadataFilename);
    System.out.println("\t\turi:          " + theGenerated.getTextgridUri().getValue());
    System.out.println(LINE);

    // Create metadata from file.
    MetadataContainerType tgObjectMetadata = getMetadataFromFile(metadataFilename);

    // Add the given generated type.
    tgObjectMetadata.getObject().getGeneric().setGenerated(theGenerated);

    // Create data from file.
    new DataHandler(new FileDataSource(TGCrudClientUtils.getResource(dataFilename)));

    // Create an HTTP entity.
    try (StringWriter metadataStringWriter = new StringWriter()) {
      JAXB.marshal(tgObjectMetadata, metadataStringWriter);
      Entity<List<Attachment>> entity =
          createMultipartEntityFromStringAndFilename(metadataStringWriter.toString(), dataFilename);
      metadataStringWriter.close();

      // Check tgsearch revision list before update.
      List<BigInteger> revisionsBeforeUpdate = checkTgsearchRevisions(
          tgObjectMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue(),
          tgsearch);

      // Assemble and submit request.
      String uri =
          tgObjectMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      String path = uri + "/update";
      WebTarget target = theClient.path(path).queryParam(PARAM_SID, theSessionId);

      System.out.println("\t\thttp POST:    " + target.getUri());

      Response response = target.request().post(entity);
      int statusCode = response.getStatus();
      String reasonPhrase = response.getStatusInfo().getReasonPhrase();
      InputStream content = response.readEntity(InputStream.class);

      System.out.println("\t\tstatus code:  " + statusCode + " " + reasonPhrase);
      System.out.println(LINE);

      // Check response and get metadata.
      assertFalse(content == null);
      if (statusCode != Status.OK.getStatusCode()) {
        String contentString = IOUtils.readStringFromStream(content);
        assertFalse("ERROR HTML must NOT be null or empty!",
            contentString == null || contentString.isEmpty());
        System.out.println("\t\tERROR HTML:");
        System.out.println(contentString);
        return;
      }

      // Get and check metadata.
      MetadataContainerType metadata = TGCrudClientUtils.getTextgridMetadata(content);
      if (metadata == null || statusCode != Status.OK.getStatusCode()) {
        System.out.println();
        assertTrue(false);
      }

      // Check returned metadata.
      if (TGCrudClientUtils.metadataWarnings(metadata, expectedWarnings)) {
        throw new AssertionFailedError();
      }

      // Check if the updated title and subtitle is existing in the metadata file.
      if (!metadata.getObject().getGeneric().getProvided().getTitle().get(0)
          .startsWith("UPDATE:")) {
        throw new AssertionFailedError();
      }

      // Check expected headers and print metadata.
      checkFilenameAndMimetypeHeader(response, tgObjectMetadata, MediaType.TEXT_XML);
      TGCrudClientUtils.printMetadata(tgObjectMetadata.getObject());

      // Check tgsearch revision list after update and compare.
      List<BigInteger> revisionsAfterUpdate = checkTgsearchRevisions(
          metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue(), tgsearch);

      if (!revisionsAfterUpdate.equals(revisionsBeforeUpdate)) {
        System.out.println("ERROR: Revision list after (" + revisionsAfterUpdate
            + ") update is NOT equal to revision list before update (" + revisionsBeforeUpdate
            + ")!");
        assertTrue(false);
      }
    }
  }

  /**
   * <p>
   * Calls TG-crud#DELETE.
   * </p>
   * 
   * @param tgcrud
   * @param tgsearch
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @throws ObjectNotFoundFault
   * @throws RelationsExistFault
   * @throws IoFault
   * @throws AuthFault
   * @throws IOException
   */
  public static void delete(TGCrudService tgcrud, SearchClient tgsearch, String theSessionId,
      String theLogParameter, String theUri)
      throws ObjectNotFoundFault, RelationsExistFault, IoFault, AuthFault, IOException {

    System.out.println("\tCalling #DELETE");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Check tgsearch revision list before delete.
    List<BigInteger> revisionsBeforeDelete = checkTgsearchRevisions(theUri, tgsearch);
    BigInteger revision = BigInteger.valueOf(getRevision(URI.create(theUri)));

    // Start #DELETE operation.
    try {
      tgcrud.delete(theSessionId, theLogParameter, theUri);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (RelationsExistFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    // Check tgsearch revision list after delete.
    List<BigInteger> revisionsAfterDelete = checkTgsearchRevisions(theUri, tgsearch);

    if (revisionsAfterDelete.contains(revision)
        || (revisionsBeforeDelete.size() - 1) != revisionsAfterDelete.size()) {
      System.out.println("ERROR: Deleted revision is still in tgsearch revision list (" + revision
          + " contained in " + revisionsAfterDelete + ") or revision sizes do not match ("
          + (revisionsAfterDelete.size() - 1) + " != " + revisionsAfterDelete.size() + ")!");
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Calls TG-crud#DELETE RESTful.
   * </p>
   * 
   * @param theClient
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @throws IOException
   */
  public static void deleteREST(WebTarget theClient, String theSessionId, String theLogParameter,
      String theUri) throws IOException {

    System.out.println("\tCalling #DELETE RESTful");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Assemble and submit request.
    String path = theUri + "/delete";
    WebTarget target = theClient.path(path).queryParam(PARAM_SID, theSessionId)
        .queryParam(PARAM_LOG, theLogParameter);

    System.out.println("\t\thttp GET:     " + target.getUri());

    Response response = target.request().get();
    int statusCode = response.getStatus();

    System.out.println("\t\tstatus code:  " + statusCode);
    System.out.println(LINE);

    if (statusCode != Status.NO_CONTENT.getStatusCode()) {
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Calls TG-crud#MOVEPUBLIC.
   * </p>
   * 
   * @param tgcrud
   * @param theSessionId
   * @param theLogParameter
   * @param uri
   * @throws ObjectNotFoundFault
   * @throws RelationsExistFault
   * @throws IoFault
   * @throws AuthFault
   */
  public static void move(TGCrudService tgcrud, String theSessionId, String theLogParameter,
      String theUri) throws ObjectNotFoundFault, RelationsExistFault, IoFault, AuthFault {

    System.out.println("\tCalling #MOVEPUBLIC");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Start #MOVEPUBLIC operation.
    try {
      tgcrud.movePublic(theSessionId, theLogParameter, theUri);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }
  }

  /**
   * <p>
   * Calls TG-crud#LOCK.
   * </p>
   * 
   * @param tgcrud
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @return
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   */
  public static boolean lock(TGCrudService tgcrud, String theSessionId, String theLogParameter,
      String theUri) throws ObjectNotFoundFault, IoFault, AuthFault {

    boolean result = false;

    System.out.println("\tCalling #LOCK");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Start #LOCK operation.
    try {
      result = tgcrud.lock(theSessionId, theLogParameter, theUri);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    if (result) {
      System.out.println("\t\tLOCKED");
    }

    return result;
  }

  /**
   * <p>
   * Calls TG-crud#UNLOCK.
   * </p>
   * 
   * @param tgcrud
   * @param theSessionId
   * @param theLogParameter
   * @param theUri
   * @return
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   */
  public static boolean unlock(TGCrudService tgcrud, String theSessionId, String theLogParameter,
      String theUri) throws ObjectNotFoundFault, IoFault, AuthFault {

    boolean result = false;

    System.out.println("\tCalling #UNLOCK");
    System.out.println("\t\turi:          " + theUri);
    System.out.println(LINE);

    // Start #UNLOCK operation.
    try {
      result = tgcrud.unlock(theSessionId, theLogParameter, theUri);
    } catch (ObjectNotFoundFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (IoFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    } catch (AuthFault e) {
      System.out
          .println(TGCrudClientUtils.faultDetails(e.getFaultInfo(), e.getClass().getSimpleName()));
      throw e;
    }

    if (result) {
      System.out.println("\t\tUNLOCKED");
    }

    return result;
  }

  /**
   * <p>
   * Prints out the middleware metadata from a TextGrid metadata object.
   * </p>
   * 
   * @param theGenerated
   * @throws IOException
   */
  public synchronized static void printMetadata(ObjectType theMetadata) throws IOException {

    GeneratedType generated = theMetadata.getGeneric().getGenerated();
    ProvidedType provided = theMetadata.getGeneric().getProvided();
    RelationType relation = theMetadata.getRelations();

    System.out.println("\t\ttitle:        " + provided.getTitle().get(0));
    System.out.println("\t\tformat:       " + provided.getFormat());
    System.out.println(LINE);

    SimpleDateFormat uriDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    System.out.println("\t\tcreated:      "
        + uriDateFormat.format(generated.getCreated().toGregorianCalendar().getTime()));
    System.out.println("\t\tlastModified: "
        + uriDateFormat.format(generated.getLastModified().toGregorianCalendar().getTime()));
    System.out.println("\t\turi:          " + generated.getTextgridUri().getValue());
    System.out.println("\t\trevision:     " + generated.getRevision());
    System.out.println("\t\tsize:         " + generated.getExtent());
    if (generated.getFixity() != null && !generated.getFixity().isEmpty()) {
      for (Fixity f : generated.getFixity()) {
        System.out.println("\t\tfixity:       " + f.getMessageDigestAlgorithm() + " "
            + f.getMessageDigest() + " " + f.getMessageDigestOriginator());
      }
    }
    System.out.println("\t\towner ID:     " + generated.getDataContributor());
    System.out.println("\t\tpermissions:  " + generated.getPermissions());
    System.out.print("\t\twarnings:     ");
    if (generated.getWarning() != null && !generated.getWarning().isEmpty()) {
      System.out.println();
      int i = 0;
      for (Warning w : generated.getWarning()) {
        System.out.println("\t\t\t[" + i++ + "] " + w.getValue());
      }
    } else {
      System.out.println("none at all");
    }
    System.out.print("\t\trelations:    ");
    if (relation != null) {
      System.out.println();
      if (relation.getRDF() != null) {
        try (StringWriter rdfWriter = new StringWriter()) {
          JAXBElement<RdfType> t = new JAXBElement<RdfType>(
              new QName("http://www.w3.org/1999/02/22-rdf-syntax-ns#", "RDF"),
              RdfType.class, relation.getRDF());
          JAXB.marshal(t, rdfWriter);
          System.out.println("\t\t\tRDF:\n" + rdfWriter.toString().trim());
        }
      }
      if (relation.getHasAdaptor() != null) {
        System.out.println("\t\t\thasAdaptor: " + relation.getHasAdaptor());
      }
      if (relation.getHasSchema() != null) {
        System.out.println("\t\t\thasSchema: " + relation.getHasSchema());
      }
      if (relation.getIsAlternativeFormatOf() != null) {
        System.out.println("\t\t\taltFormat: " + relation.getIsAlternativeFormatOf());
      }
      if (relation.getIsDerivedFrom() != null) {
        System.out.println("\t\t\tderivedFrom: " + relation.getIsDerivedFrom());
      }
    } else {
      System.out.println("none at all");
    }
  }

  /**
   * <p>
   * Loads a resource.
   * </p>
   * 
   * TODO Put together with the method in TGCrudServiceUtilities! Maybe create a first build maven
   * module for util things.
   * 
   * @param {@link String} The resource to search for.
   * @return {@link File} The resource.
   * @throws IOException
   */
  public static File getResource(String resPart) throws IOException {

    File res;

    // If we have an absolute resPart, just return the file.
    if (resPart.startsWith(File.separator)) {
      return new File(resPart);
    }

    URL url = ClassLoader.getSystemClassLoader().getResource(resPart);
    if (url == null) {
      throw new IOException("Resource '" + resPart + "' not found");
    }
    try {
      res = new File(url.toURI());
    } catch (URISyntaxException ue) {
      res = new File(url.getPath());
    }

    return res;
  }

  /**
   * <p>
   * Transfer a TextGrid metadata input stream into a TextGrid Metadata object.
   * </p>
   * 
   * @param theMetadataStream
   * @return
   */
  public static MetadataContainerType getTextgridMetadata(InputStream theMetadataStream) {
    return JAXB.unmarshal(theMetadataStream, MetadataContainerType.class);
  }

  /**
   * <p>
   * Checks the resulting metadata file given back by the TG-crud for specific entries, that can not
   * be checked by the TG-crud's internal tests.
   * </p>
   * 
   * @param theMetadata
   * @param expectedWarnings
   * @return
   */
  public static boolean metadataWarnings(MetadataContainerType theMetadata, int expectedWarnings) {

    GeneratedType generated = theMetadata.getObject().getGeneric().getGenerated();

    // At first check the warnings, there should be the expected amount only.
    List<Warning> warnings = generated.getWarning();
    if (!ignoreWarnings && warnings != null) {
      if (warnings.size() != expectedWarnings) {
        System.out.println(
            "\t\tWARNING: Amount of warnings differ (" + warnings.size() + ")! There should be "
                + expectedWarnings + " warning" + (expectedWarnings != 1 ? "s" : "")
                + " in the response metadata!");
        return true;
      }
    }

    return false;
  }

  /**
   * @param theFaultType
   * @return
   */
  public static String faultDetails(TextGridFaultType theFaultType, String theClassName) {
    return "\t\tfault:      #" + theFaultType.getFaultNo() + "\n\t\tfault name: " + theClassName
        + "\n\t\tmessage:    " + theFaultType.getFaultMessage() + "\n\t\tcause:      "
        + theFaultType.getCause();
  }

  /**
   * @param theFaultType
   * @return
   */
  public static String faultDetails(
      info.textgrid.namespaces.middleware.tgauth_crud.TextGridFaultType theFaultType,
      String theClassName) {
    return "\t\tfault:      #" + theFaultType.getFaultNo() + "\n\t\tfault name: " + theClassName
        + "\n\t\tmessage:    " + theFaultType.getFaultMessage() + "\n\t\tcause:      "
        + theFaultType.getCause();
  }

  /**
   * @param theFaultType
   * @return
   */
  public static String faultDetails(
      info.textgrid.namespaces.middleware.tgauth.TextGridFaultType theFaultType,
      String theClassName) {
    return "\t\tfault:      #" + theFaultType.getFaultNo() + "\n\t\tfault name: " + theClassName
        + "\n\t\tmessage:    " + theFaultType.getFaultMessage() + "\n\t\tcause:      "
        + theFaultType.getCause();
  }

  // **
  // PRIVATE METHODS
  // **

  /**
   * @param theData
   * @param theSize
   * @param theChecksum
   * @throws IOException
   * @throws NoSuchAlgorithmException
   */
  private static void checkSizeAndDigest(DataHandler theData, MetadataContainerType theMetadata)
      throws IOException, NoSuchAlgorithmException {

    File f = File.createTempFile("fileCheck__", "__tempFile", getResource(""));
    try (FileOutputStream fos = new FileOutputStream(f)) {

      // Create a counting stream around a file output stream.
      CountingOutputStream count = new CountingOutputStream(fos);

      // Create a message digest stream around the counting stream.
      MessageDigest md = MessageDigest.getInstance("MD5");
      try (DigestOutputStream digest = new DigestOutputStream(count, md)) {
        // Write TO THE STORAGE using the digest stream using the counting stream :-)
        theData.writeTo(digest);
      }

      // Close both streams.
      count.close();
      fos.close();

      // Delete temp file.
      boolean deleted = f.delete();
      if (!deleted) {
        throw new AssertionFailedError("ERROR: Temp file was not deleted: " + f.getCanonicalPath());
      }

      // Check file size.
      BigInteger size = BigInteger.valueOf(count.getByteCount());
      BigInteger metadataSize = theMetadata.getObject().getGeneric().getGenerated().getExtent();

      System.out.println(LINE);
      System.out.println("\t\tsource file size:   " + size);
      System.out.println("\t\tsize from metadata: " + metadataSize);
      System.out.println("\t\tsize difference:    " + (size.subtract(metadataSize)).abs());

      if (size.compareTo(metadataSize) != 0 && !theMetadata.getObject().getGeneric().getProvided()
          .getFormat().contains(CHANGE_ACCEPT_STRING)) {
        throw new AssertionFailedError(
            "ERROR: Size from source file and size from metadata differs!");
      }

      // Check digest.
      String checksum = new String(Hex.encodeHex(md.digest()));
      String metadataChecksum =
          theMetadata.getObject().getGeneric().getGenerated().getFixity().get(0).getMessageDigest();

      System.out.println(LINE);
      System.out.println("\t\tMD5 sum of file:       " + checksum);
      System.out.println("\t\tMD5 sum from metadata: " + metadataChecksum);

      if (!metadataChecksum.equals(checksum)) {
        System.out.println("ERROR: MD5 sum from data file and MD5 sum from metadata differs!");
        throw new AssertionFailedError();
      }
    }
  }

  /**
   * @param theMetadata
   * @param theClient
   * @throws IOException
   */
  private static void checkRevisionStatusCreate(MetadataContainerType theMetadata,
      SearchClient theClient) throws IOException {

    // Get revision from metadata.
    int revisionFromMetadata = theMetadata.getObject().getGeneric().getGenerated().getRevision();

    // Get base URI.
    String baseUri = stripRevision(
        URI.create(theMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue()))
            .toString();

    // Check with tgsearch. Revision should be same as in metadata.
    List<BigInteger> revisionList = theClient.infoQuery().listRevisions(baseUri).getRevision();

    // Check if revision from metadata (after create revision must be contained in tgsearch revision
    // list).
    if (!revisionList.contains(BigInteger.valueOf(revisionFromMetadata))) {
      System.out.println("ERROR: Revision from metadata (" + revisionFromMetadata
          + ") is not contained in tgsearch revision list response: " + revisionList);
      throw new AssertionFailedError();
    }

    // Check tgsearch revision value (after create revision must always be latest in tgsearch
    // revision list).
    if (revisionList.get(revisionList.size() - 1).intValue() != revisionFromMetadata) {
      System.out.println(
          "ERROR: Latest revision value has different values in metadata (" + revisionFromMetadata
              + ") and tgsearch (" + revisionList.get(revisionFromMetadata) + ")!");
      System.out.println("tgsearch response: " + revisionList);
      throw new AssertionFailedError();
    }

    System.out.println(LINE);
    System.out.println("\t\trevision from metadata:        " + revisionFromMetadata);
    System.out
        .println("\t\tlatest revision from tgsearch: " + revisionList.get(revisionList.size() - 1));
    System.out.println("\t\trevisions from tgsearch:       " + revisionList);
  }

  /**
   * @param theData
   * @param theMetadata
   * @param theSearchClient
   * @param theSessionID
   * @throws IOException
   */
  public static void checkTGSearchAggregationEntries(String theData,
      MetadataContainerType theMetadata, SearchClient theSearchClient, String theSessionID)
      throws IOException {

    String format = theMetadata.getObject().getGeneric().getProvided().getFormat();

    // Check for format.
    if (!TextGridMimetypes.AGGREGATION_SET.contains(format)) {
      return;
    }

    String uri = theMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    System.out.println("\t\tdo navigation query for " + uri);

    info.textgrid.namespaces.middleware.tgsearch.Response aggregationURIs =
        theSearchClient.navigationQuery().setSid(theSessionID).listAggregation(uri);

    List<ResultType> results = aggregationURIs.getResult();

    if (results != null && !results.isEmpty()) {
      System.out.println(LINE);
      for (ResultType r : results) {
        System.out.println("\t\t" + r.getTextgridUri());
        if (!theData.contains(r.getTextgridUri())) {
          System.out.println(LINE);
          System.out.println("\t\tERROR: Expected aggregation entry " + r.getTextgridUri()
              + " is not existing in tgsearch result!");
          System.out.println(LINE);
          System.out.println(theData.trim());
          System.out.println(LINE);
          throw new AssertionFailedError();
        }
      }
      System.out.println(LINE);
      System.out.println(theData.trim());
      System.out.println(LINE);
    } else {
      System.out.println(LINE);
      System.out
          .println("\t\tERROR: Expected aggregation entries are not existing in tgsearch result!");
      System.out.println(LINE);
      System.out.println(theData.trim());
      System.out.println(LINE);
      throw new AssertionFailedError();
    }
  }

  /**
   * @param theUri
   * @param theClient
   * @return
   * @throws IOException
   */
  private static List<BigInteger> checkTgsearchRevisions(String theUri, SearchClient theClient)
      throws IOException {

    // Get base URI.
    String baseUri = stripRevision(URI.create(theUri)).toString();

    // Check with tgsearch. Revision should be greater or equal as in metadata.
    List<BigInteger> revisionList = theClient.infoQuery().listRevisions(baseUri).getRevision();

    System.out.println("\t\trevisions from tgsearch: " + revisionList);

    return revisionList;
  }

  /**
   * <p>
   * Queries the Sesame.
   * </p>
   * 
   * @param theServiceUri
   * @param theQuery
   * @return
   * @throws IOException
   */
  public static InputStream querySesame(String theServiceUri, String theQuery) throws IOException {

    // Get a Sesame TextGrid HTTP client.
    SesameClient client = new SesameClient(theServiceUri);

    return client.sparql(theQuery);
  }

  // **
  // GETTERS AND SETTERS
  // **

  /**
   * @param ignoreWarningsSetter
   */
  public static void setIgnoreWarnings(boolean ignoreWarningsSetter) {
    ignoreWarnings = ignoreWarningsSetter;
  }

  /**
   * <p>
   * Get metadata from file.
   * </p>
   * 
   * @param theFilename
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   */
  public static MetadataContainerType getMetadataFromFile(String theFilename)
      throws FileNotFoundException, IOException {

    MetadataContainerType result;

    // Create metadata from file.
    result = TGCrudClientUtils
        .getTextgridMetadata(new FileInputStream(TGCrudClientUtils.getResource(theFilename)));

    // We want to accept MetadataContainerTypes and ObjectTypes as XML. So we check if
    // MetadataContainerType contains ObjectType, if not, read ObjectType and set
    // MetadataContainerType.
    if (result.getObject() == null) {
      result
          .setObject(JAXB.unmarshal(TGCrudClientUtils.getResource(theFilename), ObjectType.class));
    }

    return result;
  }

  /**
   * <p>
   * Returns a regular TG-auth stub.
   * </p>
   * 
   * @param theWsdlLocation
   * @return
   */
  public static PortTgextra getTgauthService(URL theEndpoint) {

    System.out.println("tgextra endpoint: " + theEndpoint.getProtocol() + "://"
        + theEndpoint.getHost() + (theEndpoint.getPort() == -1 ? "" : ":" + theEndpoint.getPort())
        + theEndpoint.getFile());

    // Create TG-extra service stubs.
    PortTgextra service = TGAuthClientUtilities.getTgextra(theEndpoint);

    System.out.println("...ok");

    return service;
  }

  /**
   * <p>
   * Returns a TG-auth_crud stub.
   * </p>
   * 
   * @param theWsdlLocation
   * @return
   */
  public static PortTgextraCrud getTgauthCrudService(URL theEndpoint) {

    System.out.println("tgextra-crud endpoint: " + theEndpoint.getProtocol() + "://"
        + theEndpoint.getHost() + (theEndpoint.getPort() == -1 ? "" : ":" + theEndpoint.getPort())
        + theEndpoint.getFile());

    // Create TG-extra service stubs.
    PortTgextraCrud service = TGAuthClientCrudUtilities.getTgextraCrud(theEndpoint);

    System.out.println("...ok");

    return service;
  }

  /**
   * <p>
   * Strips the revision from the given URI, if existing.
   * </p>
   * 
   * @param theUri
   * @return The URI stripped from revision numbers.
   * @throws IOException
   */
  public static URI stripRevision(URI theUri) throws IOException {

    if (isBaseUri(theUri)) {
      return theUri;
    }

    return URI.create(theUri.toASCIIString().substring(0, theUri.toASCIIString().indexOf(".")));
  }

  /**
   * <p>
   * Checks if the given URI is a base URI.
   * </p>
   * 
   * @param theUri
   * @return Is it a base URI or is it not a base URI?
   * @throws IOException
   */
  public static boolean isBaseUri(URI theUri) throws IOException {

    if (theUri == null) {
      throw new IOException("URI must not be null!");
    }

    return !theUri.toASCIIString().contains(REVISION_SEPARATION_CHAR);
  }

  /**
   * <p>
   * Returns the URI's revision number as an int.
   * </p>
   * 
   * @param theUri
   * @return The revision of an URI as an int.
   */
  public static int getRevision(URI theUri) {
    return Integer.parseInt(theUri.toASCIIString()
        .substring(theUri.toASCIIString().lastIndexOf(REVISION_SEPARATION_CHAR) + 1));
  }

  /**
   * @param theResponse
   * @param theMetadata
   * @param theExpectedMediaType
   */
  public static void checkFilenameAndMimetypeHeader(Response theResponse,
      MetadataContainerType theMetadata, String theExpectedMediaType) {

    // Check for filename in HTTP header.
    MultivaluedMap<String, Object> m = theResponse.getHeaders();

    // Check needed header entries and their values.
    if (m.containsKey(HttpHeaders.CONTENT_DISPOSITION)) {
      String v = (String) m.getFirst(HttpHeaders.CONTENT_DISPOSITION);
      System.out.println("\t\tcontent-disp: " + v);
      // Expected something like:
      // inline; filename="TG_crud_JUnit_CWDS_Band_1_Seite_0001.3tkjf.0.tiff.meta"
      String expectedHeaderValue = "inline; filename=\"" + getDataFilenameFromMetadata(theMetadata);
      if (!v.contains(expectedHeaderValue)) {
        System.out.println("\t\tEXPECTED content-disp: " + expectedHeaderValue);
        assertTrue(false);
      }
    } else {
      assertTrue(false);
    }
    if (m.containsKey(HttpHeaders.CONTENT_TYPE)) {
      String v = (String) m.getFirst(HttpHeaders.CONTENT_TYPE);
      System.out.println("\t\tcontent-type: " + v);
      if (!v.equals(theExpectedMediaType.toString())) {
        System.out.println("\t\tEXPECTED content-type: " + v);
        assertTrue(false);
      }
    } else {
      assertTrue(false);
    }
    if (m.containsKey(X_CLACKS_NAME)) {
      String v = (String) m.getFirst(X_CLACKS_NAME);
      System.out.println("\t\tx-clcks-ovrh: " + v);
      if (!v.equals(X_CLACKS_VALUE)) {
        System.out.println("\t\tEXPECTED x-clacks-overhead: " + X_CLACKS_VALUE);
        assertTrue(false);
      }
    } else {
      assertTrue(false);
    }

    System.out.println(LINE);
  }

  /**
   * @param theMetadataFilename
   * @param theDataFilename
   * @return
   * @throws IOException
   */
  public static Entity<List<Attachment>> createMultipartEntityFromFilename(
      String theMetadataFilename, String theDataFilename) throws IOException {

    Entity<List<Attachment>> result;

    List<Attachment> attachments = new LinkedList<Attachment>();
    Attachment metadata = new Attachment("tgObjectMetadata", MediaType.TEXT_XML,
        TGCrudClientUtils.getResource(theMetadataFilename));
    Attachment data = new Attachment("tgObjectData", MediaType.APPLICATION_OCTET_STREAM,
        TGCrudClientUtils.getResource(theDataFilename));
    attachments.add(metadata);
    attachments.add(data);

    result = Entity.entity(attachments, MediaType.MULTIPART_FORM_DATA);

    return result;
  }

  /**
   * @param theMetadataString
   * @param theDataFilename
   * @return
   * @throws IOException
   */
  public static Entity<List<Attachment>> createMultipartEntityFromStringAndFilename(
      String theMetadataString, String theDataFilename) throws IOException {

    Entity<List<Attachment>> result;

    List<Attachment> attachments = new LinkedList<Attachment>();

    if (theMetadataString != null && !theMetadataString.isEmpty()) {
      Attachment metadata =
          new Attachment("tgObjectMetadata", MediaType.TEXT_XML, theMetadataString);
      attachments.add(metadata);
    }
    if (theDataFilename != null && !theDataFilename.isEmpty()) {
      Attachment data = new Attachment("tgObjectData", MediaType.APPLICATION_OCTET_STREAM,
          TGCrudClientUtils.getResource(theDataFilename));
      attachments.add(data);
    }

    result = Entity.entity(attachments, MediaType.MULTIPART_FORM_DATA);

    return result;
  }

  /**
   * <p>
   * Build a fine filename from metadata:
   * 
   * <ul>
   * <li>edition: [title].[baseURI].[revision].edition<br/>
   * Reise_nach_dem_Mittelpunkt_der_Erde.wr7j.0.edition</li>
   * <li>collection: [title].[baseURI].[revision].collection<br/>
   * example: Reise_nach_dem_Mittelpunkt_der_Erde.wr7j.0.collection</li>
   * <li>aggregation: [title].[baseURI].[revision].aggregation<br/>
   * example: Reise_nach_dem_Mittelpunkt_der_Erde.wr7j.0.aggregation</li>
   * <li>work: [title].[baseURI].[revision].work<br/>
   * example: Reise_nach_dem_Mittelpunkt_der_Erde.wr7g.0.work</li>
   * <li>data: [title].[baseUri].[revision].[fileExtension]<br/>
   * example: Graphic_Recording_1.24g6b.0.jpg</li>
   * </ul>
   * 
   * Metadata filename is always data filename plus ".meta"! Has to be appended from calling method!
   * </p>
   * 
   * <p>
   * TODO Put this method in common, it's used in TGCrudServiceUtilities, too!
   * </p>
   * 
   * @param theURI
   * @param theMimetype
   * @return The filename for the given URI.
   */
  public static String getDataFilenameFromMetadata(MetadataContainerType theMetadata) {

    String result;

    // Get suffix from format or file extension, if applicable.
    String mimetype = theMetadata.getObject().getGeneric().getProvided().getFormat();
    String formatSuffix;
    switch (mimetype) {
      case TextGridMimetypes.EDITION:
        formatSuffix = ".edition";
        break;
      case TextGridMimetypes.COLLECTION:
        formatSuffix = ".collection";
        break;
      case TextGridMimetypes.AGGREGATION:
        formatSuffix = ".aggregation";
        break;
      case TextGridMimetypes.WORK:
        formatSuffix = ".work";
        break;
      default:
        formatSuffix = LTPUtils.getFileExtension(mimetype);
    }

    // Get more information.
    String title = theMetadata.getObject().getGeneric().getProvided().getTitle().get(0);
    String uri = theMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Gather everything.
    result = title.replaceAll("\\W+", "_") + "." + uri.replace("textgrid:", "") + formatSuffix;

    return result.trim();
  }

}
