<?xml version="1.0" encoding="UTF-8"?>

<!--  UPDATE -->

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0"
  xmlns="http://www.tei-c.org/ns/1.0">

  <xsl:strip-space elements="whitespace-only"/>
  <xsl:output method="xml" omit-xml-declaration="no" encoding="UTF-8"
    indent="no" normalization-form="NFKC"/>

  <xsl:template match="*">
    <xsl:element name="{local-name(.)}" namespace="http://www.tei-c.org/ns/1.0">
      <xsl:copy-of select="@*"/>
      <!--xsl:call-template name="insert-orig-pointer"/-->
      <xsl:apply-templates/>
    </xsl:element>
  </xsl:template>

  <xsl:template match="TEI">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="teiHeader">
    <teiHeader>
      <xsl:apply-templates/>
    </teiHeader>
  </xsl:template>

  <xsl:template match="fileDesc">
    <fileDesc>
      <xsl:apply-templates/>
    </fileDesc>
  </xsl:template>

  <xsl:template match="titleStmt">
    <titleStmt>
      <xsl:apply-templates/>
    </titleStmt>
  </xsl:template>

  <xsl:template match="title">
    <title>
      <xsl:apply-templates/>
    </title>
  </xsl:template>

  <xsl:template match="author">
    <author>
      <xsl:apply-templates/>
    </author>
  </xsl:template>

  <xsl:template match="publicationStmt">
    <publicationStmt>
      <xsl:apply-templates/>
    </publicationStmt>
  </xsl:template>

  <xsl:template match="availability">
    <availability>
      <xsl:apply-templates/>
    </availability>
  </xsl:template>

  <xsl:template match="authority">
    <authority>
      <xsl:apply-templates/>
    </authority>
  </xsl:template>

  <xsl:template match="sourceDesc">
    <sourceDesc>
      <xsl:apply-templates/>
    </sourceDesc>
  </xsl:template>

  <xsl:template match="encodingDesc">
    <encodingDesc>
      <xsl:apply-templates/>
    </encodingDesc>
  </xsl:template>

  <xsl:template match="editorialDecl">
    <editorialDecl>
      <xsl:apply-templates/>
    </editorialDecl>
  </xsl:template>

  <xsl:template match="normalization//head">
    <p>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="normalization">
    <normalization>
      <xsl:apply-templates/>
    </normalization>
  </xsl:template>

  <xsl:template match="p">
    <p>
      <xsl:apply-templates/>
    </p>
  </xsl:template>

  <xsl:template match="revisionDesc">
    <revisionDesc>
      <xsl:apply-templates/>
    </revisionDesc>
  </xsl:template>


  <xsl:template match="change">
    <change>
      <xsl:attribute name="who">
        <xsl:value-of select="@who"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </change>
  </xsl:template>

  <xsl:template match="head">
    <head>
      <xsl:apply-templates/>
    </head>
  </xsl:template>

  <xsl:template match="text">
    <text>
      <xsl:apply-templates/>
    </text>
  </xsl:template>

  <xsl:template match="body">
    <body>
      <xsl:apply-templates/>
    </body>
  </xsl:template>



  <xsl:template match="div">
    <div>
      <xsl:if test="@n">
        <xsl:attribute name="n">
          <xsl:value-of select="@n"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="@type">
        <xsl:attribute name="type">
          <xsl:value-of select="@type"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:if test="@xml:id">
        <xsl:attribute name="xml:id">
          <xsl:value-of select="@xml:id"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </div>
  </xsl:template>


  <xsl:template match="superEntry">
    <superEntry>
      <xsl:if test="@xml:id">
        <xsl:attribute name="xml:id">
          <xsl:value-of select="@xml:id"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </superEntry>
  </xsl:template>


  <xsl:template match="entry">
    <entry>
      <xsl:if test="@xml:id">
        <xsl:attribute name="xml:id">
          <xsl:value-of select="@xml:id"/>
        </xsl:attribute>
      </xsl:if>
      <form type="headword">
        <xsl:apply-templates select="form"/>
      </form>
      <sense>
        <xsl:apply-templates select="node()[local-name(.)!='form']"/>
      </sense>
    </entry>
  </xsl:template>


  <xsl:template match="form">
    <!-- Beispiel, bei dem das tag ein Attribut hat -->
    <!--form>
                <xsl:choose>
                <xsl:when test="@type='headword'">
                <xsl:attribute name="type">
                    <xsl:text>headword</xsl:text>
                </xsl:attribute>
                </xsl:when>
                <xsl:when test="@type='lemma'">
                <xsl:attribute name="type">
                    <xsl:text>lemma</xsl:text>
                </xsl:attribute>
                </xsl:when>
                <xsl:otherwise>
                <xsl:attribute name="type">
                    <xsl:text>simple</xsl:text>
                </xsl:attribute>
                </xsl:otherwise>
                </xsl:choose-->
    <xsl:apply-templates/>
    <!--/form-->
  </xsl:template>



  <xsl:template match="per"/>

  <xsl:template match="tns"/>

  <xsl:template match="mood">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="iType"/>


  <xsl:template match="number">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="hi/usg">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="usg">
    <!-- Beispiel, bei dem das Attribut einen Wert hat-->
    <usg>
      <xsl:if test="@type">
        <xsl:attribute name="type">
          <xsl:value-of select="@type"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </usg>
  </xsl:template>


  <xsl:template match="g">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="orth">
    <!--Beispiel für Inhaltausgabe, ohne dass die tags der Projektkodierung wiedergegeben werden -->
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="c">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="w[@rend]">
    <w lemma="{@rend}">
      <xsl:apply-templates/>
    </w>
  </xsl:template>


  <!--  <xsl:template match="orth/tei:c"/> -->




  <xsl:template match="gramGrp">
    <!--Beispiel für Inhaltausgabe, bei dem die tags der Projektkodierung wiedergegeben werden -->
    <gramGrp>
      <xsl:apply-templates/>
    </gramGrp>
  </xsl:template>



  <xsl:template match="pos">
    <pos>
      <xsl:if test="@value">
        <xsl:attribute name="value">
          <xsl:value-of select="@value"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </pos>
  </xsl:template>

  <xsl:template match="gen">
    <gen>
      <xsl:if test="@value">
        <xsl:attribute name="value">
          <xsl:value-of select="@value"/>
        </xsl:attribute>
      </xsl:if>
      <xsl:apply-templates/>
    </gen>
  </xsl:template>
  <!--  
    <xsl:template match="form[attribute::type='determiner']">
        <form>
            <xsl:attribute name="type">
                <xsl:text>determiner</xsl:text>
            </xsl:attribute>
            <xsl:apply-templates/>
        </form>
        </xsl:template>-->

  <!--  <xsl:template match="orth">
        <orth>
            <xsl:apply-templates/>
        </orth>
    </xsl:template>
        
    -->

  <!--<xsl:template match="gen">
        <xsl:apply-templates/>
    </xsl:template>

    <xsl:template match="case">
        <xsl:apply-templates/>
    </xsl:template>
    
    <xsl:template match="oVar">
    <xsl:apply-templates/>
        </xsl:template> -->

  <xsl:template match="entry/lb[1]"/>

  <xsl:template match="cit/lb"/>

  <xsl:template match="cit/author">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="lb">
    <milestone unit="line">
      <xsl:if test="@n">
        <xsl:attribute name="n">
          <xsl:value-of select="@n"/>
        </xsl:attribute>
      </xsl:if>
    </milestone>
  </xsl:template>

  <xsl:template match="cit/pb"/>

  <xsl:template match="pb">
    <milestone unit="page"/>
  </xsl:template>

  <xsl:template match="cit/cb"/>

  <xsl:template match="cb">
    <milestone unit="column"/>
  </xsl:template>



  <xsl:template match="sense">
    <sense>
      <xsl:apply-templates/>
    </sense>
  </xsl:template>


  <xsl:template match="sense[attribute::n]">
    <sense>
      <xsl:attribute name="n">
        <xsl:value-of select="@n"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </sense>
  </xsl:template>


  <xsl:template match="lbl[attribute::type=ordering]">
    <xsl:apply-templates/>
  </xsl:template>


  <xsl:template match="def">
    <def>
      <xsl:apply-templates/>
    </def>
  </xsl:template>


  <xsl:template match="lbl">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="cit">
    <cit>
      <xsl:attribute name="type">
        <xsl:text>example</xsl:text>
      </xsl:attribute>
      <xsl:apply-templates/>
    </cit>
  </xsl:template>


  <xsl:template match="q">
    <q>
      <xsl:apply-templates/>
    </q>
  </xsl:template>


  <xsl:template match="quote">
    <!--        <cit>
            <xsl:attribute name="tei:type">
                <xsl:text>verse</xsl:text>
            </xsl:attribute>
        </cit> 
 Soll jedes Zitat von <cit type="verse"> umklammert werden, wie im Skript zur Kernkodierung, S.6? 
            Das würde heißen, dass jedes Zitat in Versform ist, was aber nicht der Fall ist. 
            Andererseits fehlt <cit type="verse"> beim Zitat  auf S. 7 im Skript? -->
    <quote>
      <xsl:apply-templates/>
    </quote>
  </xsl:template>


  <xsl:template match="bibl">
    <bibl>
      <xsl:apply-templates/>
    </bibl>
  </xsl:template>

  <xsl:template match="bibl/author">
    <author>
      <xsl:attribute name="n">
        <xsl:value-of select="@n"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </author>
  </xsl:template>

  <xsl:template match="bibl/title">
    <title>
      <xsl:attribute name="n">
        <xsl:value-of select="@n"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </title>
  </xsl:template>


  <xsl:template match="note">
    <note>
      <xsl:apply-templates/>
    </note>
  </xsl:template>


  <xsl:template match="name">
    <name>
      <xsl:attribute name="n">
        <xsl:value-of select="@n"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </name>
  </xsl:template>


  <xsl:template match="abbr">
    <!-- Soll <abbr> immer ausgegeben werden 
            oder mit Ausnahme der Lemmazeile?-->
    <!--abbr-->
    <xsl:apply-templates/>
    <!--/abbr-->
  </xsl:template>


  <xsl:template match="ref">
    <ref>
      <xsl:attribute name="target">
        <xsl:value-of select="@target"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </ref>
  </xsl:template>



  <xsl:template match="re">
    <re>
      <xsl:apply-templates/>
    </re>
  </xsl:template>

  <xsl:template match="cit/hi">
    <xsl:apply-templates/>
  </xsl:template>

  <xsl:template match="hi">
    <hi>
      <xsl:attribute name="n">
        <xsl:value-of select="@rend"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </hi>
  </xsl:template>


  <xsl:template match="xr">
    <xr>
      <xsl:apply-templates/>
    </xr>
  </xsl:template>

  <xsl:template match="etym">
    <etym>
      <xsl:apply-templates/>
    </etym>
  </xsl:template>


  <xsl:template match="subc">
    <subc>
      <xsl:attribute name="value">
        <xsl:value-of select="@value"/>
      </xsl:attribute>
      <xsl:apply-templates/>
    </subc>
  </xsl:template>


</xsl:stylesheet>
