/**
 * This software is copyright (c) 2016 by
 * 
 * TextGrid Consortium (https://textgrid.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 * 
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (http://www.textgrid.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 */

package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient.tgtest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import jakarta.activation.DataHandler;
import jakarta.xml.bind.JAXB;
import jakarta.xml.ws.Holder;
import org.apache.cxf.helpers.IOUtils;
import info.textgrid.middleware.common.JPairtree;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;

/**
 * <p>
 * This class is an online TG-crud function test class, including eXist original and baseline calls.
 * </p>
 * 
 * @author Stefan E. Funk, DAASI International GmbH
 * @version 2016-12-02
 * @since 2016-11-30
 */

public class TestUtherUpdates {

  private static final String FOLDER = "/Users/fugu/Desktop/uther3";

  /**
   * @param args
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IoFault
   * @throws MetadataParseFault
   */
  public static void main(String[] args) throws IOException, ObjectNotFoundFault, AuthFault,
      ProtocolNotImplementedFault, IoFault, MetadataParseFault {

    readUtherFilesFromDisk(FOLDER + "/uther_agg.txt");
  }

  /**
   * <p>
   * Reads all Uther files from the Textgrid repository using the given URI list, and stores them in
   * the ElasticSearch folders.
   * </p>
   * 
   * @param theUriListFile
   * @throws FileNotFoundException
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IoFault
   * @throws MetadataParseFault
   */
  private static void readUtherFilesFromDisk(String theUriListFile)
      throws FileNotFoundException, IOException, ObjectNotFoundFault, AuthFault,
      ProtocolNotImplementedFault, IoFault, MetadataParseFault {

    //
    // READ UTHER FILES FROM LIST AND DOWNLOAD FROM TGREP.
    //

    TGCrudService tgcrud =
        TGCrudClientUtilities.getTgcrud("https://textgridlab.org/1.0/tgcrud/TGCrudService");

    String uther = IOUtils.toString(new FileInputStream(new File(theUriListFile)));

    String all[] = uther.split("\n");

    // Create directories.
    File d = new File(FOLDER + "/uther4elasticsearch/json/");
    d.mkdirs();
    d = new File(FOLDER + "/uther4elasticsearch/metadata/");
    d.mkdirs();
    d = new File(FOLDER + "/uther4elasticsearch/structure/original/");
    d.mkdirs();
    d = new File(FOLDER + "/uther4elasticsearch/structure/aggregation/");
    d.mkdirs();
    d = new File(FOLDER + "/uther4storage/");
    d.mkdirs();

    for (int i = 0; i < all.length; i++) {
      String line[] = all[i].split("-");
      String u = line[0].trim();
      String uri = "textgrid:" + u;

      System.out.println("Datei " + (i + 1) + ": " + uri);

      // Get JPairTree path and ID, create path.
      JPairtree tree = new JPairtree(uri);
      File ptree = tree.createPPathFolders(new File(FOLDER + "/uther4storage"));

      // Create holders and read data.
      Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
      Holder<DataHandler> dataHolder = new Holder<DataHandler>();

      tgcrud.read("", "", uri, metadataHolder, dataHolder);

      // Backup metadata.
      File backupMeta = new File(ptree, tree.getPtreeEncodedId() + ".meta_20160212");
      JAXB.marshal(metadataHolder.value, backupMeta);

      //
      // Handle metadata (all files).
      //

      System.out.println("----------------------------------------");
      System.out.println("ORIG META:");
      JAXB.marshal(metadataHolder.value, System.out);

      // Already updated on textgridlab.org! Don't do it again! :-)
      // metadataHolder.value.getObject().getGeneric().getProvided().setNotes("Autor der Biographie:
      // Prof. Dr. Hans-Jörg Uther");

      File metaFile = new File(FOLDER + "/uther4elasticsearch/metadata/" + u);

      System.out.println("----------------------------------------");
      System.out.println("NEU META:");
      JAXB.marshal(metadataHolder.value, System.out);

      JAXB.marshal(metadataHolder.value, metaFile);

      // Copy metadata file to storage pairtree path.
      File gMeta = new File(ptree, tree.getPtreeEncodedId() + ".meta");
      copyFile(metaFile, gMeta);

      //
      // Handle data (NO edition files!).
      //

      if (!metadataHolder.value.getObject().getGeneric().getProvided().getFormat()
          .equals("text/tg.edition+tg.aggregation+xml")) {

        // Backup data.
        File backupData = new File(ptree, tree.getPtreeEncodedId() + "_20161202");
        FileOutputStream backupDataStream = new FileOutputStream(backupData);
        dataHolder.value.writeTo(backupDataStream);
        backupDataStream.close();

        String datakrams = IOUtils.readStringFromStream(new FileInputStream(backupData));

        System.out.println("----------------------------------------");
        System.out.println("ORIG DATA:");
        System.out.println(datakrams);

        // Already updated on textgridlab.org! Don't do it again! :-)
        // datakrams = datakrams.replaceFirst("</titleStmt>", " <author>Prof. Dr. Hans-Jörg
        // Uther</author></titleStmt>");

        File dataFile = new File(FOLDER + "/uther4elasticsearch/structure/original/" + u);
        FileWriter data = new FileWriter(dataFile);
        data.write(datakrams);
        data.close();

        System.out.println("----------------------------------------");
        System.out.println("NEU DATA:");
        System.out.println(IOUtils.readStringFromStream(new FileInputStream(dataFile)));

        // Copy data file to storage pairtree path.
        File gData = new File(ptree, tree.getPtreeEncodedId());
        copyFile(dataFile, gData);
      }

      //
      // Handle data (edition files ONLY!).
      //

      else {

        // Backup data.
        File backupData = new File(ptree, tree.getPtreeEncodedId() + "_20161202");
        FileOutputStream backupDataStream = new FileOutputStream(backupData);
        dataHolder.value.writeTo(backupDataStream);
        backupDataStream.close();

        String datakrams = IOUtils.readStringFromStream(new FileInputStream(backupData));

        System.out.println("----------------------------------------");
        System.out.println("ORIG DATA:");
        System.out.println(datakrams);

        // datakrams = datakrams.replaceFirst("</titleStmt>", " <author>Prof. Dr. Hans-Jörg
        // Uther</author> </titleStmt>");

        File dataFile = new File(FOLDER + "/uther4elasticsearch/structure/aggregation/" + u);
        FileWriter data = new FileWriter(dataFile);
        data.write(datakrams);
        data.close();

        System.out.println("----------------------------------------");
        System.out.println("NEU DATA:");
        System.out.println(IOUtils.readStringFromStream(new FileInputStream(dataFile)));

        // Copy data file to storage pairtree path.
        File gData = new File(ptree, tree.getPtreeEncodedId());
        copyFile(dataFile, gData);
      }
    }
  }

  /**
   * @param in
   * @param out
   * @throws IOException
   */
  private static void copyFile(File in, File out) throws IOException {

    FileReader r = new FileReader(in);
    FileWriter w = new FileWriter(out);

    IOUtils.copy(r, w, 4096);

    System.out.println(in.getCanonicalPath() + " >> " + out.getCanonicalPath());

    r.close();
    w.close();
  }

}
