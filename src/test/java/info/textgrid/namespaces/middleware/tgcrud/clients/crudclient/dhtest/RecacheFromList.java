package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient.dhtest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URI;
import java.net.URL;
import java.text.ParseException;
import javax.xml.stream.XMLStreamException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.DHCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;

/**
 * <p>
 * This class re-caches via crud#recache() and wildfly caching.
 * </p>
 * 
 * @author Stefan E Funk (SUB Göttingen)
 */

public class RecacheFromList {

  private static DHCrudService dhcrudClient;

  /**
   * @param args
   * @throws IoFault
   * @throws ParseException
   * @throws IOException
   * @throws XMLStreamException
   */
  public static void main(String[] args)
      throws IoFault, ParseException, IOException, XMLStreamException {

    // PUBLISHED OBJECT. TREP.
    URL crudEndpoint = new URL("https://trep.de.dariah.eu/1.0/dhcrud/");
    String logID = "DHTEST_" + System.currentTimeMillis();

    // Get crud client.
    dhcrudClient = JAXRSClientFactory.create(crudEndpoint.toString(), DHCrudService.class);

    // Get PID list.
    String pidFile = IOUtils.readStringFromStream(
        new FileInputStream(new File("/Users/fugu/Desktop/kofferbilderliste.txt")));
    pidFile = pidFile.replaceAll("hdl:", "");
    String[] pids = pidFile.split("\n");

    // Loop list PIDs.
    int c = 0;
    for (String p : pids) {

      System.out.println((c++) + ". re-caching " + p);

      // Call #re-cache!
      dhcrudClient.recache(URI.create(p), logID);

      try {
        Thread.sleep(836);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
  }

}
