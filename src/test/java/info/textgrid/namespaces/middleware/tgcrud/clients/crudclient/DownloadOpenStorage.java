package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.util.HashMap;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

/**
 *
 */
public class DownloadOpenStorage {

  // private static final int START = 604713; // KAPUTT von 604751 bis 604801 ! 670901 bis 670902 !
  private static final int START = 812066;
  private static final int MAX = 816000;
  private static final String DOWNLOAD_FOLDER = "/Users/fugu/Desktop/openstorage-files-2/";
  private static final String OPENSTORAGE_URL =
      "http://dariah1.scc.kit.edu:8080/StorageImplementation-1.0-SNAPSHOT/";

  private static CloseableHttpClient client = HttpClients.createDefault();

  /**
   * @param args
   * @throws ClientProtocolException
   * @throws IOException
   */
  public static void main(String[] args) throws ClientProtocolException, IOException {
    // 1. Download all from (START)! First two are at 37452 and 47491! Continue from X then! :-)
    downloadAll(START);
    // 2. Update existing ones in folder!
    updateAllFromFolder(new File(DOWNLOAD_FOLDER));
  }


  /**
   * <p>
   * Looks in the OpenStorage download folder, reads all files from OpenStorage using existing IDs
   * and updates them, if necessary. Used for UPDATING all downloaded files. Use downloadAll()
   * first!
   * </p>
   *
   * @throws IOException
   */
  private static void updateAllFromFolder(File folder) throws IOException {
    int i = 0;
    int j = 0;
    if (folder != null) {
      File files[] = folder.listFiles();
      if (files != null) {
        for (File f : files) {
          j++;
          String filename = f.getName();
          try {
            int id = Integer.parseInt(filename.split("\\.")[0]);
            String uri = OPENSTORAGE_URL + id;
            if (!f.getName().endsWith("json")) {
              i++;
              boolean update = checkForLastModified(uri, f.lastModified());
              if (update) {
                downloadFile(uri, id, f.getName().substring(f.getName().lastIndexOf(".")),
                    DOWNLOAD_FOLDER);
              }
              if (i % 1000 == 0) {
                System.out.println("done " + i + "/" + j + " non-json-calls so far!");
              }
            }
          } catch (NumberFormatException e) {
            System.out.println("WARNING! ignoring incorrect filename '" + filename + "'");
          }
        }
        System.out.println("done " + i + "/" + j + " non-json-calls!");
      }
    } else {
      throw new IOException("Folder must not be null!");
    }
  }

  /**
   * <p>
   * Creates a mimetype hash from all the files in the OpenStorage.
   * </p>
   *
   * @throws IOException
   * @throws ClientProtocolException
   */
  private static void getMimetypeHash() throws ClientProtocolException, IOException {

    HashMap<String, Integer> hash = new HashMap<String, Integer>();

    System.out.print("loading");

    for (int i = 0; i < MAX; i++) {
      String uri = OPENSTORAGE_URL + i;

      // Get status and mimetype of response.
      CloseableHttpResponse response = client.execute(new HttpHead(uri));
      int statusCode = response.getStatusLine().getStatusCode();
      if (statusCode == 200) {
        System.out.print(".");
        String mimetype = response.getHeaders("Content-Type")[0].getValue();
        if (!hash.containsKey(mimetype)) {
          System.out.println("\n" + i + ": new mimetype '" + mimetype + "' found");
          hash.put(mimetype, 1);
          System.out.println(hash);
        } else {
          hash.put(mimetype, hash.get(mimetype) + 1);
        }
      }
    }
    System.out.println(hash);
  }

  /**
   * <p>
   * Sorts the mimetype hash from above method to be human readable.
   * </p>
   */
  public static void sortHashString(String all) {

    // Sort hash from created hash string.

    String single[] = all.split(",");

    HashMap<String, Integer> hash = new HashMap<String, Integer>();
    int sum2 = 0;
    for (int i = 0; i < single.length; i++) {
      // Get int from string.
      String number = single[i].substring(single[i].lastIndexOf("=") + 1).trim();
      sum2 += Integer.parseInt(number);

      // Get mimetype.
      String mimetype = "";
      if (single[i].indexOf(";") != -1) {
        mimetype = single[i].substring(0, single[i].indexOf(";")).trim();
      } else {
        mimetype = single[i].substring(0, single[i].indexOf("=")).trim();
      }

      String multi = "multipart/form-data";
      if (mimetype.startsWith(multi)) {
        if (hash.containsKey(multi)) {
          hash.put(multi, (hash.get(multi) + 1));
        } else {
          hash.put(multi, 1);
        }
      }

      hash.put(mimetype, Integer.parseInt(number));
    }

    int sum = 0;
    for (String s : hash.keySet()) {
      System.out.println(s + "," + hash.get(s));
      sum += hash.get(s);
    }

    System.out.println(sum + "=" + sum2);
  }

  /**
   * <p>
   * Downloads all files with a certain mimetype from (start) to MAX. Used for NEW FILES!
   * </p>
   *
   * @throws IOException
   */
  private static void downloadAll(int start) throws IOException {

    // Download certain mimetypes: text/plain, text/csv, text/xml, application/rdf+xml,
    // application/vnd.google-earth.kml+xml, application/xml, application/json
    //
    HashMap<String, String> mimetypes = new HashMap<String, String>();
    mimetypes.put("text/plain", ".txt");
    mimetypes.put("text/csv", ".csv");
    mimetypes.put("text/comma-separated-values", ".csv");
    mimetypes.put("text/xml", ".xml");
    mimetypes.put("application/rdf+xml", ".rdf.xml");
    mimetypes.put("application/vnd.google-earth.kml+xml", ".ge.kml.xml");
    mimetypes.put("application/xml", ".app.xml");
    mimetypes.put("application/json", ".json");
    mimetypes.put("application/vnd.dariahde.geobrowser.csv", ".csv");
    mimetypes.put("application/vnd.ms-excel", ".csv");

    System.out.println("checking and downloading if one of: " + mimetypes);

    int count = 0;
    for (int i = start; i < MAX; i++) {
      String uri = OPENSTORAGE_URL + i;

      if (i % 1000 == 0) {
        System.out.println(i + "/" + MAX + " --> " + count + " matching files found so far");
      }

      CloseableHttpResponse response = client.execute(new HttpHead(uri));
      int statusCode = response.getStatusLine().getStatusCode();

      if (statusCode == 200) {
        String mimetype = response.getHeaders("Content-Type")[0].getValue();
        if (mimetypes.containsKey(mimetype)) {
          downloadFile(uri, i, mimetypes.get(mimetype), DOWNLOAD_FOLDER);
          count++;
        }
      } else if (statusCode == 404) {
        // System.out.println("NOT FOUND [" + uri + "]: " + response.getStatusLine());
      } else {
        System.err.println(" ## ERROR [" + uri + "]: " + response.getStatusLine());
      }
    }
  }

  /**
   * @param uri
   * @param id
   * @param filesuffix
   * @param storeTo
   * @param lastModified
   * @throws IOException
   */
  private static void downloadFile(String uri, int id, String filesuffix, String storeTo)
      throws IOException {

    CloseableHttpResponse response = client.execute(new HttpGet(uri));
    int statusCode = response.getStatusLine().getStatusCode();
    String statusLine = response.getStatusLine().toString();

    long lastModifiedFromHeader = 0;
    String lastModifiedString = "";
    if (response.getHeaders("Last-Modified").length > 0) {
      lastModifiedString = response.getHeaders("Last-Modified")[0].getValue();
      lastModifiedFromHeader = Date.parse(lastModifiedString);
    }
    String contentType = "";
    if (response.getHeaders("Content-Type").length > 0) {
      contentType = response.getHeaders("Content-Type")[0].getValue();
    }
    if (statusCode == 200) {
      File f = new File(storeTo + String.valueOf(id) + filesuffix);
      FileOutputStream fos = new FileOutputStream(f);
      response.getEntity().writeTo(fos);
      long contentLength = f.length();
      fos.close();
      f.setLastModified(lastModifiedFromHeader);
      if (contentLength == 0) {
        f.delete();
      } else {
        System.out
            .println(id + " --> " + contentType + " --> '" + filesuffix + "' --> downloaded --> "
                + lastModifiedString + " --> " + contentLength + " bytes --> " + statusLine);
      }
    } else {
      System.err.println(" ** ERROR [" + uri + " " + id + "]: " + statusLine);
    }
  }

  /**
   * @param uri
   * @param lastModified
   * @return F * @throws IOException
   */
  private static boolean checkForLastModified(String uri, long lastModified)
      throws IOException {

    boolean result = false;

    CloseableHttpResponse response = client.execute(new HttpHead(uri));
    int statusCode = response.getStatusLine().getStatusCode();
    String statusLine = response.getStatusLine().toString();

    if (statusCode == 200) {
      long lastModifiedFromHeader = 0;
      String lastModifiedString = "";
      if (response.getHeaders("Last-Modified").length > 0) {
        lastModifiedString = response.getHeaders("Last-Modified")[0].getValue();
        lastModifiedFromHeader = Date.parse(lastModifiedString);
      }
      if (lastModifiedFromHeader > lastModified) {
        result = true;
      }
    } else {
      System.out.println(" %% ERROR: " + statusLine);
    }

    return result;
  }

}
