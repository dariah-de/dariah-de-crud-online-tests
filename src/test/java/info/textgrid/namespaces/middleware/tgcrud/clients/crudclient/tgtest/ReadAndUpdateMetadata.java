package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient.tgtest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigInteger;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.xml.namespace.QName;
import org.apache.cxf.helpers.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.RiotException;
import org.w3._1999._02._22_rdf_syntax_ns_.RdfType;
import info.textgrid.namespaces.metadata.core._2010.DateType;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.metadata.core._2010.RelationType;
import info.textgrid.namespaces.middleware.tgcrud.clients.crudclientutils.TGCrudClientUtils;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;
import jakarta.activation.DataHandler;
import jakarta.activation.FileDataSource;
import jakarta.xml.bind.JAXB;
import jakarta.xml.bind.JAXBElement;
import jakarta.xml.ws.Holder;

/**
 * <p>
 * Repair data from issue <https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/340>.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 */
public class ReadAndUpdateMetadata {

  public static final String PROPERTIES_FILE = "tgcrud.test.things.properties";
  private static final String MODEL_RDF = "RDF/XML";
  private static final String RDF_NAMESPACE = "http://www.w3.org/1999/02/22-rdf-syntax-ns#";
  private static final String RDF_TAG = "RDF";

  private static String rbacSessionId;
  private static TGCrudService tgcrud;

  /**
   * @param args
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ProtocolNotImplementedFault
   * @throws AuthFault
   * @throws UpdateConflictFault
   */
  public static void main(String[] args) throws IOException, ObjectNotFoundFault,
      MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, UpdateConflictFault {

    if (tgcrud == null) {
      tgcrud = TGCrudClientUtilities.getTgcrud("https://textgridlab.org/1.0/tgcrud/TGCrudService");
    }

    String logParameter = "";
    List<String> uris = new ArrayList<String>();

    // add1999URIs(uris);
    // add1996URIs(uris);

    // Load RBAC Session ID from properties file.
    Properties p = new Properties();
    p.load(new FileInputStream(TGCrudClientUtils.getResource(PROPERTIES_FILE)));
    rbacSessionId = p.getProperty("crud_sessionId");

    // UPDATE METADATA ONLY
    String uri1 = "textgrid:475mh.0";
    String nd1 = "1774";
    MetadataContainerType m1 = tgcrud.readMetadata(rbacSessionId, "", uri1);
    DateType d1 = m1.getObject().getWork().getDateOfCreation();
    if (d1 == null) {
      System.out.println("date for " + uri1 + " is NULL, setting to " + nd1);
      d1 = new DateType();
      d1.setDate(nd1);
      d1.setNotBefore(nd1);
      d1.setNotAfter(nd1);
      m1.getObject().getWork().setDateOfCreation(d1);
      Holder<MetadataContainerType> h1 = new Holder<MetadataContainerType>();
      h1.value = m1;
      tgcrud.updateMetadata(rbacSessionId, "", h1);
    }

    String uri2 = "textgrid:475sk.0";
    String nd2 = "1785";
    MetadataContainerType m2 = tgcrud.readMetadata(rbacSessionId, "", uri2);
    DateType d2 = m2.getObject().getWork().getDateOfCreation();
    if (d2 == null) {
      System.out.println("date for " + uri2 + " is NULL, setting to " + nd2);
      d2 = new DateType();
      d2.setDate(nd2);
      d2.setNotBefore(nd2);
      d2.setNotAfter(nd2);
      m2.getObject().getWork().setDateOfCreation(d2);
      Holder<MetadataContainerType> h2 = new Holder<MetadataContainerType>();
      h2.value = m2;
      tgcrud.updateMetadata(rbacSessionId, "", h2);
    }

    System.exit(0);

    // Create data folder.
    File d = new File("data_" + String.valueOf(System.currentTimeMillis()));
    d.mkdir();

    int invalidRDFCount = 0;
    int count = 0;
    for (String u : uris) {
      BigInteger extent;
      String checksum = "";

      System.out.println();

      count++;

      // READMETADATA.
      System.out.println(u + " -> #READMETADATA [" + count + "]");
      MetadataContainerType metadata = tgcrud.readMetadata(rbacSessionId, logParameter, u);

      // Check relation metadata.
      boolean rdfOK = checkRelationMetadata(metadata);

      if (rdfOK) {
        continue;
      } else {
        invalidRDFCount++;
        System.out.println(u + " -> DO CRUD#UPDATE!");
      }

      // READ.
      System.out.println(u + " -> #READ");
      Holder<MetadataContainerType> metadataHolder = new Holder<MetadataContainerType>();
      Holder<DataHandler> dataHolder = new Holder<DataHandler>();
      tgcrud.read(rbacSessionId, logParameter, u, metadataHolder, dataHolder);

      // JAXB.marshal(metadata.value, System.out);

      extent = metadataHolder.value.getObject().getGeneric().getGenerated().getExtent();
      checksum = metadataHolder.value.getObject().getGeneric().getGenerated().getFixity().get(0)
          .getMessageDigest();
      System.out.println(u + " -> extent: " + extent + " | checksum: " + checksum);

      // Store data file.
      File f = new File(d, u + ".data");
      System.out.println(u + " -> " + dataHolder.value.getContentType() + " -> " + f.getName());
      IOUtils.transferTo(dataHolder.value.getInputStream(), f);

      // Check size and checksum.
      BigInteger dataFileSize = BigInteger.valueOf(f.length());
      if (dataFileSize.longValue() != extent.longValue()) {
        System.out.println(u + " -> ERROR! " + extent + " != " + dataFileSize);
        System.exit(99);
      }

      // Remove Relations from metadata.
      metadataHolder.value.getObject().setRelations(null);
      // JAXB.marshal(metadataHolder.value, System.out);

      // UPDATE.
      System.out.println(u + " -> #UPDATE");
      DataHandler dh = new DataHandler(new FileDataSource(f));
      tgcrud.update(rbacSessionId, logParameter, metadataHolder, dh);

      // READMETADATA.
      MetadataContainerType newMetadata = tgcrud.readMetadata(rbacSessionId, logParameter, u);

      System.out.println(u + " -> #READMETADATA");
      // JAXB.marshal(newMetadata, System.out);

      BigInteger newExtent = newMetadata.getObject().getGeneric().getGenerated().getExtent();
      String newChecksum =
          newMetadata.getObject().getGeneric().getGenerated().getFixity().get(0).getMessageDigest();

      // Check new metadata vs. old metadata.
      if (newExtent.longValue() != extent.longValue()) {
        System.out.println(u + " -> ERROR! " + newExtent + " != " + extent);
        System.exit(98);
      }
      if (!newChecksum.equals(checksum)) {
        System.out.println(u + " -> ERROR! " + newChecksum + " != " + checksum);
        System.exit(97);
      }
      System.out.println(u + " -> UPDATE SUCCESSFULLY COMPLETED!");
      System.out.println(u + " -> [" + newExtent + "==" + extent + "]");
      System.out.println(u + " -> [" + newChecksum + "==" + checksum + "]");
    }

    System.out.println();
    System.out.println("INVALID RDF COUNT: " + invalidRDFCount + "/" + uris.size());
  }

  /**
   * @param theMetadata
   * @return
   */
  private static boolean checkRelationMetadata(MetadataContainerType theMetadata) {

    boolean result;

    String uri = theMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    RelationType relation = theMetadata.getObject().getRelations();

    if (relation != null && relation.getRDF() != null) {
      // Handle generic RDF tag.
      StringWriter rdfWriter = new StringWriter();
      JAXBElement<RdfType> rdfType = new JAXBElement<RdfType>(new QName(RDF_NAMESPACE, RDF_TAG),
          RdfType.class, relation.getRDF());
      // Only read the rdf:RDF tag.
      JAXB.marshal(rdfType, rdfWriter);

      // Create model to create N-TRIPLES: First put RDF/XML in, use TextGrid URI as base for all
      // triples, that do not have a base already (in RDF: about)!
      try {
        Model model = ModelFactory.createDefaultModel();
        model.read(new StringReader(rdfWriter.toString()), uri, MODEL_RDF);
        result = true;
      } catch (RiotException e) {
        result = false;
        System.out.println(uri + " -> RDF metadata INVALID! " + e.getMessage() + "!");
      } finally {
        try {
          rdfWriter.close();
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    } else {
      result = true;
    }

    if (result) {
      System.out.println(uri + " -> RDF metadata OK");
    }

    return result;
  }

  /**
   * @param theURIs
   */
  private static void add1999URIs(List<String> theURIs) {

    /**
     * Images: 1999 Ästhetische Erfahrung als Mimesis
     */

    theURIs.add("textgrid:3wfx7.1");
    theURIs.add("textgrid:3wfx8.1");
    theURIs.add("textgrid:3wfx9.1");
    theURIs.add("textgrid:3wfxb.1");
    theURIs.add("textgrid:3wfxc.1");
    theURIs.add("textgrid:3wfxf.1");
  }

  private static void add1996URIs(List<String> theURIs) {

    /**
     * Images: 1996 Grundfragen ästhetischer Bildung
     */

    theURIs.add("textgrid:3wgv3.1"); // DONE
    theURIs.add("textgrid:3wgv4.1"); // DONE

    theURIs.add("textgrid:3wgv5.1");
    theURIs.add("textgrid:3wgv6.1");
    theURIs.add("textgrid:3wgv7.1");
    theURIs.add("textgrid:3wgv8.1");
    theURIs.add("textgrid:3wgv9.1");
    theURIs.add("textgrid:3wgvb.1");
    theURIs.add("textgrid:3wgvc.1");
    theURIs.add("textgrid:3wgvd.1");
    theURIs.add("textgrid:3wgvf.1");
    theURIs.add("textgrid:3wgvg.1");
    theURIs.add("textgrid:3wgvh.1");
    theURIs.add("textgrid:3wgvj.1");
    theURIs.add("textgrid:3wgvk.1");
    theURIs.add("textgrid:3wgvm.1");
    theURIs.add("textgrid:3wgvn.1");
    theURIs.add("textgrid:3wgvp.1");
    theURIs.add("textgrid:3wgvq.1");
    theURIs.add("textgrid:3wgvr.1");
    theURIs.add("textgrid:3wgvs.1");
    theURIs.add("textgrid:3wgvt.1");
    theURIs.add("textgrid:3wgvv.1");
    theURIs.add("textgrid:3wgvw.1");
    theURIs.add("textgrid:3wgvx.1");
    theURIs.add("textgrid:3wgvz.1");
    theURIs.add("textgrid:3wgw0.1");
    theURIs.add("textgrid:3wgw1.1");
    theURIs.add("textgrid:3wgw2.1");
    theURIs.add("textgrid:3wgw3.1");
    theURIs.add("textgrid:3wgw4.1");
    theURIs.add("textgrid:3wgw5.1");
    theURIs.add("textgrid:3wgw6.1");
    theURIs.add("textgrid:3wgw7.1");
    theURIs.add("textgrid:3wgw8.1");
    theURIs.add("textgrid:3wgw9.1");
    theURIs.add("textgrid:3wgwb.1");
    theURIs.add("textgrid:3wgwc.1");
    theURIs.add("textgrid:3wgwd.1");
    theURIs.add("textgrid:3wgwf.1");
    theURIs.add("textgrid:3wgwg.1");
    theURIs.add("textgrid:3wgwh.1");
    theURIs.add("textgrid:3wgwj.1");
    theURIs.add("textgrid:3wgwk.1");
    theURIs.add("textgrid:3wgwm.1");
    theURIs.add("textgrid:3wgwn.1");
    theURIs.add("textgrid:3wgwp.1");
    theURIs.add("textgrid:3wgwq.1");
    theURIs.add("textgrid:3wgwr.1");
    theURIs.add("textgrid:3wgws.1");
    theURIs.add("textgrid:3wgwt.1");
    theURIs.add("textgrid:3wgwv.1");
    theURIs.add("textgrid:3wgww.1");
    theURIs.add("textgrid:3wgwx.1");
    theURIs.add("textgrid:3wgwz.1");
    theURIs.add("textgrid:3wgx0.1");
    theURIs.add("textgrid:3wgx1.1");
    theURIs.add("textgrid:3wgx2.1");
    theURIs.add("textgrid:3wgx3.1");
    theURIs.add("textgrid:3wgx4.1");
    theURIs.add("textgrid:3wgx5.1");
    theURIs.add("textgrid:3wgx6.1");
    theURIs.add("textgrid:3wgx7.1");
    theURIs.add("textgrid:3wgx8.1");
    theURIs.add("textgrid:3wgx9.1");
    theURIs.add("textgrid:3wgxb.1");
    theURIs.add("textgrid:3wgxc.1");
    theURIs.add("textgrid:3wgxd.1");
    theURIs.add("textgrid:3wgxf.1");
    theURIs.add("textgrid:3wgxg.1");
    theURIs.add("textgrid:3wgxh.1");
    theURIs.add("textgrid:3wgxj.1");
    theURIs.add("textgrid:3wgxk.1");
    theURIs.add("textgrid:3wgxm.1");
    theURIs.add("textgrid:3wgxn.1");
    theURIs.add("textgrid:3wgxp.1");
    theURIs.add("textgrid:3wgxq.1");
    theURIs.add("textgrid:3wgxr.1");
    theURIs.add("textgrid:3wgxs.1");
    theURIs.add("textgrid:3wgxt.1");
    theURIs.add("textgrid:3wgxv.1");
    theURIs.add("textgrid:3wgxw.1");
    theURIs.add("textgrid:3wgxx.1");
    theURIs.add("textgrid:3wgxz.1");
    theURIs.add("textgrid:3wgz0.1");
    theURIs.add("textgrid:3wgz1.1");
    theURIs.add("textgrid:3wgz2.1");
    theURIs.add("textgrid:3wgz3.1");
    theURIs.add("textgrid:3wgz4.1");
    theURIs.add("textgrid:3wgz5.1");
    theURIs.add("textgrid:3wgz6.1");
    theURIs.add("textgrid:3wgz7.1");
    theURIs.add("textgrid:3wgz8.1");
    theURIs.add("textgrid:3wgz9.1");
    theURIs.add("textgrid:3wgzb.1");
    theURIs.add("textgrid:3wgzc.1");
    theURIs.add("textgrid:3wgzd.1");
    theURIs.add("textgrid:3wgzf.1");
    theURIs.add("textgrid:3wgzg.1");
    theURIs.add("textgrid:3wgzh.1");
    theURIs.add("textgrid:3wgzj.1");
    theURIs.add("textgrid:3wgzk.1");
    theURIs.add("textgrid:3wgzm.1");
    theURIs.add("textgrid:3wgzn.1");
    theURIs.add("textgrid:3wgzp.1");
    theURIs.add("textgrid:3wgzq.1");
    theURIs.add("textgrid:3wgzr.1");
    theURIs.add("textgrid:3wgzs.1");
    theURIs.add("textgrid:3wgzt.1");
    theURIs.add("textgrid:3wgzv.1");
    theURIs.add("textgrid:3wgzw.1");
    theURIs.add("textgrid:3wgzx.1");
    theURIs.add("textgrid:3wgzz.1");
    theURIs.add("textgrid:3wh00.1");
    theURIs.add("textgrid:3wh01.1");
    theURIs.add("textgrid:3wh02.1");
    theURIs.add("textgrid:3wh03.1");
    theURIs.add("textgrid:3wh04.1");
    theURIs.add("textgrid:3wh05.1");
    theURIs.add("textgrid:3wh06.1");
    theURIs.add("textgrid:3wh07.1");
    theURIs.add("textgrid:3wh08.1");
    theURIs.add("textgrid:3wh09.1");
    theURIs.add("textgrid:3wh0b.1");
    theURIs.add("textgrid:3wh0c.1");
    theURIs.add("textgrid:3wh0d.1");
    theURIs.add("textgrid:3wh0f.1");
    theURIs.add("textgrid:3wh0g.1");
    theURIs.add("textgrid:3wh0h.1");
    theURIs.add("textgrid:3wh0j.1");
    theURIs.add("textgrid:3wfz6.1");
    theURIs.add("textgrid:3wfz7.1");
    theURIs.add("textgrid:3wfz8.1");
    theURIs.add("textgrid:3wfz9.1");
    theURIs.add("textgrid:3wfzb.1");
    theURIs.add("textgrid:3wfzc.1");
    theURIs.add("textgrid:3wfzd.1");
    theURIs.add("textgrid:3wfzf.1");
    theURIs.add("textgrid:3wfzg.1");
    theURIs.add("textgrid:3rkn9.1");
    theURIs.add("textgrid:3rknb.1");
    theURIs.add("textgrid:3rknc.1");
    theURIs.add("textgrid:3rknd.1");
    theURIs.add("textgrid:3rknf.1");
    theURIs.add("textgrid:3rkng.1");
    theURIs.add("textgrid:3rknh.1");
    theURIs.add("textgrid:3wg16.1");
    theURIs.add("textgrid:3wg17.1");
    theURIs.add("textgrid:3wg18.1");
    theURIs.add("textgrid:3wg19.1");
    theURIs.add("textgrid:3wg1b.1");
    theURIs.add("textgrid:3wg1c.1");
    theURIs.add("textgrid:3wg1d.1");
    theURIs.add("textgrid:3wg1f.1");
    theURIs.add("textgrid:3wg1g.1");
    theURIs.add("textgrid:3wg1h.1");
    theURIs.add("textgrid:3wg59.1");
    theURIs.add("textgrid:3wg5b.1");
    theURIs.add("textgrid:3wg5c.1");
    theURIs.add("textgrid:3wg5d.1");
    theURIs.add("textgrid:3wg5f.1");
    theURIs.add("textgrid:3wg5g.1");
    theURIs.add("textgrid:3wg75.1");
    theURIs.add("textgrid:3wg67.1");
    theURIs.add("textgrid:3wg68.1");
    theURIs.add("textgrid:3wg69.1");
    theURIs.add("textgrid:3wg6b.1");
    theURIs.add("textgrid:3wg6c.1");
    theURIs.add("textgrid:3wg6d.1");
    theURIs.add("textgrid:3wg6f.1");
    theURIs.add("textgrid:3wg6g.1");
    theURIs.add("textgrid:3wg6h.1");
    theURIs.add("textgrid:3wg6j.1");
    theURIs.add("textgrid:3wg6k.1");
    theURIs.add("textgrid:3wg6m.1");
    theURIs.add("textgrid:3wg6n.1");
    theURIs.add("textgrid:3wg6p.1");
    theURIs.add("textgrid:3wg6q.1");
    theURIs.add("textgrid:3wg6r.1");
    theURIs.add("textgrid:3wg6s.1");
    theURIs.add("textgrid:3wg6t.1");
    theURIs.add("textgrid:3wg6v.1");
    theURIs.add("textgrid:3wg6w.1");
    theURIs.add("textgrid:3wh0t.1");
    theURIs.add("textgrid:41mzd.0");
    theURIs.add("textgrid:41cj0.0");
    theURIs.add("textgrid:3whgr.1");
    theURIs.add("textgrid:3r3n6.1");
    theURIs.add("textgrid:3rhnv.1");
    theURIs.add("textgrid:3rhfb.1");
    theURIs.add("textgrid:3wg93.1");
    theURIs.add("textgrid:3wg6x.1");
    theURIs.add("textgrid:3wg94.1");
    theURIs.add("textgrid:3wg6z.1");
    theURIs.add("textgrid:3whgt.1");
    theURIs.add("textgrid:3rkns.1");
    theURIs.add("textgrid:3wg76.1");
    theURIs.add("textgrid:3rhnw.1");
    theURIs.add("textgrid:3rhfc.1");
    theURIs.add("textgrid:3rhnx.1");
    theURIs.add("textgrid:3rhfd.1");
    theURIs.add("textgrid:3wgtw.1");
    theURIs.add("textgrid:3r3qb.1");
    theURIs.add("textgrid:3wfzh.1");
    theURIs.add("textgrid:3wfzj.1");
    theURIs.add("textgrid:3wfzk.1");
    theURIs.add("textgrid:3wfzm.1");
    theURIs.add("textgrid:3wfzn.1");
    theURIs.add("textgrid:3wfzp.1");
    theURIs.add("textgrid:3wfzq.1");
    theURIs.add("textgrid:3wfzr.1");
    theURIs.add("textgrid:3wfzs.1");
    theURIs.add("textgrid:3rkjm.1");
    theURIs.add("textgrid:3rkjn.1");
    theURIs.add("textgrid:3rkjp.1");
    theURIs.add("textgrid:3rkjq.1");
    theURIs.add("textgrid:3rkjr.1");
    theURIs.add("textgrid:3rknj.1");
    theURIs.add("textgrid:3rknk.1");
    theURIs.add("textgrid:3rknm.1");
    theURIs.add("textgrid:3rknn.1");
    theURIs.add("textgrid:3rknp.1");
    theURIs.add("textgrid:3rknq.1");
    theURIs.add("textgrid:3rknr.1");
    theURIs.add("textgrid:3wg21.1");
    theURIs.add("textgrid:3wg22.1");
    theURIs.add("textgrid:3wg23.1");
    theURIs.add("textgrid:3wg24.1");
    theURIs.add("textgrid:3wg25.1");
    theURIs.add("textgrid:3wg26.1");
    theURIs.add("textgrid:3wg27.1");
    theURIs.add("textgrid:3wg28.1");
    theURIs.add("textgrid:3wg29.1");
    theURIs.add("textgrid:3wg2b.1");
    theURIs.add("textgrid:3wg53.1");
    theURIs.add("textgrid:3wg54.1");
    theURIs.add("textgrid:3wg55.1");
    theURIs.add("textgrid:3wg56.1");
    theURIs.add("textgrid:3wg57.1");
    theURIs.add("textgrid:3wg58.1");
    theURIs.add("textgrid:3wgbb.1");
    theURIs.add("textgrid:3wgbc.1");
    theURIs.add("textgrid:3wgbd.1");
    theURIs.add("textgrid:3wgbf.1");
    theURIs.add("textgrid:3wgbg.1");
    theURIs.add("textgrid:3wgbh.1");
    theURIs.add("textgrid:3wgbj.1");
    theURIs.add("textgrid:3wgbk.1");
    theURIs.add("textgrid:3wgbm.1");
    theURIs.add("textgrid:3wgbn.1");
    theURIs.add("textgrid:3wgbp.1");
    theURIs.add("textgrid:3wgbq.1");
    theURIs.add("textgrid:3wgbr.1");
    theURIs.add("textgrid:3wgbs.1");
    theURIs.add("textgrid:3wgbt.1");
    theURIs.add("textgrid:3whgv.1");
    theURIs.add("textgrid:3whgw.1");
    theURIs.add("textgrid:3whgx.1");
    theURIs.add("textgrid:3whgz.1");
    theURIs.add("textgrid:3whh0.1");
    theURIs.add("textgrid:3whh1.1");
    theURIs.add("textgrid:3whh2.1");
    theURIs.add("textgrid:3whh3.1");
    theURIs.add("textgrid:3whh4.1");
    theURIs.add("textgrid:3whh5.1");
    theURIs.add("textgrid:3whh6.1");
    theURIs.add("textgrid:3whh7.1");
    theURIs.add("textgrid:3whh8.1");
    theURIs.add("textgrid:3whh9.1");
    theURIs.add("textgrid:3whhb.1");
    theURIs.add("textgrid:3whhc.1");
    theURIs.add("textgrid:3whhd.1");
    theURIs.add("textgrid:3whhf.1");
    theURIs.add("textgrid:3whhg.1");
    theURIs.add("textgrid:3whhh.1");
    theURIs.add("textgrid:3whhj.1");
    theURIs.add("textgrid:3whhk.1");
    theURIs.add("textgrid:3whhm.1");
    theURIs.add("textgrid:3whhn.1");
    theURIs.add("textgrid:3whhp.1");
    theURIs.add("textgrid:3whhq.1");
    theURIs.add("textgrid:3whhr.1");
    theURIs.add("textgrid:3whhs.1");
    theURIs.add("textgrid:3whht.1");
    theURIs.add("textgrid:3whhv.1");
    theURIs.add("textgrid:3whhw.1");
    theURIs.add("textgrid:3whhx.1");
    theURIs.add("textgrid:3whhz.1");
    theURIs.add("textgrid:3whj0.1");
    theURIs.add("textgrid:3whj1.1");
    theURIs.add("textgrid:3whj2.1");
    theURIs.add("textgrid:3whj3.1");
    theURIs.add("textgrid:3whj4.1");
    theURIs.add("textgrid:3whj5.1");
    theURIs.add("textgrid:3whj6.1");
    theURIs.add("textgrid:3whj7.1");
    theURIs.add("textgrid:3rkjs.1");
    theURIs.add("textgrid:3rkjt.1");
    theURIs.add("textgrid:3wg95.1");
    theURIs.add("textgrid:3wg70.1");
    theURIs.add("textgrid:3wg96.1");
    theURIs.add("textgrid:3wg71.1");
    theURIs.add("textgrid:3wg97.1");
    theURIs.add("textgrid:3wg72.1");
    theURIs.add("textgrid:41cj1.0");
    theURIs.add("textgrid:3whgs.1");
    theURIs.add("textgrid:3r2fb.1");
    theURIs.add("textgrid:3wg73.1");
    theURIs.add("textgrid:3wg98.1");
    theURIs.add("textgrid:3wg74.1");

    /**
     * Audios: 1996 Grundfragen ästhetischer Bildung
     */

    theURIs.add("textgrid:41n3j.0");
    theURIs.add("textgrid:41n1k.0");
    theURIs.add("textgrid:41n1b.0");
    theURIs.add("textgrid:41n14.0");
    theURIs.add("textgrid:41n12.0");
    theURIs.add("textgrid:41n0p.0");
    theURIs.add("textgrid:41n09.0");
    theURIs.add("textgrid:41mzx.0");
    theURIs.add("textgrid:41mzj.0");
    theURIs.add("textgrid:3whnf.1");
    theURIs.add("textgrid:41n3h.0");
    theURIs.add("textgrid:41n3g.0");
    theURIs.add("textgrid:41n3f.0");
    theURIs.add("textgrid:41n3d.0");
    theURIs.add("textgrid:41n3c.0");
    theURIs.add("textgrid:41n3b.0");
    theURIs.add("textgrid:41n39.0");
    theURIs.add("textgrid:3whng.1");
    theURIs.add("textgrid:41n38.0");
    theURIs.add("textgrid:41n37.0");
    theURIs.add("textgrid:41n1j.0");
    theURIs.add("textgrid:41n1h.0");
    theURIs.add("textgrid:41n1g.0");
    theURIs.add("textgrid:41n1f.0");
    theURIs.add("textgrid:41n1d.0");
    theURIs.add("textgrid:41n1c.0");
    theURIs.add("textgrid:41n19.0");
    theURIs.add("textgrid:41n18.0");
    theURIs.add("textgrid:41n17.0");
    theURIs.add("textgrid:41n16.0");
    theURIs.add("textgrid:41n15.0");
    theURIs.add("textgrid:41n11.0");
    theURIs.add("textgrid:41n10.0");
    theURIs.add("textgrid:41n0z.0");
    theURIs.add("textgrid:41n0x.0");
    theURIs.add("textgrid:41n0w.0");
    theURIs.add("textgrid:41n0v.0");
    theURIs.add("textgrid:41n0t.0");
    theURIs.add("textgrid:41n0s.0");
    theURIs.add("textgrid:41n0r.0");
    theURIs.add("textgrid:41n0q.0");
    theURIs.add("textgrid:41n0n.0");
    theURIs.add("textgrid:41n0m.0");
    theURIs.add("textgrid:41n0k.0");
    theURIs.add("textgrid:41n0j.0");
    theURIs.add("textgrid:41n0h.0");
    theURIs.add("textgrid:41n0g.0");
    theURIs.add("textgrid:41n0f.0");
    theURIs.add("textgrid:41n0d.0");
    theURIs.add("textgrid:41n0c.0");
    theURIs.add("textgrid:41n0b.0");
    theURIs.add("textgrid:41n08.0");
    theURIs.add("textgrid:41n07.0");
    theURIs.add("textgrid:41n06.0");
    theURIs.add("textgrid:41n05.0");
    theURIs.add("textgrid:41n04.0");
    theURIs.add("textgrid:41n03.0");
    theURIs.add("textgrid:41n02.0");
    theURIs.add("textgrid:41n01.0");
    theURIs.add("textgrid:41n00.0");
    theURIs.add("textgrid:41mzz.0");
    theURIs.add("textgrid:41mzw.0");
    theURIs.add("textgrid:41mzv.0");
    theURIs.add("textgrid:41mzt.0");
    theURIs.add("textgrid:41mzs.0");
    theURIs.add("textgrid:41mzr.0");
    theURIs.add("textgrid:41mzq.0");
    theURIs.add("textgrid:41mzp.0");
    theURIs.add("textgrid:41mzn.0");
    theURIs.add("textgrid:41mzm.0");
    theURIs.add("textgrid:41mzk.0");
    theURIs.add("textgrid:41mzh.0");
  }

}
