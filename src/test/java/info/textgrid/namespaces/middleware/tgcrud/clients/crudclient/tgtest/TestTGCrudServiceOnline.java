/**
 * This software is copyright (c) 2025 by
 *
 * TextGrid Consortium (https://textgrid.de)
 *
 * DAASI International GmbH (https://daasi.de)
 *
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright TextGrid Consortium (https://textgrid.de)
 * @copyright DAASI International GmbH (https://daasi.de)
 * @license GNU Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient.tgtest;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.stream.XMLStreamException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.http.HttpStatus;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.w3._1999._02._22_rdf_syntax_ns_.RdfType;
import info.textgrid.clients.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Pid;
import info.textgrid.namespaces.metadata.core._2010.GeneratedType.Warning;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgauth.AddMemberRequest;
import info.textgrid.namespaces.middleware.tgauth.AuthenticationFault;
import info.textgrid.namespaces.middleware.tgauth.CreateProjectRequest;
import info.textgrid.namespaces.middleware.tgauth.CreateProjectResponse;
import info.textgrid.namespaces.middleware.tgauth.GetAllProjectsRequest;
import info.textgrid.namespaces.middleware.tgauth.GetRightsRequest;
import info.textgrid.namespaces.middleware.tgauth.IsPublicRequest;
import info.textgrid.namespaces.middleware.tgauth.PortTgextra;
import info.textgrid.namespaces.middleware.tgauth.ProjectInfo;
import info.textgrid.namespaces.middleware.tgauth.RbacFault;
import info.textgrid.namespaces.middleware.tgauth_crud.BooleanResponse;
import info.textgrid.namespaces.middleware.tgauth_crud.NearlyPublishRequest;
import info.textgrid.namespaces.middleware.tgauth_crud.PortTgextraCrud;
import info.textgrid.namespaces.middleware.tgauth_crud.PublishRequest;
import info.textgrid.namespaces.middleware.tgcrud.clients.crudclientutils.TGCrudClientUtils;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;
import jakarta.activation.DataHandler;
import jakarta.ws.rs.client.ClientBuilder;
import jakarta.ws.rs.client.WebTarget;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.xml.bind.JAXB;

/**
 * TODOLOG
 *
 * TODO Add tests for creating and reading TECHMD
 *
 * TODO Add tests for creating and resolving PIDs (READ, READMETADATA, READTECHMD).
 *
 * TODO Check if messaging does work!!
 *
 * TODO Check all relations and make more tests...!!
 *
 * TODO Add ElasticSearch database checks!!
 *
 * TODO Put all DELETES at method's end into a try/finally block!!
 *
 * TODO Add tests for #READ with invalid session IDs and public objects!!
 *
 **
 * CHANGELOG
 *
 * 2024-08-20 - Funk - Add test for invalid RDF relation data.
 * 
 * 2023-03-01 - Funk - Correct some assertion methods, simplified and correctly used now! :-)
 * 
 * 2022-11-22 - Funk - Add test for #302
 *
 * 2022-10-13 - Funk - Add test for #277.
 *
 * 2022-01-14 - Funk - Add PRINT_DATA.
 * 
 * 2021-11-23 - Funk - Remove eXist testing.
 * 
 * 2021-11-08 - Funk - Add test for #36304.
 *
 * 2020-11-02 - Funk - Add test for #33975, concurrent revision creation on the same URI.
 * 
 * 2020-06-17 - Funk - Add portalconfig test.
 *
 * 2020-03-09 - Funk - Add test for EXIF metadata extraction after image UPDATE.
 *
 * 2017-04-07 - Funk - Add tests for updating and deleting revisions. Querying tgsearch for correct
 * revision handling!
 **/

/**
 * <p>
 * This class is an online TG-crud function test class, including eXist original and baseline calls.
 * </p>
 *
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2025-01-16
 * @since 2009-08-26
 **/
public class TestTGCrudServiceOnline {

  // **
  // STATIC FINALS
  // **

  // ** SET PROPERTIES FILE BELOW!
  //
  // private static final String PROPERTIES_FILE = "tgcrud.test.textgridlab-org.properties";
  private static final String PROPERTIES_FILE = "tgcrud.test.dev-textgridlab-org.properties";
  // private static final String PROPERTIES_FILE = "tgcrud.test.test-textgridlab-org.properties";
  //
  // ** SET PROPERTIES FILE ABOVE!

  // DO NOT CHANGE THIS! :-O
  //
  private static final String PRODUCTIVE = "tgcrud.test.textgridlab-org.properties";
  //
  // DO NOT CHANGE THIS! :-O

  private static final String NO_URI = null;
  private static final boolean NEW_REVISION = true;
  // NOTE If deletion is set to false, most tests will fail, because we expect a NotFoundFault! Just
  // use for debugging tests and keeping files.
  // private static final boolean DELETE = false;
  private static final boolean DELETE = true;
  private static boolean TESTTECHMD = true;
  // NOTE Please set to false only if you have NO DIRECT ACCESS to the databases!
  // FIXME USE THIS TESTS AGAIN SOON!!
  private static final boolean CHECK_RDFDB = false;
  // private static final boolean CHECK_ESDB = false;
  // private static final int tgcrudClientTimeout = 120000;
  private static final boolean PRINT_DATA = true;
  private static final boolean PRINT_NO_DATA = false;
  private static final int NO_WARNINGS = 0;
  private static final int ONE_WARNING = 1;

  // **
  // STATICS
  // **

  private static TGCrudService tgcrud;
  private static TGCrudService tgcrudNonMtom;
  private static TGCrudService tgcrudPublic;
  private static WebTarget tgcrudHttpClient;
  private static WebTarget tgcrudHttpClientPublic;
  private static PortTgextra tgauth;
  private static PortTgextraCrud tgauthCrud;
  private static SearchClient tgsearch;
  private static SearchClient tgsearchPublic;
  private static String rbacSessionId;
  private static String rbacSessionI2;
  private static String rbacSessionIdInvalid;
  private static String logParameter;
  private static String tgauthCrudSecret;
  private static Long automagicUnlockingTime;
  private static String projectId;
  private static String projectI2;
  private static String projectIdNoAccess;
  private static String projectIdInvalid;
  private static String junitProject1;
  private static String junitProject2;
  private static String junitEppn1;
  private static String junitEppn2;
  private static String sesame;
  private static String createTinyTiffImageFilename;
  private static String createSmallTiffImageFilename;
  private static String createSmallJpegImageFilename;
  private static String createJpegImageMetadataFilename;
  private static String createTiffImageMetadataFilename;
  private static String createFuguWaffelfuttererFilename;
  private static String createFuguWaffelfuttererMetadataFilename;
  private static String createTiffImageMetadataRDFFilename;
  private static String createHugeMP4Filename;
  private static String createHugeMP4MetadataFilename;
  private static String createTiffImagePublicMetadataFilename;
  private static String createMetadataNoMetadata;
  private static String createMetadataWithoutGenericType;
  private static String createMetadataWithoutProvidedType;
  private static String createMetadataWithoutTitleFilename;
  private static String createMetadataWithoutFormatFilename;
  private static String createEmptyFileFilename;
  private static String createEmptyFileMetadataFilename;
  private static String createTestFileFilename;
  private static String createTestFileMetadataFilename;
  private static String createTestFileObjectMetadataFilename;
  private static String createTestFileEltecValidMetadataFilename;
  private static String createTestFileEltecInvalidDateMetadataFilename;
  private static String createTestFileEltecInvalidAgentMetadataFilename;
  private static String createTestFileEltecInvalidGenreMetadataFilename;
  private static String createXmlCampeMetadataFilename;
  private static String createXmlCampeFilename;
  private static String createXmlWertherMetadataFilename;
  private static String createXmlWertherFilename;
  private static String createXmlCampeAdaptorMetadataFilename;
  private static String createXmlCampeAdaptorFilename;
  private static String createXmlWertherAdaptorMetadataFilename;
  private static String createXmlWertherAdaptorFilename;
  private static String createXmlTEISchemaExtractionFilename;
  private static String createXmlTEISchemaExtractionMetadataFilename;
  private static String createAggregationFilename;
  private static String createAggregationMetadataFilename;
  private static String createAggregationPublicMetadataFilename;
  private static String createSpecialAggregationFilename;
  private static String createSpecialAggregationMetadataFilename;
  private static String createXsdFilename;
  private static String createXsdWithoutNamespaceFilename;
  private static String createXsdMetadataFilename;
  private static String createXsdMetadataWithRelationsFilename;
  private static String createTinyXmlFilename;
  private static String createTinyXmlMetadataFilename;
  private static String createSmallXmlFilename;
  private static String createSmallXmlMetadataFilename;
  private static String createLinkfileFilename;
  private static String createLinkfileMetadataFilename;
  private static String createFaultyLinkfileFilename;
  private static String createNonWellformedXmlFilename;
  private static String createWorkMetadataFilename;
  private static String createWorkMetadataFilenameDateOnly;
  private static String createWorkMetadataFilenameCompleteDate;
  private static String createWorkMetadataFilenameNotBeforeNotAfter;
  private static String createWorkMetadataFilenameNoDateAttribute;
  private static String createWorkFilename;
  private static String createItem1MetadataFilename;
  private static String createItem1Filename;
  private static String createItem2MetadataFilename;
  private static String createItem2Filename;
  private static String createEditionMetadataFilename;
  private static String createEditionFilename;
  private static String createLolFilename;
  private static String createLolMetadataFilename;
  private static String createProjectfileFilename;
  private static String createProjectfileMetadataFilename;
  private static String createPortalconfigFilename;
  private static String createPortalconfigMetadataFilename;
  private static String createItemWithoutItemTagMetadataFilename;

  private static String readUnknownResource;
  private static String readPid;

  private static String updateAggregationFilename;
  private static String updateAggregationMetadataFilename;
  private static String updateSpecialAggregationFilename;
  private static String updateSpecialAggregationMetadataFilename;
  private static String updateEmptyFileMetadataFilename;
  private static String updateTestFileFilename;
  private static String updateTestFileMetadataFilename;
  private static String updateErrorTestFileMetadataFilename;
  private static String updateXmlCampeAdaptorMetadataFilename;
  private static String updateXmlCampeAdaptorFilename;
  private static String updateTiffImageMetadataFilename;

  private static String updateMetadataTiffImageMetadataFilename;
  private static String updateMetadataAggregationMetadataFilename;
  private static String updateSpecialMetadataAggregationMetadataFilename;
  private static String updateMetadataTinyXmlMetadataFilename;

  private static String createBugTG1061Filename;
  private static String createBugTG1061MetadataFilename;
  private static String updateBugTG1061XmlFilename;
  private static String updateBugTG1061XmlMetadataFilename;
  private static String createBugTG1534MetadataFilename;
  private static String updateMetadataBugTG1534MetadataFilename;
  private static String updateBugTG1534MetadataFilename;
  private static String createBugTG1158Filename;
  private static String createBugTG1158MetadataFilename;
  private static String createBugTG1171Filename;
  private static String createBugTG1171MetadataFilename;
  private static String createBugTG1157Filename;
  private static String createBugTG1157MetadataFilename;
  private static String createBugTG1860MetadataFilename;
  private static String createBug36304EditionMetadataFilename;
  private static String createBug36304CollectionMetadataFilename;
  private static String readBugTG1929ObjectUri;
  private static String loadtestsSmallFilename;
  private static String loadtestsSmallMetadataFilename;
  private static String loadtestsMediumFilename;
  private static String loadtestsMediumMetadataFilename;
  private static String loadtestsLargeFilename;
  private static String loadtestsLargeMetadataFilename;
  private static String createBug9442MetadataFilename;
  private static String createSomeUTF8CharsFilename;
  private static String createSomeUTF8CharsMetadataFilename;
  private static String updateBug302MetadataFilename;
  private static String createBug340MetadataFilename;
  private static String createBug340ImageFilename;
  private static String updateBug340MetadataFilename;

  private static String crudPublishSecret;
  private static String userRoleBearbeiter = "Bearbeiter";
  private static String userRoleAdministrator = "Administrator";

  // **
  // PREPARATIONS
  // **

  /**
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws Exception {

    System.out.println("System's default charset: " + Charset.defaultCharset().displayName());

    // Load properties file.
    Properties p =
        new Properties();
    p.load(new FileInputStream(TGCrudClientUtils.getResource(PROPERTIES_FILE)));

    System.out.println("Properties file: " + PROPERTIES_FILE);

    // Get TG-crud MTOM stub.
    System.out.println("Getting TG-crud MTOM stub...");
    tgcrud = TGCrudClientUtilities.getTgcrud(p.getProperty("crud_serviceEndpointUrl").trim(), true);
    System.out.println("...ok");

    // Get TG-crud REST endpoint and HTTP client.
    System.out.println("Getting TG-crud HTTP client...");
    String tgcrudHttpClientUrl = p.getProperty("crud_rest_serverUrl").trim();
    tgcrudHttpClient = ClientBuilder.newClient().property("thread.safe.client", "true")
        .target(tgcrudHttpClientUrl);
    System.out.println("...ok");

    // Get TG-crud REST public endpoint and public HTTP client.
    System.out.println("Getting TG-crud public HTTP client...");
    String tgcrudPublicHttpClientUrl = p.getProperty("crud_public_rest_serverUrl").trim();
    tgcrudHttpClientPublic = ClientBuilder.newClient().property("thread.safe.client", "true")
        .target(tgcrudPublicHttpClientUrl);
    System.out.println("...ok");

    // Get TG-crud NON-MTOM stub.
    System.out.println("Getting TG-crud NON-MTOM stub...");
    tgcrudNonMtom =
        TGCrudClientUtilities.getTgcrud(p.getProperty("crud_serviceEndpointUrl").trim(), false);
    System.out.println("...ok");

    // Get public MTOM stub.
    System.out.println("Getting TG-crud public MTOM stub...");
    tgcrudPublic =
        TGCrudClientUtilities.getTgcrud(p.getProperty("crud_public_serviceEndpointUrl"), true);
    System.out.println("...ok");

    // Get other needed things.
    rbacSessionId = p.getProperty("crud_sessionId");
    rbacSessionI2 = p.getProperty("crud_sessionI2");
    junitProject1 = p.getProperty("junit_project1");
    junitProject2 = p.getProperty("junit_project2");
    junitEppn1 = p.getProperty("junit_eppn1");
    junitEppn2 = p.getProperty("junit_eppn2");
    rbacSessionIdInvalid = p.getProperty("crud_sessionId_invalid");
    logParameter = p.getProperty("crud_logParameter");
    tgauthCrudSecret = p.getProperty("tgauth_crudSecret");
    automagicUnlockingTime = Long.parseLong(p.getProperty("automagicUnlockingTime"));
    projectIdNoAccess = p.getProperty("getUri_projectIdNoAccess");
    projectIdInvalid = p.getProperty("getUri_projectIdInvalid");

    // Get the TG-auth stubs.
    URL authLocation = new URL(p.getProperty("auth_serviceEndpointUrl"));
    URL authLocation_crud = new URL(p.getProperty("auth_crud_serviceEndpointUrl"));

    tgauth = TGCrudClientUtils.getTgauthService(authLocation);
    tgauthCrud = TGCrudClientUtils.getTgauthCrudService(authLocation_crud);

    // Getting tgsearch clients.
    System.out.println("Getting TG-search client...");

    tgsearch = new SearchClient(p.getProperty("search_serverUrl"));
    System.out.println("...ok");

    System.out.println("Getting TG-search public client...");
    tgsearchPublic = new SearchClient(p.getProperty("search_public_serverUrl"));
    System.out.println("...ok");

    // Get things for ElasticSearch.
    // elasticsearch = p.getProperty("elasticsearch_serverUrl");
    // elasticsearchUsername = p.getProperty("elasticsearch_username");
    // elasticsearchPassword = p.getProperty("elasticsearch_password");

    // Get things for Sesame.
    sesame = p.getProperty("sesame_serverUrl");

    // Get properties for #CREATE.
    createTinyTiffImageFilename = p.getProperty("create_tinyTiffImageFilename");
    createSmallTiffImageFilename = p.getProperty("create_smallTiffImageFilename");
    createTiffImageMetadataFilename = p.getProperty("create_tiffImageMetadataFilename");
    createFuguWaffelfuttererFilename = p.getProperty("create_fuguWaffelfuttererFilename");
    createFuguWaffelfuttererMetadataFilename =
        p.getProperty("create_fuguWaffelfuttererMetadataFilename");
    createTiffImageMetadataRDFFilename = p.getProperty("create_tiffImageMetadataRDFFilename");
    createSmallJpegImageFilename = p.getProperty("create_smallJpegImageFilename");
    createJpegImageMetadataFilename = p.getProperty("create_jpegImageMetadataFilename");
    createHugeMP4Filename = p.getProperty("create_hugeMP4Filename");
    createHugeMP4MetadataFilename = p.getProperty("create_hugeMP4MetadataFilename");
    createTiffImagePublicMetadataFilename = p.getProperty("create_tiffImagePublicMetadataFilename");
    createMetadataNoMetadata = p.getProperty("create_noMetadataFile");
    createMetadataWithoutProvidedType = p.getProperty("create_metadataFilenameWithoutProvided");
    createMetadataWithoutGenericType = p.getProperty("create_metadataFilenameWithoutGeneric");
    createMetadataWithoutTitleFilename = p.getProperty("create_metadataFilenameWithoutTitle");
    createMetadataWithoutFormatFilename = p.getProperty("create_metadataFilenameWithoutFormat");
    createEmptyFileFilename = p.getProperty("create_emptyFileFilename");
    createEmptyFileMetadataFilename = p.getProperty("create_emptyFileMetadataFilename");
    createTestFileFilename = p.getProperty("create_testFileFilename");
    createTestFileMetadataFilename = p.getProperty("create_testFileMetadataFilename");
    createTestFileObjectMetadataFilename = p.getProperty("create_testFileObjectMetadataFilename");
    createTestFileEltecValidMetadataFilename =
        p.getProperty("create_testFileEltecValidMetadataFilename");
    createTestFileEltecInvalidDateMetadataFilename =
        p.getProperty("create_testFileEltecInvalidDateMetadataFilename");
    createTestFileEltecInvalidAgentMetadataFilename =
        p.getProperty("create_testFileEltecInvalidAgentMetadataFilename");
    createTestFileEltecInvalidGenreMetadataFilename =
        p.getProperty("create_testFileEltecInvalidGenreMetadataFilename");
    createXmlCampeFilename = p.getProperty("create_xmlCampeFilename");
    createXmlCampeMetadataFilename = p.getProperty("create_xmlCampeMetadataFilename");
    createXmlCampeAdaptorFilename = p.getProperty("create_xmlCampeAdaptorFilename");
    createXmlCampeAdaptorMetadataFilename = p.getProperty("create_xmlCampeAdaptorMetadataFilename");
    createXmlWertherFilename = p.getProperty("create_xmlWertherFilename");
    createXmlWertherMetadataFilename = p.getProperty("create_xmlWertherMetadataFilename");
    createXmlWertherAdaptorFilename = p.getProperty("create_xmlWertherAdaptorFilename");
    createXmlWertherAdaptorMetadataFilename =
        p.getProperty("create_xmlWertherAdaptorMetadataFilename");
    createTinyXmlFilename = p.getProperty("create_tinyXmlFilename");
    createTinyXmlMetadataFilename = p.getProperty("create_tinyXmlMetadataFilename");
    createXmlTEISchemaExtractionFilename = p.getProperty("create_xmlTEISchemaExtractionFilename");
    createXmlTEISchemaExtractionMetadataFilename =
        p.getProperty("create_xmlTEISchemaExtractionMetadataFilename");
    createSmallXmlFilename = p.getProperty("create_smallXmlFilename");
    createSmallXmlMetadataFilename = p.getProperty("create_smallXmlMetadataFilename");
    createXsdFilename = p.getProperty("create_xsdFilename");
    createXsdWithoutNamespaceFilename = p.getProperty("create_xsdWithoutNamespaceFilename");
    createXsdMetadataFilename = p.getProperty("create_xsdMetadataFilename");
    createAggregationFilename = p.getProperty("create_aggregationFilename");
    createAggregationMetadataFilename = p.getProperty("create_aggregationMetadataFilename");
    createAggregationPublicMetadataFilename =
        p.getProperty("create_aggregationPublicMetadataFilename");
    createSpecialAggregationFilename = p.getProperty("create_specialAggregationFilename");
    createSpecialAggregationMetadataFilename =
        p.getProperty("create_specialAggregationMetadataFilename");
    createXsdMetadataWithRelationsFilename =
        p.getProperty("create_xsdMetadataWithRelationsFilename");
    createLinkfileFilename = p.getProperty("create_linkfileFilename");
    createFaultyLinkfileFilename = p.getProperty("create_faultyLinkfileFilename");
    createLinkfileMetadataFilename = p.getProperty("create_linkfileMetadataFilename");
    createNonWellformedXmlFilename = p.getProperty("create_nonWellformedXmlFilename");
    createLolFilename = p.getProperty("create_lolFilename");
    createLolMetadataFilename = p.getProperty("create_lolMetadataFilename");
    createProjectfileFilename = p.getProperty("create_projectfileFilename");
    createProjectfileMetadataFilename = p.getProperty("create_projectfileMetadataFilename");
    createPortalconfigFilename = p.getProperty("create_portalconfigFilename");
    createPortalconfigMetadataFilename = p.getProperty("create_portalconfigMetadataFilename");
    createSomeUTF8CharsFilename = p.getProperty("create_someUTF8CharsFilename");
    createSomeUTF8CharsMetadataFilename = p.getProperty("create_someUTF8CharsMetadataFilename");
    createItemWithoutItemTagMetadataFilename =
        p.getProperty("create_itemWithoutItemTagMetadataFilename");

    // #CREATE Edition/Work.
    // Ingest the work.
    createWorkMetadataFilename = p.getProperty("create_workMetadataFilename");
    createWorkMetadataFilenameDateOnly = p.getProperty("create_workMetadataFilenameDateOnly");
    createWorkMetadataFilenameCompleteDate =
        p.getProperty("create_workMetadataFilenameCompleteDate");
    createWorkMetadataFilenameNotBeforeNotAfter =
        p.getProperty("create_workMetadataFilenameNotBeforeNotAfter");
    createWorkMetadataFilenameNoDateAttribute =
        p.getProperty("create_workMetadataFilenameNoDateAttribute");
    createWorkFilename = p.getProperty("create_workFilename");
    createItem1MetadataFilename = p.getProperty("create_item1MetadataFilename");
    createItem1Filename = p.getProperty("create_item1Filename");
    createItem2MetadataFilename = p.getProperty("create_item2MetadataFilename");
    createItem2Filename = p.getProperty("create_item2Filename");
    createEditionFilename = p.getProperty("create_editionFilename");
    createEditionMetadataFilename = p.getProperty("create_editionMetadataFilename");

    // Get properties for #READ.
    readUnknownResource = p.getProperty("read_unknownResource");
    readPid = p.getProperty("read_pid");

    // Get properties for #UPDATEMETADATA.
    updateMetadataTiffImageMetadataFilename =
        p.getProperty("updateMetadata_tiffImageMetadataFilename");
    updateMetadataAggregationMetadataFilename =
        p.getProperty("updateMetadata_aggregationMetadataFilename");
    updateSpecialMetadataAggregationMetadataFilename =
        p.getProperty("updateMetadata_specialAggregationMetadataFilename");
    updateMetadataTinyXmlMetadataFilename = p.getProperty("updateMetadata_tinyXmlMetadataFilename");

    // Get properties for #UPDATE.
    updateAggregationFilename = p.getProperty("update_aggregationFilename");
    updateAggregationMetadataFilename = p.getProperty("update_aggregationMetadataFilename");
    updateSpecialAggregationFilename = p.getProperty("update_specialAggregationFilename");
    updateSpecialAggregationMetadataFilename =
        p.getProperty("update_specialAggregationMetadataFilename");
    updateEmptyFileMetadataFilename = p.getProperty("update_emptyFileMetadataFilename");
    updateTestFileFilename = p.getProperty("update_testFileFilename");
    updateTestFileMetadataFilename = p.getProperty("update_testFileMetadataFilename");
    updateErrorTestFileMetadataFilename = p.getProperty("update_errorTestFileMetadataFilename");
    updateXmlCampeAdaptorFilename = p.getProperty("update_xmlCampeAdaptorFilename");
    updateXmlCampeAdaptorMetadataFilename = p.getProperty("update_xmlCampeAdaptorMetadataFilename");
    updateTiffImageMetadataFilename = p.getProperty("update_tiffImageMetadataFilename");

    // Get properties for bugs.
    createBugTG1061Filename = p.getProperty("create_bugTG1061Filename");
    createBugTG1061MetadataFilename = p.getProperty("create_bugTG1061MetadataFilename");
    updateBugTG1061XmlFilename = p.getProperty("update_bugTG1061Filename");
    updateBugTG1061XmlMetadataFilename = p.getProperty("update_bugTG1061MetadataFilename");
    createBugTG1158Filename = p.getProperty("create_bugTG1158Filename");
    createBugTG1158MetadataFilename = p.getProperty("create_bugTG1158MetadataFilename");
    createBugTG1171Filename = p.getProperty("create_bugTG1171Filename");
    createBugTG1171MetadataFilename = p.getProperty("create_bugTG1171MetadataFilename");
    createBugTG1157Filename = p.getProperty("create_bugTG1157Filename");
    createBugTG1157MetadataFilename = p.getProperty("create_bugTG1157MetadataFilename");
    createBugTG1860MetadataFilename = p.getProperty("create_bugTG1860MetadataFilename");
    createBugTG1534MetadataFilename = p.getProperty("create_bugTG1534MetadataFilename");
    updateMetadataBugTG1534MetadataFilename =
        p.getProperty("updateMetadata_bugTG1534MetadataFilename");
    updateBugTG1534MetadataFilename = p.getProperty("update_bugTG1534MetadataFilename");
    readBugTG1929ObjectUri = p.getProperty("read_bugTG1929ObjectUri");
    loadtestsSmallFilename = p.getProperty("create_loadtestsSmall");
    loadtestsSmallMetadataFilename = p.getProperty("create_loadtestsSmallMetadata");
    loadtestsMediumFilename = p.getProperty("create_loadtestsMedium");
    loadtestsMediumMetadataFilename = p.getProperty("create_loadtestsMediumMetadata");
    loadtestsLargeFilename = p.getProperty("create_loadtestsLarge");
    loadtestsLargeMetadataFilename = p.getProperty("create_loadtestsLargeMetadata");
    createBug9442MetadataFilename = p.getProperty("create_bug9442MetadataFilename");
    createBug36304EditionMetadataFilename = p.getProperty("create_bug36304EditionMetadataFilename");
    createBug36304CollectionMetadataFilename =
        p.getProperty("create_bug36304CollectionMetadataFilename");
    updateBug302MetadataFilename = p.getProperty("update_bug302MetadataFilename");
    createBug340MetadataFilename = p.getProperty("create_bug340MetadataFilename");
    createBug340ImageFilename = p.getProperty("create_bug340ImageFilename");
    updateBug340MetadataFilename = p.getProperty("update_bug340MetadataFilename");

    // Get properties for #MOVESTATIC.
    crudPublishSecret = p.getProperty("tgcrud_publishSecret");

    // TRIM THE SESSION IDS!
    rbacSessionId = rbacSessionId.trim();
    rbacSessionI2 = rbacSessionI2.trim();

    // Create two JUnit projects using tgextra, if not yet existing.
    GetAllProjectsRequest allProjetsRequest =
        new GetAllProjectsRequest();
    allProjetsRequest.setAuth(rbacSessionId);
    allProjetsRequest.setLog(logParameter);
    List<ProjectInfo> allProjects = tgauth.getAllProjects(allProjetsRequest).getProject();

    System.out.println("Checking for JUNIT test projects " + junitProject1 + " and " + junitProject2
        + " out of " + allProjects.size());

    boolean project1Existing = false;
    boolean project2Existing = false;

    for (ProjectInfo pi : allProjects) {
      if (pi.getName().equals(junitProject1)) {
        project1Existing = true;
        projectId = pi.getId();
      }
      if (pi.getName().equals(junitProject2)) {
        project2Existing = true;
        projectI2 = pi.getId();
      }
    }

    if (project1Existing) {
      System.out.println("\tProject " + junitProject1 + " already exists: " + projectId);
    } else {
      // Create project.
      System.out.println("\tCreating project " + junitProject1);

      CreateProjectRequest junitProject1Request = new CreateProjectRequest();
      junitProject1Request.setAuth(rbacSessionId);
      junitProject1Request.setLog(logParameter);
      junitProject1Request.setName(junitProject1);
      junitProject1Request.setDescription(junitProject1);
      CreateProjectResponse project1Response = tgauth.createProject(junitProject1Request);
      projectId = project1Response.getProjectId();

      System.out.println("\tProject created: " + projectId);

      try {
        // Add user1 to project1 as Bearbeiter and Administrator.
        AddMemberRequest addMemberRequest = new AddMemberRequest();
        String role = userRoleBearbeiter + "," + projectId + ",Projekt-Teilnehmer";
        addMemberRequest.setAuth(rbacSessionId);
        addMemberRequest.setLog(logParameter);
        addMemberRequest.setRole(role);
        addMemberRequest.setUsername(junitEppn1);
        tgauth.addMember(addMemberRequest);

        role = userRoleAdministrator + "," + projectId + ",Projekt-Teilnehmer";
        addMemberRequest.setRole(role);
        tgauth.addMember(addMemberRequest);

        System.out.println("\tAdded user " + junitEppn1 + " to project " + junitProject1
            + " with roles " + userRoleBearbeiter + " and " + userRoleAdministrator);
      } catch (RbacFault e) {
        System.out.println("\tRBAC Fault: " + e.getMessage());
      }
    }

    if (project2Existing) {
      System.out.println("\tProject " + junitProject2 + " already exists: " + projectI2);
    } else {
      System.out.println("\tCreating project " + junitProject2);

      CreateProjectRequest junitProject2Request = new CreateProjectRequest();
      junitProject2Request.setAuth(rbacSessionI2);
      junitProject2Request.setLog(logParameter);
      junitProject2Request.setName(junitProject2);
      junitProject2Request.setDescription(junitProject2);

      // System.out.println("*auth:" + junitProject2Request.getAuth() +
      // "*");
      // System.out.println("*logg:" + junitProject2Request.getLog() +
      // "*");
      // System.out.println("*name:" + junitProject2Request.getName() +
      // "*");
      // System.out.println("*file:" + junitProject2Request.getFile() +
      // "*");
      // System.out.println(
      // "*desc:" + junitProject2Request.getDescription() + "*");

      CreateProjectResponse project2Response = tgauth.createProject(junitProject2Request);
      projectI2 = project2Response.getProjectId();

      System.out.println("\tProject created: " + projectI2);

      try {
        // Add user2 to project2 as Bearbeiter and Administrator.
        AddMemberRequest addMemberRequest = new AddMemberRequest();
        String role = userRoleBearbeiter + "," + projectI2 + ",Projekt-Teilnehmer";
        addMemberRequest.setAuth(rbacSessionI2);
        addMemberRequest.setLog(logParameter);
        addMemberRequest.setRole(role);
        addMemberRequest.setUsername(junitEppn2);
        tgauth.addMember(addMemberRequest);

        role = userRoleAdministrator + "," + projectI2 + ",Projekt-Teilnehmer";
        addMemberRequest.setRole(role);
        tgauth.addMember(addMemberRequest);

        System.out.println("\tAdded user " + junitEppn2 + " to project " + junitProject2
            + " with roles " + userRoleBearbeiter + " and " + userRoleAdministrator);

        // Add user2 to project1 as Bearbeiter.
        role = userRoleBearbeiter + "," + projectId + ",Projekt-Teilnehmer";
        addMemberRequest.setAuth(rbacSessionId);
        addMemberRequest.setRole(role);
        addMemberRequest.setUsername(junitEppn2);
        tgauth.addMember(addMemberRequest);

        System.out.println("\tAdded user " + junitEppn2 + " to project " + junitProject1
            + " with role " + userRoleBearbeiter);
      } catch (RbacFault e) {
        System.out.println("\tRBAC Fault: " + e.getMessage());
      }
    }
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * <p>
   * Runs a getVersion test.
   * </p>
   */
  @Test
  public void testGetVersion() {

    System.out.println("Testing #GETVERSION");

    System.out.println("\tVersion is: " + TGCrudClientUtils.getVersion(tgcrud));
  }

  /**
   * <p>
   * Runs a getVersion test using REST.
   * </p>
   *
   * @throws IOException
   */
  @Test
  public void testGetVersionREST() throws IOException {

    System.out.println("Testing #GETVERSION via REST");

    Response response = tgcrudHttpClient.path("version").request().get();
    String version = response.readEntity(String.class);

    assertFalse(version == null || version.equals(""));

    System.out.println("\tVersion is: " + version);
  }

  /**
   * <p>
   * Runs a getVersion test using REST public.
   * </p>
   *
   * @throws IOException
   */
  @Test
  public void testGetVersionPublicREST() throws IOException {

    System.out.println("Testing #GETVERSION via REST");

    Response response = tgcrudHttpClientPublic.path("version").request().get();
    String version = response.readEntity(String.class);

    assertFalse(version == null || version.equals(""));

    System.out.println("\tVersion is: " + version);
  }

  /**
   * <p>
   * Runs a getUri test with invalid session ID.
   * </p>
   *
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   */
  @Test(expected = AuthFault.class)
  public void testGetUriNoAccess() throws AuthFault, IOException, ObjectNotFoundFault, IoFault {

    System.out.println("Testing #GETURI with invalid SID");

    TGCrudClientUtils.getUri(tgcrud, "", logParameter, projectIdNoAccess, 2);
  }

  /**
   * <p>
   * Runs a getUri test with 4 URIs and valid project ID on tgcrud non-public.
   * </p>
   *
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   */
  @Test
  public void testGetUri() throws IOException, ObjectNotFoundFault, IoFault, AuthFault {

    System.out.println("Testing #GETURI from non-public TG-crud");

    TGCrudClientUtils.getUri(tgcrud, rbacSessionId, logParameter, projectId, 4);
  }

  /**
   * <p>
   * Runs a getUri test with 4 URIs and valid project ID on tgcrud non-public via REST.
   * </p>
   *
   * @throws IOException
   * @throws IoFault
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   */
  @Test
  public void testGetUriREST() throws IOException, ObjectNotFoundFault, AuthFault, IoFault {

    System.out.println("Testing #GETURI from non-public TG-crud via REST");

    WebTarget target = tgcrudHttpClient.path("getUri").queryParam("sessionId", rbacSessionId)
        .queryParam("logParameter", logParameter).queryParam("howMany", 3);

    System.out.println("\tQuery URL: " + target.getUri());

    Response response = target.request().get();
    String uris = response.readEntity(String.class);

    assertFalse(uris == null || uris.isEmpty());

    System.out.println("\tURIs created: " + uris.replaceAll("\n", ", "));
  }

  /**
   * <p>
   * Runs a getUri test with 4 URIs and valid project ID on tgcrud public.
   * </p>
   *
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   */
  @Test
  public void testGetUriPublic() throws IOException, ObjectNotFoundFault, IoFault, AuthFault {

    System.out.println("Testing #GETURI from public TG-crud");

    TGCrudClientUtils.getUri(tgcrudPublic, rbacSessionId, logParameter, projectId, 4);
  }

  /**
   * <p>
   * Runs a getUri test with one single URIs and valid project ID, to do a #CREATE with that URI
   * used. The URI is not existing.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   */
  @Test(expected = IoFault.class)
  public void testCreateWithInvalidUri()
      throws IOException, ObjectNotFoundFault, IoFault, AuthFault, MetadataParseFault {

    System.out.println("Testing #CREATE with invalid URI");

    TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileMetadataFilename, createTestFileFilename, "textgrid:URGL:ARGL:AUA",
        NO_WARNINGS);
  }

  /**
   * <p>
   * Tries creating an object with invalid metadata (no <tgObjectMetadata>).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   */
  @Test
  public void testCreateWithObjectTypeOnlyMetadataREST()
      throws IOException, ObjectNotFoundFault, IoFault, AuthFault, MetadataParseFault,
      RelationsExistFault {

    System.out.println("Testing #CREATE with <object> type only metadata (text/plain)");

    TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileObjectMetadataFilename, createTestFileFilename,
        Status.BAD_REQUEST.getStatusCode(), NO_WARNINGS);
  }

  /**
   * <p>
   * Tries creating an object with invalid metadata (incorrect dateOfPublication).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   */
  @Test
  public void testCreateWithEltecInvalidDateMetadataREST() throws IOException, ObjectNotFoundFault,
      IoFault, AuthFault, MetadataParseFault, RelationsExistFault {

    System.out.println("Testing #CREATE with invalid eltec date metadata");

    TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileEltecInvalidDateMetadataFilename, createTestFileFilename,
        Status.INTERNAL_SERVER_ERROR.getStatusCode(), NO_WARNINGS);
  }

  /**
   * <p>
   * Tries creating an object with invalid metadata (incorrect genre).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   */
  @Test
  public void testCreateWithEltecInvalidGenreMetadataREST() throws IOException, ObjectNotFoundFault,
      IoFault, AuthFault, MetadataParseFault, RelationsExistFault {

    System.out.println("Testing #CREATE with invalid eltec genre metadata");

    MetadataContainerType metadata = TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch,
        rbacSessionId, logParameter, projectId, createTestFileEltecInvalidGenreMetadataFilename,
        createEmptyFileFilename, Status.OK.getStatusCode(), ONE_WARNING);

    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.deleteREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
          HttpStatus.SC_NOT_FOUND, PRINT_NO_DATA);
    }
  }

  /**
   * <p>
   * Tries creating an object with invalid metadata (incorrect dateOfPublication).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   */
  @Test(expected = IoFault.class)
  public void testCreateWithEltecInvalidDateMetadata() throws IOException, ObjectNotFoundFault,
      IoFault, AuthFault, MetadataParseFault, RelationsExistFault {

    System.out.println("Testing #CREATE with invalid eltec date metadata");

    TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileEltecInvalidDateMetadataFilename, createTestFileFilename, NO_WARNINGS);
  }

  /**
   * <p>
   * Tries creating an object with invalid metadata (incorrect agentID).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   */
  @Test
  public void testCreateWithEltecInvalidAgentIDMetadataREST() throws IOException,
      ObjectNotFoundFault, IoFault, AuthFault, MetadataParseFault, RelationsExistFault {

    System.out.println("Testing #CREATE with invalid eltec agent id metadata");

    // CREATE.
    MetadataContainerType metadata = TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch,
        rbacSessionId, logParameter, projectId, createTestFileEltecInvalidAgentMetadataFilename,
        createTestFileFilename, Status.OK.getStatusCode(), 1);
    // One warning, because we do not ingest an aggregation file! :-D
    // TODO Test with real aggregation file!

    // Status code is OK here, because we do not get any warnings, client marshaling takes out the
    // false agent ID already!
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // READMETADATA.
    metadata =
        TGCrudClientUtils.readMetadataREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);

    // DELETE and READ.
    if (DELETE) {
      TGCrudClientUtils.deleteREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
          HttpStatus.SC_NOT_FOUND, PRINT_NO_DATA);
    }
  }

  /**
   * <p>
   * Tries creating an object with invalid metadata (correct dateOfPublication).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   */
  @Test
  public void testCreateWithEltecValidMetadataREST() throws IOException, ObjectNotFoundFault,
      IoFault, AuthFault, MetadataParseFault, RelationsExistFault {

    System.out.println("Testing #CREATE with valid eltec metadata");

    MetadataContainerType metadata = TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch,
        rbacSessionId, logParameter, projectId, createTestFileEltecValidMetadataFilename,
        createTestFileFilename, Status.OK.getStatusCode(), 1);
    // One warning, because we do not ingest an aggregation file! :-D
    // TODO Test with real aggregation file!

    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.deleteREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
          HttpStatus.SC_NOT_FOUND, PRINT_NO_DATA);
    }
  }

  /**
   * <p>
   * Tries creating an object with empty metadata.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   */
  @Test
  public void testCreateWithNullMetadataREST() throws IOException, ObjectNotFoundFault, IoFault,
      AuthFault, MetadataParseFault, RelationsExistFault {

    System.out.println("Testing #CREATE with empty metadata");

    TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch, rbacSessionId, logParameter, projectId,
        createEmptyFileFilename, createTestFileFilename, Status.BAD_REQUEST.getStatusCode(),
        NO_WARNINGS);
  }

  /**
   * <p>
   * Runs a getUri test with one single URIs and valid project ID, to do a #CREATE with that URI
   * used, and do a create again, to get the second failure.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   */
  @Test(expected = IoFault.class)
  public void testCreateWithExistingUri() throws IOException, ObjectNotFoundFault, IoFault,
      AuthFault, MetadataParseFault, RelationsExistFault {

    System.out.println("Testing #CREATE with already used URI");

    String uri = TGCrudClientUtils.getUri(tgcrud, rbacSessionId, logParameter, projectId, 1).get(0);
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename, uri,
        NO_WARNINGS);
    // Get the URI from the response metadata.
    uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    // Again try to create another object with the same (already existing)
    // URI.
    TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileMetadataFilename, createTestFileFilename, uri, NO_WARNINGS);
    // Delete the created file again.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test a #CREATE with a revision URI. TG-crud should create a new object with the initial URI
   * suffix ".0".
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   */
  @Test
  public void testCreateWithRevisionUri() throws IOException, ObjectNotFoundFault, IoFault,
      AuthFault, MetadataParseFault, RelationsExistFault {

    System.out.println("Testing #CREATE with a revision URI");

    // Get an URI.
    String uri = TGCrudClientUtils.getUri(tgcrud, rbacSessionId, logParameter, projectId, 1).get(0);
    // Append revision to your liking (!= 0, should be stripped by TG-crud).
    uri += ".3";
    // Call #CREATE.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename, uri,
        NO_WARNINGS);

    // Get new revision number.
    String newUri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    int newRevisionNumber = Integer.parseInt(newUri.substring(newUri.lastIndexOf('.') + 1));

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, newUri);
    }

    // Check new revision number.
    assertFalse(newRevisionNumber != 0);
  }

  /**
   * <p>
   * Runs a getUri test with one single URI and valid project ID, to do a #CREATE with that URI
   * used.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testGetUriAndCreate()
      throws IOException, ObjectNotFoundFault, IoFault, AuthFault, MetadataParseFault,
      ProtocolNotImplementedFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing #CREATE with valid URI");

    String uri = TGCrudClientUtils.getUri(tgcrud, rbacSessionId, logParameter, projectId, 1).get(0);
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename, uri,
        NO_WARNINGS);
    // Get the URI from the response metadata.
    uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs a getUri test with 0 URIs requested.
   * </p>
   *
   * @throws IoFault
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  @Test(expected = IoFault.class)
  public void testGetUriZeroUrisWanted()
      throws IoFault, IOException, ObjectNotFoundFault, AuthFault {

    System.out.println("Testing #GETURI with 0 URIs requested");

    TGCrudClientUtils.getUri(tgcrud, rbacSessionId, logParameter, projectId, 0);
  }

  /**
   * <p>
   * Runs a getUri test with -1 URIs requested.
   * </p>
   *
   * @throws IoFault
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   */
  @Test(expected = IoFault.class)
  public void testGetUriNegativeUrisWanted()
      throws IoFault, IOException, ObjectNotFoundFault, AuthFault {

    System.out.println("Testing #GETURI with negative amount of URIs");

    TGCrudClientUtils.getUri(tgcrud, rbacSessionId, logParameter, projectId, -1);
  }

  /**
   * <p>
   * Tests resolving of Handle PIDs via TG-crud.
   * </p>
   *
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testPidReadResolving() throws IOException, ObjectNotFoundFault, MetadataParseFault,
      IoFault, ProtocolNotImplementedFault, AuthFault, NoSuchAlgorithmException {

    if (!readPid.equals("none") && !readPid.equals("")) {
      System.out.println("Testing #READ with PID: " + readPid);

      TGCrudClientUtils.read(tgcrud, "", logParameter, readPid);
    }
  }

  /**
   * <p>
   * Tests resolving of Handle PIDs via TG-crud using REST.
   * </p>
   *
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   */
  @Test
  public void testPidReadResolvingREST() throws IOException, ObjectNotFoundFault,
      MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault {

    if (!readPid.equals("none") && !readPid.equals("")) {
      System.out.println("Testing #READ REST with PID: " + readPid);

      TGCrudClientUtils.readREST(tgcrudHttpClient, "", logParameter, readPid, HttpStatus.SC_OK,
          PRINT_DATA);
    }
  }

  /**
   * <p>
   * Tests reading a unknown URI.
   * </p>
   *
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   */
  @Test
  public void testReadWithErrorREST() throws IOException, ObjectNotFoundFault,
      MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault {

    String uri = "textgrid:fuguoO0"; // O and 0 will not exist in a valid TextGrid URI! See NOID
                                     // ID minting!

    TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
        HttpStatus.SC_NOT_FOUND, PRINT_DATA);
  }

  /**
   * <p>
   * Tests resolving of Handle PIDs via TG-crud.
   * </p>
   *
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   */
  @Test
  public void testPidReadMetadataResolving() throws IOException, ObjectNotFoundFault,
      MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault {

    if (!readPid.equals("none") && !readPid.equals("")) {
      System.out.println("Testing #READMETADATA with PID: " + readPid);

      TGCrudClientUtils.readMetadata(tgcrud, "", logParameter, readPid);
    }
  }

  /**
   * <p>
   * Tests resolving of Handle PIDs via TG-crud using REST.
   * </p>
   *
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   */
  @Test
  public void testPidReadMetadataResolvingREST() throws IOException, ObjectNotFoundFault,
      MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault {

    if (!readPid.equals("none") && !readPid.equals("")) {
      System.out.println("Testing #READMETADATA REST with  PID: " + readPid);

      TGCrudClientUtils.readMetadataREST(tgcrudHttpClient, "", logParameter, readPid);
    }
  }

  /**
   * <p>
   * Runs all basic TG-crud tests using a tiny TIFF image and test file for update. Uses MTOM stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCrudBasicsWithTinyImageFile() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud basics (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update and read the metadata.
    TGCrudClientUtils.updateMetadata(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateMetadataTiffImageMetadataFilename, metadata.getObject().getGeneric().getGenerated());
    metadata = TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);

    // TODO Check new metadata!

    // Update and read all.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // TODO Check new data!

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs a TG-crud UPDATE with corrupt image RDF metadata (bug #340).
   * </p>
   * 
   * NOTE: Does not work somehow... seems we can't get the invalid RDF into the metadata...
   * 
   * @throws FileNotFoundException
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Ignore
  @Test(expected = ObjectNotFoundFault.class)
  public void testCrudUpdateImageBug340() throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, UpdateConflictFault,
      NoSuchAlgorithmException, ProtocolNotImplementedFault, RelationsExistFault {

    System.out.println("Testing TG-crud update image bug 340 (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createBug340MetadataFilename, createBug340ImageFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // TODO Check new metadata!

    // Update and read all.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateBug340MetadataFilename, createBug340ImageFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    try {
      // Do READ.
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    } finally {
      // Delete, and READ again.
      if (DELETE) {
        TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
        TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
      }
    }
  }

  /**
   * <p>
   * Test #CREATE with some UTF-8 chars (SOAP).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCrudCreateWithUTF8() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud #CREATE and #READ with some UTF-8 chars (file size: "
        + TGCrudClientUtils.getResource(createSomeUTF8CharsFilename).length() + " bytes)");

    // Check content on disk.
    String data = IOUtils.readStringFromStream(
        new FileInputStream(TGCrudClientUtils.getResource(createSomeUTF8CharsFilename)));

    System.out.println("DATA__disk content:");
    System.out.println(data);

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createSomeUTF8CharsMetadataFilename, createSomeUTF8CharsFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // READ.
    DataHandler handler = TGCrudClientUtils.simpleRead(tgcrud, rbacSessionId, logParameter, uri);

    // Check content after reading.
    data = IOUtils.readStringFromStream(handler.getInputStream());

    System.out.println("DATA__after reading:");
    System.out.println(data);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test #CREATE with some UTF-8 chars REST).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCrudCreateWithUTF8REST() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud #CREATE and #READ with some UTF-8 chars REST (file size: "
        + TGCrudClientUtils.getResource(createSomeUTF8CharsFilename).length() + " bytes)");

    // Check content on disk.
    String data = IOUtils.readStringFromStream(
        new FileInputStream(TGCrudClientUtils.getResource(createSomeUTF8CharsFilename)));

    System.out.println("DATA__disk content:");
    System.out.println(data);

    // Create object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch,
        rbacSessionId, logParameter, projectId, createSomeUTF8CharsMetadataFilename,
        createSomeUTF8CharsFilename, Status.OK.getStatusCode(), NO_WARNINGS);
    // TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
    // create_someUTF8CharsMetadataFilename, create_someUTF8CharsFilename);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // READ REST.
    TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
        Status.OK.getStatusCode(), PRINT_DATA);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs update with checking EXIF metadata (bug#15326).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testEXIFMetadataWithTinyImageFile() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud EXIF extraction (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Check RDF width/height metadata.
    StringWriter relationDataWriter = new StringWriter();
    JAXB.marshal(metadata.getObject().getRelations().getRDF(), relationDataWriter);
    boolean exifDataExisting = relationDataWriter.toString().contains(
        "<exif:height ns3:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">328</exif:height>")
        && relationDataWriter.toString().contains(
            "<exif:width ns3:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">258</exif:width>");
    assertTrue("EXIF metadata not corect!", exifDataExisting);
    relationDataWriter.close();

    // Update and read all.
    metadata = TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateTiffImageMetadataFilename, createSmallJpegImageFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Check RDF width/height metadata again.
    relationDataWriter = new StringWriter();
    JAXB.marshal(metadata.getObject().getRelations().getRDF(), relationDataWriter);
    exifDataExisting = relationDataWriter.toString().contains(
        "<exif:height ns3:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">1640</exif:height>")
        && relationDataWriter.toString().contains(
            "<exif:width ns3:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">1291</exif:width>");
    assertTrue("EXIF metadata not corect!", exifDataExisting);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs create with relations content (bug#302), see
   * <https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/302>
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testRelationTypeCreate() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud relation type create");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, updateBug302MetadataFilename, createXmlWertherFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Check for relations metadata.
    String isDerivedFrom = metadata.getObject().getRelations().getIsDerivedFrom();
    String isAlternativeFormatOf = metadata.getObject().getRelations().getIsAlternativeFormatOf();
    String hasSchema = metadata.getObject().getRelations().getHasSchema();
    boolean missingRelationMetadata =
        isDerivedFrom == null || isDerivedFrom.equals("") || isAlternativeFormatOf == null
            || isAlternativeFormatOf.equals("") || hasSchema == null || hasSchema.equals("");
    assertFalse("Relations are missing after #CREATE!", missingRelationMetadata);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs update with new relations content (bug#302), see
   * <https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/302>
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testRelationTypeUpdate() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud relation type update");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createXmlWertherMetadataFilename, createXmlWertherFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update and read all.
    metadata = TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateBug302MetadataFilename, createXmlWertherFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Check for relations metadata.
    String isDerivedFrom = metadata.getObject().getRelations().getIsDerivedFrom();
    String isAlternativeFormatOf = metadata.getObject().getRelations().getIsAlternativeFormatOf();
    String hasSchema = metadata.getObject().getRelations().getHasSchema();
    if (isDerivedFrom == null || isDerivedFrom.equals("")
        || isAlternativeFormatOf == null || isAlternativeFormatOf.equals("")
        || hasSchema == null || hasSchema.equals("")) {
      assertTrue("Relations are missing after #UPDATE!", false);
    }

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Creates new revision with checking EXIF metadata (bug#35593).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   * @see https://projects.gwdg.de/projects/tg/work_packages/35593
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testEXIFMetadataWithNewRevision() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud EXIF extraction with new revision (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Test for correct RDF:description part.
    String rdfString = null;
    try (StringWriter relationDataWriter = new StringWriter()) {
      JAXB.marshal(metadata.getObject().getRelations().getRDF(), relationDataWriter);
      rdfString = relationDataWriter.toString().trim();
      relationDataWriter.close();
    }

    String expectedRDFString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
        "<rdfType xmlns:ns2=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n" +
        "    <rdf:Description ns3:about=\"" + uri
        + "\" xmlns=\"http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ns2=\"http://textgrid.info/namespaces/metadata/core/2010\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:exif=\"http://www.w3.org/2003/12/exif/ns#\" xmlns:ns3=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
        +
        "        <exif:height ns3:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">328</exif:height>\n"
        +
        "        <exif:width ns3:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">258</exif:width>\n"
        +
        "  </rdf:Description>\n" +
        "</rdfType>";

    if (!(rdfString.equals(expectedRDFString))) {
      System.out.println("EXIF metadata not corect!");
      System.out.println(rdfString);
      System.out.println("!=");
      System.out.println(expectedRDFString);
      assertTrue(false);
    }

    // Create new revision with larger TIFF image file and metadata from CREATE response and edited
    // RDF:description about attribute.
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTiffImageMetadataRDFFilename, createSmallTiffImageFilename, uri, NEW_REVISION,
        NO_WARNINGS);
    uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Test for correct RDF:description part.

    try (StringWriter relationDataWriter = new StringWriter()) {
      JAXB.marshal(metadata.getObject().getRelations().getRDF(), relationDataWriter);
      rdfString = relationDataWriter.toString().trim();
      relationDataWriter.close();
    }

    expectedRDFString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
        "<rdfType xmlns:ns2=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n" +
        "    <rdf:Description ns3:about=\"" + uri
        + "\" xmlns=\"http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ns2=\"http://textgrid.info/namespaces/metadata/core/2010\" xmlns:dc=\"http://purl.org/dc/elements/1.1/\" xmlns:exif=\"http://www.w3.org/2003/12/exif/ns#\" xmlns:ns3=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
        +
        "        <exif:height ns3:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">1640</exif:height>\n"
        +
        "        <exif:width ns3:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">1291</exif:width>\n"
        +
        "  </rdf:Description>\n" +
        "</rdfType>";

    if (!(rdfString.equals(expectedRDFString))) {
      System.out.println("EXIF metadata not corect!");
      System.out.println(rdfString);
      System.out.println("!=");
      System.out.println(expectedRDFString);
      assertTrue(false);
    }

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Creates new revision with checking stylesheet and schema information (bug#31709).
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   * @see https://projects.gwdg.de/projects/tg/work_packages/31709
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testStylesheetAndSchemaMetadataWithNewRevision()
      throws IOException, XMLStreamException, MetadataParseFault, AuthFault, IoFault,
      ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault, RelationsExistFault,
      NoSuchAlgorithmException {

    System.out.println("Testing TG-crud Schema extraction with new revision (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createXmlTEISchemaExtractionMetadataFilename, createXmlTEISchemaExtractionFilename,
            NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Test for correct RDF:description part.
    String rdfString = null;
    try (StringWriter relationDataWriter = new StringWriter()) {
      JAXB.marshal(metadata.getObject().getRelations().getRDF(), relationDataWriter);
      rdfString = relationDataWriter.toString().trim();
      relationDataWriter.close();
    }

    String expectedRDFString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
        "<rdfType xmlns:ns2=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n" +
        "    <rdf:Description ns3:about=\"" + uri
        + "\" xmlns=\"http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService\" xmlns:tg=\"http://textgrid.info/relation-ns#\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ns2=\"http://textgrid.info/namespaces/metadata/core/2010\" xmlns:ns3=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
        +
        "        <tg:rootElementLocalPart>TEI</tg:rootElementLocalPart>\n" +
        "        <tg:rootElementNamespace ns3:resource=\"http://www.tei-c.org/ns/1.0\"/>\n" +
        "  </rdf:Description>\n" +
        "</rdfType>";

    if (!(rdfString.equals(expectedRDFString))) {
      System.out.println("Root element local part and namespace RDF metadata not corect!");
      System.out.println(rdfString);
      System.out.println("!=");
      System.out.println(expectedRDFString);
      assertTrue(false);
    }

    // Create new revision with larger TIFF image file and metadata from CREATE response and edited
    // RDF:description about attribute.
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createXmlTEISchemaExtractionMetadataFilename, createXmlTEISchemaExtractionFilename, uri,
        NEW_REVISION, NO_WARNINGS);
    uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Test for correct RDF:description part.
    try (StringWriter relationDataWriter = new StringWriter()) {
      JAXB.marshal(metadata.getObject().getRelations().getRDF(), relationDataWriter);
      rdfString = relationDataWriter.toString().trim();
      relationDataWriter.close();
    }

    expectedRDFString = "<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n" +
        "<rdfType xmlns:ns2=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n" +
        "    <rdf:Description ns3:about=\"" + uri
        + "\" xmlns=\"http://textgrid.info/namespaces/middleware/tgcrud/services/TGCrudService\" xmlns:tg=\"http://textgrid.info/relation-ns#\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ns2=\"http://textgrid.info/namespaces/metadata/core/2010\" xmlns:ns3=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
        +
        "        <tg:rootElementLocalPart>TEI</tg:rootElementLocalPart>\n" +
        "        <tg:rootElementNamespace ns3:resource=\"http://www.tei-c.org/ns/1.0\"/>\n" +
        "  </rdf:Description>\n" +
        "</rdfType>";

    if (!(rdfString.equals(expectedRDFString))) {
      System.out.println("Root element local part and namespace RDF metadata not corect!");
      System.out.println(rdfString);
      System.out.println("!=");
      System.out.println(expectedRDFString);
      assertTrue(false);
    }

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Testing: Objects with deleted revisions cannot be revisioned again (Bug #17289)
   * </p>
   *
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testRevisionDeleteAndCreate() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud revisioning and deleting and revisioning (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Ingest first file.
    MetadataContainerType metadata0 = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
        NO_WARNINGS);
    String uri0 = metadata0.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri0);

    // Ingest and read first revision.
    String uri1 = createRevision(uri0);
    // Check URI.
    if (!uri1.contains(".1")) {
      System.out.println("URI is " + uri1 + ", revision should be 1");
      assertTrue(false);
    }
    // Ingest and read second revision.
    String uri2 = createRevision(uri1);
    // Check URI.
    if (!uri2.contains(".2")) {
      System.out.println("URI is " + uri2 + ", revision should be 2");
      assertTrue(false);
    }

    // Delete last revision.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri2);
    }

    // Ingest and read next revision from current revision (.1).
    String uri3 = createRevision(uri1);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri2);
    // Check URI.
    if (!uri2.contains(".3")) {
      System.out.println("URI is " + uri2 + ", revision should be 3");
      assertTrue(false);
    }

    // Delete all .0, .1, .2, and .3 revisions, and READ latest.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri0);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri1);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri2);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri3);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri3);
    }
  }

  /**
   * <p>
   * Runs a #CREATE with a projectfile.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateWithProjectfile() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud projectfile creation (file size: "
        + TGCrudClientUtils.getResource(createProjectfileFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createProjectfileMetadataFilename, createProjectfileFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs a #CREATE with a portalconfig file.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateWithPrortalconfigFile() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud portalconfig file creation (file size: "
        + TGCrudClientUtils.getResource(createPortalconfigFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createPortalconfigMetadataFilename, createPortalconfigFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs a TG-crud#READ with invalid session ID. Uses MTOM stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testReadWithInvalidSessionId() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#READ with invalid session ID (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read all.
    try {
      TGCrudClientUtils.read(tgcrud, rbacSessionIdInvalid, logParameter, uri);
    } catch (AuthFault e) {
      throw e;
    } finally {
      // Delete, and READ.
      if (DELETE) {
        TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
        TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
      }
    }
  }

  /**
   * <p>
   * Runs a TG-crud#CREATE with invalid session ID. Uses MTOM stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   */
  @Test(expected = AuthFault.class)
  public void testCreateWithInvalidSessionId()
      throws IOException, XMLStreamException, MetadataParseFault, AuthFault, IoFault,
      ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault, RelationsExistFault {

    System.out.println("Testing TG-crud#CREATE with invalid session ID (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create TextGrid object, get metadata and URI.
    TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionIdInvalid, logParameter, projectId,
        createTiffImageMetadataFilename, createTinyTiffImageFilename, NO_WARNINGS);
  }

  /**
   * <p>
   * Runs a TG-crud#READ with invalid session ID via REST.
   * </p>
   *
   * @throws IOException
   */
  @Test
  public void testReadWithInvalidSessionIdREST() throws IOException {

    System.out.println("Testing TG-crud#READ with invalid session ID (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes) via REST");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch,
        rbacSessionId, logParameter, projectId, createTiffImageMetadataFilename,
        createTinyTiffImageFilename, HttpStatus.SC_OK, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read data only.
    TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionIdInvalid, logParameter, uri,
        HttpStatus.SC_UNAUTHORIZED, PRINT_NO_DATA);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.deleteREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
          HttpStatus.SC_NOT_FOUND, PRINT_NO_DATA);
    }
  }

  /**
   * <p>
   * Runs all basic TG-crud tests using a tiny TIFF image and test file for update via REST.
   * </p>
   *
   * @throws IOException
   */
  @Test
  public void testCrudBasicsWithTinyImageFileREST() throws IOException {

    System.out.println("Testing TG-crud basics (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes) via REST");

    // #CREATE a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch,
        rbacSessionId, logParameter, projectId, createTiffImageMetadataFilename,
        createTinyTiffImageFilename, HttpStatus.SC_OK, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // #READMETADATA.
    metadata =
        TGCrudClientUtils.readMetadataREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);

    // #UPDATEMETADATA.
    TGCrudClientUtils.updateMetadataREST(tgcrudHttpClient, tgsearch, rbacSessionId, logParameter,
        updateMetadataTiffImageMetadataFilename, metadata.getObject().getGeneric().getGenerated());

    // #READMETADATA.
    metadata =
        TGCrudClientUtils.readMetadataREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);

    // TODO Check new metadata!

    // #UPDATE.
    TGCrudClientUtils.updateREST(tgcrudHttpClient, tgsearch, rbacSessionId, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // #READ.
    TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
        HttpStatus.SC_OK, PRINT_NO_DATA);

    // #READMETADATA.
    metadata =
        TGCrudClientUtils.readMetadataREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);

    // TODO Check new data!

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.deleteREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
          HttpStatus.SC_NOT_FOUND, PRINT_NO_DATA);
    }
  }

  /**
   * <p>
   * Runs all basic TG-crud tests using a tiny XML and test file for update via REST.
   * </p>
   *
   * @throws IOException
   */
  @Test
  public void testCrudUpdateErrorWithTinyXmlFileREST() throws IOException {

    System.out.println("Testing TG-crud update error (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes) via REST");

    String uri = "";
    try {
      // Create a first TextGrid object, get metadata and URI.
      MetadataContainerType metadata = TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch,
          rbacSessionId, logParameter, projectId, createTinyXmlMetadataFilename,
          createTinyXmlFilename, HttpStatus.SC_OK, NO_WARNINGS);
      uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

      // Try update.
      TGCrudClientUtils.updateREST(tgcrudHttpClient, tgsearch, rbacSessionId, logParameter,
          updateErrorTestFileMetadataFilename, updateTestFileFilename,
          metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    } finally {
      // Delete, and READ.
      if (!uri.isEmpty() && DELETE) {
        TGCrudClientUtils.deleteREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);
        TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
            HttpStatus.SC_NOT_FOUND, PRINT_NO_DATA);
      }
    }
  }

  /**
   * <p>
   * Runs all basic TG-crud tests using a tiny XML and test file for update via REST.
   * </p>
   *
   * @throws IOException
   */
  @Test
  public void testCrudBasicsWithTinyXmlFileREST() throws IOException {

    System.out.println("Testing TG-crud basics (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes) via REST");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata =
        TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch, rbacSessionId, logParameter,
            projectId, createTinyXmlMetadataFilename, createTinyXmlFilename, HttpStatus.SC_OK,
            NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read the metadata.
    metadata =
        TGCrudClientUtils.readMetadataREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);

    // Update and read the metadata.
    TGCrudClientUtils.updateMetadataREST(tgcrudHttpClient, tgsearch, rbacSessionId, logParameter,
        updateMetadataTinyXmlMetadataFilename, metadata.getObject().getGeneric().getGenerated());
    metadata =
        TGCrudClientUtils.readMetadataREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);

    // TODO Check new metadata!

    // Update.
    TGCrudClientUtils.updateREST(tgcrudHttpClient, tgsearch, rbacSessionId, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Read data and readMetadata again.
    TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
        HttpStatus.SC_OK, PRINT_NO_DATA);
    metadata =
        TGCrudClientUtils.readMetadataREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);

    // TODO Check new data!

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.deleteREST(tgcrudHttpClient, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.readREST(tgcrudHttpClient, rbacSessionId, logParameter, uri,
          HttpStatus.SC_NOT_FOUND, PRINT_NO_DATA);
    }
  }

  /**
   * <p>
   * Runs a #CREATE with invalid sessionId via REST.
   * </p>
   *
   * @throws IOException
   */
  @Test
  public void testCreateWithInvalidSessionIdREST() throws IOException {

    System.out.println("Testing TG-crud#CREATE with invalid session ID (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes) via REST");

    // Create TextGrid object, get metadata and URI.
    TGCrudClientUtils.createREST(tgcrudHttpClient, tgsearch, rbacSessionIdInvalid, logParameter,
        projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        HttpStatus.SC_UNAUTHORIZED, NO_WARNINGS);
  }

  /**
   * <p>
   * Runs a simple TG-crud#CREATE with a tiny TIFF file. Uses MTOM stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateTinyTiffFile() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with a tiny TIFF file (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs a simple TG-crud#CREATE with a small TIFF file. Uses MTOM stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateSmallTiffFile() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with a small TIFF file (file size: "
        + TGCrudClientUtils.getResource(createSmallTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createSmallTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs a simple TG-crud#CREATE with a small JPEG file. Uses MTOM stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateSmallJpegFile() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with a small JPEG file (file size: "
        + TGCrudClientUtils.getResource(createSmallJpegImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createJpegImageMetadataFilename, createSmallJpegImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs a simple TG-crud#CREATE with a tiny XML file. Uses MTOM stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateTinyXmlFile() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with a tiny XML file (file size: "
        + TGCrudClientUtils.getResource(createTinyXmlFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTinyXmlMetadataFilename, createTinyXmlFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs a simple TG-crud#CREATE with a tiny non-well-formed XML file. Uses MTOM stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateTinyXmlFileNonWellformed() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with a tiny non-wellformed XML file (file size: "
        + TGCrudClientUtils.getResource(createNonWellformedXmlFilename).length() + " bytes)");

    // Do ignore the warning we get in this method.
    TGCrudClientUtils.setIgnoreWarnings(true);

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTinyXmlMetadataFilename, createNonWellformedXmlFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Check expected warning.
    // TODO Check warning message!
    if (metadata.getObject().getGeneric().getGenerated().getWarning().isEmpty()) {
      assertTrue(false);
    }

    // Re-set warning trigger.
    TGCrudClientUtils.setIgnoreWarnings(false);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs a simple TG-crud#CREATE with a small XML file. Uses MTOM stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateSmallXmlFile() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with a small XML file (file size: "
        + TGCrudClientUtils.getResource(createSmallXmlFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createSmallXmlMetadataFilename, createSmallXmlFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs all basic TG-crud tests using a tiny TIFF image and test file for update. Uses non MTOM
   * stub.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCrudBasicsWithTinyImageFileNonMTOM() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud basics (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length()
        + " bytes), MTOM disabled");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrudNonMtom, tgsearch, rbacSessionId, logParameter, projectId,
            createTiffImageMetadataFilename, createTinyTiffImageFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update the metadata.
    TGCrudClientUtils.updateMetadata(tgcrudNonMtom, tgsearch, rbacSessionId, logParameter,
        updateMetadataTiffImageMetadataFilename, metadata.getObject().getGeneric().getGenerated());
    metadata = TGCrudClientUtils.readMetadata(tgcrudNonMtom, rbacSessionId, logParameter, uri);

    // TODO Check new metadata!

    // Update all.
    TGCrudClientUtils.update(tgcrudNonMtom, tgsearch, rbacSessionId, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    TGCrudClientUtils.read(tgcrudNonMtom, rbacSessionId, logParameter, uri);

    // TODO Check new data!

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrudNonMtom, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Ingest an edition and a work and an item and a second item with all the consequences.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testEditionIngest()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, RelationsExistFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud edition ingest");

    // Ingest the work.
    MetadataContainerType metadataWork = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createWorkMetadataFilename, createWorkFilename, NO_WARNINGS);
    String workUri =
        metadataWork.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Ingest the items, get the URIs.
    MetadataContainerType metadataItem1 = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createItem1MetadataFilename, createItem1Filename, NO_WARNINGS);
    String itemUri1 =
        metadataItem1.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    MetadataContainerType metadataItem2 = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createItem2MetadataFilename, createItem2Filename, NO_WARNINGS);
    String itemUri2 =
        metadataItem2.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create the edition's aggregation file and store it as temp file.
    String itemUri1Stripped = itemUri1.substring(0, itemUri1.lastIndexOf('.'));
    String editionString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        + "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
        + "xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
        + "\t<rdf:Description rdf:about=\"\">\n" + "\t\t<ore:aggregates rdf:resource=\""
        + itemUri1Stripped + "\" />\n" + "\t\t<ore:aggregates rdf:resource=\"" + itemUri2
        + "\" />\n" + "\t</rdf:Description>\n" + "</rdf:RDF>";
    File editionFile = TGCrudClientUtils.getResource(createEditionFilename);
    editionFile.createNewFile();
    try (FileOutputStream editionStream = new FileOutputStream(editionFile)) {
      editionStream.write(editionString.getBytes("UTF-8"));
      editionStream.close();
    }

    // Finally ingest the edition.
    MetadataContainerType metadataEdition = TGCrudClientUtils.create(tgcrud, tgsearch,
        rbacSessionId, logParameter, projectId, createEditionMetadataFilename,
        createEditionFilename, NO_URI, NO_URI, NO_URI, NO_URI, workUri, NO_WARNINGS);

    // Check aggregation entries via TG-search.
    TGCrudClientUtils.checkTGSearchAggregationEntries(editionString, metadataEdition, tgsearch,
        rbacSessionId);

    String editionUri =
        metadataEdition.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read again to check file and metadata sizes.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, editionUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, itemUri1);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, itemUri2);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, editionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, workUri);
    }
  }

  /**
   * <p>
   * Ingest a work with attribute complete dateOfCreation tag.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testWorkIngestCompleteDate()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, RelationsExistFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud work ingest (complete date attributes)");

    // Ingest the work.
    MetadataContainerType metadataWork =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createWorkMetadataFilenameCompleteDate, createWorkFilename, NO_WARNINGS);
    String workUri =
        metadataWork.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read again to check file and metadata sizes.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, workUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, workUri);
    }
  }

  /**
   * <p>
   * Ingest a work with dateOfCreation attribute "date" only.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testWorkIngestDateOnly()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, RelationsExistFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud work ingest (date attribute only)");

    // Ingest the work.
    MetadataContainerType metadataWork =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createWorkMetadataFilenameDateOnly, createWorkFilename, NO_WARNINGS);
    String workUri =
        metadataWork.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read again to check file and metadata sizes.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, workUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, workUri);
    }
  }

  /**
   * <p>
   * Ingest a work with dateOfCreation attribute "date" only.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testWorkIngestDateNoDateAttribute()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, RelationsExistFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud work ingest (no date attribute)");

    // Ingest the work.
    MetadataContainerType metadataWork =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createWorkMetadataFilenameNoDateAttribute, createWorkFilename, NO_WARNINGS);
    String workUri =
        metadataWork.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read again to check file and metadata sizes.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, workUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, workUri);
    }
  }

  /**
   * <p>
   * Ingest a work with dateOfCreation attribute "date" only.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testWorkIngestDateNotBeforeNotAfterOnly()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, RelationsExistFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud work ingest (notBefore and notAfter attributes only)");

    // Ingest the work.
    MetadataContainerType metadataWork =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createWorkMetadataFilenameNotBeforeNotAfter, createWorkFilename, NO_WARNINGS);
    String workUri =
        metadataWork.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read again to check file and metadata sizes.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, workUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, workUri);
    }
  }

  /**
   * <p>
   * Ingest an edition and NO work and an item and a second item with all the consequences (#295).
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testEditionIngestWithoutWorkSet()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, RelationsExistFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud edition ingest without work set");

    // Ingest the items, get the URIs.
    MetadataContainerType metadataItem1 = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createItem1MetadataFilename, createItem1Filename, NO_WARNINGS);
    String itemUri1 =
        metadataItem1.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    MetadataContainerType metadataItem2 = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createItem2MetadataFilename, createItem2Filename, NO_WARNINGS);
    String itemUri2 =
        metadataItem2.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create the edition's aggregation file and store it as temp file.
    String itemUri1Stripped = itemUri1.substring(0, itemUri1.lastIndexOf('.'));
    String editionString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        + "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
        + "xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
        + "\t<rdf:Description rdf:about=\"\">\n" + "\t\t<ore:aggregates rdf:resource=\""
        + itemUri1Stripped + "\" />\n" + "\t\t<ore:aggregates rdf:resource=\"" + itemUri2
        + "\" />\n" + "\t</rdf:Description>\n" + "</rdf:RDF>";
    File editionFile = TGCrudClientUtils.getResource(createEditionFilename);
    editionFile.createNewFile();
    try (FileOutputStream editionStream = new FileOutputStream(editionFile)) {
      editionStream.write(editionString.getBytes("UTF-8"));
      editionStream.close();
    }

    // Finally ingest the edition.
    MetadataContainerType metadataEdition = TGCrudClientUtils.create(tgcrud, tgsearch,
        rbacSessionId, logParameter, projectId, createEditionMetadataFilename,
        createEditionFilename, NO_URI, NO_URI, NO_URI, NO_URI, "", NO_WARNINGS);

    // TODO Check metadata for title and format in relations.RDF.Description.

    String editionUri =
        metadataEdition.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read again to check file and metadata sizes.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, editionUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, itemUri1);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, itemUri2);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, editionUri);
    }
  }

  /**
   * <p>
   * Ingest an empty file.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testEmptyFileIngest() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, RelationsExistFault, ProtocolNotImplementedFault,
      NoSuchAlgorithmException {

    System.out.println("Testing ingest of an empty file (file size: "
        + TGCrudClientUtils.getResource(createEmptyFileFilename).length() + " bytes)");

    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createEmptyFileMetadataFilename, createEmptyFileFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Ingests a tiny TIFF image.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testTinyImageFileIngest() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, RelationsExistFault,
      NoSuchAlgorithmException {

    System.out.println("Testing ingest of a tiny image file (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Ingests a small TIFF image.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testSmallImageFileIngest() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, RelationsExistFault,
      NoSuchAlgorithmException {

    System.out.println("Testing ingest of a small image file (file size: "
        + TGCrudClientUtils.getResource(createSmallTiffImageFilename).length() + " bytes)");

    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createSmallTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test ingesting a huge movie file.
   * </p>
   *
   * PLEASE NOTE Please ignore on slow and faster machines :-)
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Ignore
  @Test
  public void testHugeMovieFileIngest() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, RelationsExistFault,
      NoSuchAlgorithmException {

    System.out.println("Testing ingest of a huge movie file (file size: "
        + TGCrudClientUtils.getResource(createHugeMP4Filename).length() + " bytes)");

    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createHugeMP4MetadataFilename, createHugeMP4Filename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test XML Schema file ingest.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testXsdAndXmlFileIngest() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, RelationsExistFault,
      NoSuchAlgorithmException {

    System.out.println("Testing ingest of an XML schema file, and a relating XML file");

    // Create XSD file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createXsdMetadataFilename, createXsdFilename, NO_WARNINGS);
    String xsdUri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, xsdUri);

    // Create XML file.
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTinyXmlMetadataFilename, createTinyXmlFilename, NO_URI, NO_URI, xsdUri, NO_URI,
        NO_URI, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, xsdUri);
    }
  }

  /**
   * <p>
   * Test XML Schema file ingest without target namespace.
   * </p>
   *
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testNoNamespaceXsdIngest() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, RelationsExistFault,
      NoSuchAlgorithmException {

    System.out.println("Testing ingest of an XML schema file with no namespace declared");

    // Create XSD file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createXsdMetadataFilename, createXsdWithoutNamespaceFilename,
        NO_WARNINGS);
    String xsdUri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, xsdUri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, xsdUri);
    }
  }

  /**
   * <p>
   * Test XML file ingest with Campe file.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testCampeXmlFileIngest() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, RelationsExistFault,
      NoSuchAlgorithmException {

    System.out.println("Testing ingest of a Campe XML file (including baseline adaptor)");

    // Ingest Campe baseline adaptor.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createXmlCampeAdaptorMetadataFilename, createXmlCampeAdaptorFilename, NO_WARNINGS);
    String adaptorUri =
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, adaptorUri);

    // Create Campe file.
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createXmlCampeMetadataFilename, createXmlCampeFilename, NO_URI, adaptorUri, NO_URI, NO_URI,
        NO_URI, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, adaptorUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test XML file ingest with Werther file.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testWertherXmlFileIngest() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, RelationsExistFault,
      NoSuchAlgorithmException {

    System.out.println("Testing ingest of a Werther XML file (including baseline adaptor)");

    // Ingest Werther baseline adaptor.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createXmlWertherAdaptorMetadataFilename, createXmlWertherAdaptorFilename, NO_WARNINGS);
    String adaptorUri =
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, adaptorUri);

    // Create Werther file.
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createXmlWertherMetadataFilename, createXmlWertherFilename, NO_URI, adaptorUri, NO_URI,
        NO_URI, NO_URI, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, adaptorUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests possible relations to extract from the metadata (only non-aggregations and non-adaptors):
   * <b>isVersionOf</b> and <b>isAlternativeFormatOf</b>.
   * </p>
   *
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testRelationHandling()
      throws IOException, MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault,
      ProtocolNotImplementedFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing ingest with non-adaptor and non-version relation metadata");

    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createXsdMetadataWithRelationsFilename, createXsdFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Ingest an aggregation and an item and a second item with all the consequences, see
   * <https://gitlab.gwdg.de/dariah-de/dariah-de-crud-services/-/issues/342>, also including bug
   * TG-1485 and TG-1522 (I would think).
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   * @throws UpdateConflictFault
   */
  @Test
  public void testAggregationIngest() throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, RelationsExistFault,
      ProtocolNotImplementedFault, NoSuchAlgorithmException, UpdateConflictFault {

    System.out.println("Testing TG-crud aggregation ingest");

    // Ingest the items, get the URIs.
    MetadataContainerType metadataItem1 = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createItem1MetadataFilename, createItem1Filename, NO_WARNINGS);
    String itemUri1 =
        metadataItem1.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    MetadataContainerType metadataItem2 = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createItem2MetadataFilename, createItem2Filename, NO_WARNINGS);
    String itemUri2 =
        metadataItem2.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create the edition's aggregation file and store it as temp file.
    String itemUri1Stripped = itemUri1.substring(0, itemUri1.lastIndexOf('.'));
    String itemUri2Stripped = itemUri2.substring(0, itemUri2.lastIndexOf('.'));
    String aggregationString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        + "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
        + "xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
        + "\t<rdf:Description rdf:about=\"\">\n" + "\t\t<ore:aggregates rdf:resource=\""
        + itemUri1Stripped + "\" />\n" + "\t\t<ore:aggregates rdf:resource=\"" + itemUri2Stripped
        + "\" />\n" + "\t</rdf:Description>\n" + "</rdf:RDF>";

    File aggregationFile = TGCrudClientUtils.getResource(createEditionFilename);
    aggregationFile.createNewFile();
    try (FileOutputStream aggregationStream = new FileOutputStream(aggregationFile)) {
      aggregationStream.write(aggregationString.getBytes("UTF-8"));
      aggregationStream.close();
    }

    // Finally ingest the aggregation.
    MetadataContainerType metadataAggregation =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createAggregationMetadataFilename, createEditionFilename, NO_WARNINGS);

    // Check aggregation entries via TG-search.
    TGCrudClientUtils.checkTGSearchAggregationEntries(aggregationString, metadataAggregation,
        tgsearch, rbacSessionId);

    // Update aggregation.
    metadataAggregation = TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateAggregationMetadataFilename, createEditionFilename,
        metadataAggregation.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Check aggregation entries via TG-search (again).
    TGCrudClientUtils.checkTGSearchAggregationEntries(aggregationString, metadataAggregation,
        tgsearch, rbacSessionId);

    // Read again to check file and metadata sizes.
    String aggregationUri =
        metadataAggregation.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, aggregationUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, itemUri1);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, itemUri2);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, aggregationUri);
    }
  }

  /**
   * <p>
   * Tests importing a file without <item> tag in the metadata.
   * </p>
   *
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testItemImportBug347() throws IOException, MetadataParseFault, IoFault,
      ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault, RelationsExistFault,
      UpdateConflictFault, NoSuchAlgorithmException {

    System.out.println("Testing file ingest without item metadata tag");

    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createItemWithoutItemTagMetadataFilename, createSmallTiffImageFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Do read.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests special aggregation file ingest, including bug TG-1485 and TG-1522. First tests did not
   * work with this file!
   * </p>
   *
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testSpecialAggregationFileIngest() throws IOException, MetadataParseFault, IoFault,
      ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault, RelationsExistFault,
      UpdateConflictFault, NoSuchAlgorithmException {

    System.out.println("Testing ingest of a special aggregation file");

    // Create aggregation file.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createSpecialAggregationMetadataFilename, createSpecialAggregationFilename,
            NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Get relation data No.1.
    String query = null;
    String response_no1 = null;
    if (CHECK_RDFDB) {
      String baseUri = uri.substring(0, uri.indexOf('.'));
      query = "SELECT DISTINCT ?S ?P ?O WHERE { { <" + uri + "> ?P ?O } UNION { <" + baseUri
          + "> ?P ?O } }";
      response_no1 = IOUtils.readStringFromStream(TGCrudClientUtils.querySesame(sesame, query));
    }

    // Read the metadata.
    metadata = TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);

    // Update the metadata.
    TGCrudClientUtils.updateMetadata(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateSpecialMetadataAggregationMetadataFilename,
        metadata.getObject().getGeneric().getGenerated());

    // Get relation data No.2, and compare to No.1 (for checking
    // TG-1522).
    String response_no2 = null;
    if (CHECK_RDFDB) {
      response_no2 = IOUtils.readStringFromStream(TGCrudClientUtils.querySesame(sesame, query));

      // Compare No.1 and No.2.
      // TODO Use different queries for aggregations, and other relations!
      // Would be better to handle!
      if (!response_no1.equals(response_no2)) {
        System.out.println("\tSESAME QUERIES ARE NOT EQUAL!");
        System.out.println("\tSesame query No.1 (after #CREATE): " + query);
        System.out.println(response_no1);
        System.out.println("\tSesame query No.2 (after #UPDATEMETADATA): " + query);
        System.out.println(response_no2);
        assertTrue(false);
      }
    }

    // Read the metadata.
    metadata = TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);

    // Now update by deleting one of the included aggregations.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateSpecialAggregationMetadataFilename, updateSpecialAggregationFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Get relation data No.3 (for checking TG-1522).
    String response_no3 = null;
    if (CHECK_RDFDB) {
      response_no3 = IOUtils.readStringFromStream(TGCrudClientUtils.querySesame(sesame, query));

      // Check No.2 and No.3 for project IDs.
      if (!response_no2.contains("<uri>textgrid:project:" + projectId + "</uri>")
          || !response_no3.contains("<uri>textgrid:project:" + projectId + "</uri>")) {
        System.out.println("\tTHERE IS THE PROJECT ID MISSING!");
        System.out.println("\tSesame query No.2 (after #UPDATEMETADATA): " + query);
        System.out.println(response_no2);
        System.out.println("\tSesame query No.3 (after #UPDATE): " + query);
        System.out.println(response_no3);
        assertTrue(false);
      }

      // Check No.2 for aggregates URIs (all must be present!).
      if (!response_no2.contains("<uri>textgrid:fcmj</uri>")
          || !response_no2.contains("<uri>textgrid:fcmm</uri>")
          || !response_no2.contains("<uri>textgrid:fcmq</uri>")
          || !response_no2.contains("<uri>textgrid:fcmz</uri>")) {
        System.out.println("\tTHERE ARE AGGREGATED URIS MISSING!");
        System.out.println("\tSesame query No.2 (after #UPDATEMETADATA): " + query);
        System.out.println(response_no2);
        assertTrue(false);
      }

      // Check No.3 for aggregates URIs (only one must be present!).
      if (!response_no3.contains("<uri>textgrid:fcmj</uri>")
          || response_no3.contains("<uri>textgrid:fcmm</uri>")
          || response_no3.contains("<uri>textgrid:fcmq</uri>")
          || response_no3.contains("<uri>textgrid:fcmz</uri>")) {
        System.out.println("\tTHERE ARE TOO MUCH AGGREGATED URIS OR URI MISSING!");
        System.out.println("\tSesame query No.3 (after #UPDATEMETADATA): " + query);
        System.out.println(response_no3);
        assertTrue(false);
      }
    }

    // Do read.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests special aggregation file ingest (bug TG-1534). We have no relation tags in the metadata
   * here.
   * </p>
   *
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testBugTG1534() throws IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, RelationsExistFault, UpdateConflictFault,
      NoSuchAlgorithmException {

    System.out
        .println("Testing ingest of a special aggregation file (no relation tag entries existing)");

    // Create aggregation file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createBugTG1534MetadataFilename, createSpecialAggregationFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Get relation data No.1.
    String query = null;
    String response_no1 = null;
    if (CHECK_RDFDB) {
      String baseUri = uri.substring(0, uri.indexOf('.'));
      query = "SELECT DISTINCT ?S ?P ?O WHERE { { <" + uri + "> ?P ?O } UNION { <" + baseUri
          + "> ?P ?O } }";
      response_no1 = IOUtils.readStringFromStream(TGCrudClientUtils.querySesame(sesame, query));
    }

    // Read the metadata.
    metadata = TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);

    // Update the metadata.
    TGCrudClientUtils.updateMetadata(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateMetadataBugTG1534MetadataFilename, metadata.getObject().getGeneric().getGenerated());

    // Get relation data No.2, and compare to No.1 (for checking TG-1522).
    String response_no2 = null;
    if (CHECK_RDFDB) {
      response_no2 = IOUtils.readStringFromStream(TGCrudClientUtils.querySesame(sesame, query));

      // Compare No.1 and No.2.
      // TODO Use different queries for aggregations, and other relations!
      // Would be better to handle!
      if (!response_no1.equals(response_no2)) {
        System.out.println("\tSESAME QUERIES ARE NOT EQUAL!");
        System.out.println("\tSesame query No.1 (after #CREATE): " + query);
        System.out.println(response_no1);
        System.out.println("\tSesame query No.2 (after #UPDATEMETADATA): " + query);
        System.out.println(response_no2);
        assertTrue(false);
      }
    }

    // Read the metadata.
    metadata = TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);

    // Now update by deleting one of the included aggregations.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateBugTG1534MetadataFilename, updateSpecialAggregationFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Get relation data No.3.
    String response_no3 = null;
    if (CHECK_RDFDB) {
      response_no3 = IOUtils.readStringFromStream(TGCrudClientUtils.querySesame(sesame, query));

      // Check No.2 and No.3 for project IDs.
      if (!response_no2.contains("<uri>textgrid:project:" + projectId + "</uri>")
          || !response_no3.contains("<uri>textgrid:project:" + projectId + "</uri>")) {
        System.out.println("\tTHERE IS THE PROJECT ID MISSING!");
        System.out.println("\tSesame query No.2 (after #UPDATEMETADATA): " + query);
        System.out.println(response_no2);
        System.out.println("\tSesame query No.3 (after #UPDATE): " + query);
        System.out.println(response_no3);
        assertTrue(false);
      }

      // Check No.2 for aggregates URIs (all must be present!).
      if (!response_no2.contains("<uri>textgrid:fcmj</uri>")
          || !response_no2.contains("<uri>textgrid:fcmm</uri>")
          || !response_no2.contains("<uri>textgrid:fcmq</uri>")
          || !response_no2.contains("<uri>textgrid:fcmz</uri>")) {
        System.out.println("\tTHERE ARE AGGREGATED URIS MISSING!");
        System.out.println("\tSesame query No.2 (after #UPDATEMETADATA): " + query);
        System.out.println(response_no2);
        assertTrue(false);
      }

      // Check No.3 for aggregates URIs (only one must be present!).
      if (!response_no3.contains("<uri>textgrid:fcmj</uri>")
          || response_no3.contains("<uri>textgrid:fcmm</uri>")
          || response_no3.contains("<uri>textgrid:fcmq</uri>")
          || response_no3.contains("<uri>textgrid:fcmz</uri>")) {
        System.out.println("\tTHERE ARE TOO MUCH AGGREGATED URIS OR URI MISSING!");
        System.out.println("\tSesame query No.3 (after #UPDATEMETADATA): " + query);
        System.out.println(response_no3);
        assertTrue(false);
      }
    }

    // Do read.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests ingesting derivates (isDerivedFrom).
   * </p>
   *
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testDerivateHandling()
      throws MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, FileNotFoundException,
      IOException, ProtocolNotImplementedFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing ingest of some files and build the following derivation tree:");
    System.out.println("\t\t    (1)");
    System.out.println("\t\t   /   \\");
    System.out.println("\t\t(2)    (3)");
    System.out.println("\t\t |    /   \\");
    System.out.println("\t\t(4) (6)   (7)");
    System.out.println("\t\t |");
    System.out.println("\t\t(5)");

    // Create a first object as root element.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
        NO_WARNINGS);
    String rootUri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, rootUri);

    // Create two versions of the first version.
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileMetadataFilename, createTestFileFilename, NO_URI, NO_URI, NO_URI, rootUri,
        NO_URI, NO_WARNINGS);
    String firstChildUri =
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, firstChildUri);
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileMetadataFilename, createTestFileFilename, NO_URI, NO_URI, NO_URI, rootUri,
        NO_URI, NO_WARNINGS);
    String secondChildUri =
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, secondChildUri);

    // Create two new versions of the first child (as a list).
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileMetadataFilename, createTestFileFilename, NO_URI, NO_URI, NO_URI,
        firstChildUri, NO_URI, NO_WARNINGS);
    String firstChildOfFirstChildUri =
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, firstChildOfFirstChildUri);
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileMetadataFilename, createTestFileFilename, NO_URI, NO_URI, NO_URI,
        firstChildOfFirstChildUri, NO_URI, NO_WARNINGS);
    String nextChildOfFirstChildUri =
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, nextChildOfFirstChildUri);

    // Create two new versions of the second child.
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileMetadataFilename, createTestFileFilename, NO_URI, NO_URI, NO_URI,
        secondChildUri, NO_URI, NO_WARNINGS);
    String secondChildNewVersion1 =
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, secondChildNewVersion1);
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createTestFileMetadataFilename, createTestFileFilename, NO_URI, NO_URI, NO_URI,
        secondChildUri, NO_URI, NO_WARNINGS);
    String secondChildNewVersion2 =
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, secondChildNewVersion2);

    // Delete all the created objects again.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, rootUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, firstChildUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, secondChildUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter,
          firstChildOfFirstChildUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter,
          nextChildOfFirstChildUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter,
          secondChildNewVersion1);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter,
          secondChildNewVersion2);
    }
  }

  /**
   * <p>
   * Test Text-Image-Link-Editor-Linkfile.
   * </p>
   *
   * NOTE Please ignore until we have new files for testing?
   *
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testTextImageLinkEditorLinkfileIngest() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      RelationsExistFault, NoSuchAlgorithmException {

    // TODO Maybe add here creation of all three: image file, XML file, and TBLE link file.

    // Create and read link editor file.
    System.out.println("Testing ingest of a Text Image Link Editor link file");

    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createLinkfileMetadataFilename, createLinkfileFilename,
        NO_WARNINGS);
    String imageUri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, imageUri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, imageUri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, imageUri);
    }
  }

  /**
   * <p>
   * Test faulty Text-Image-Link-Editor-Linkfile. This is bug TG-1515.
   * </p>
   *
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testFaultyTextImageLinkEditorLinkfileIngest() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      RelationsExistFault, NoSuchAlgorithmException {

    // TODO Maybe add here creation of all three: image file, XML file, and TBLE link file.

    // Create and read link editor file.
    System.out.println("Testing ingest of a faulty (invalid URI) Text Image Link Editor link file");

    MetadataContainerType metadata;
    synchronized (this) {
      TGCrudClientUtils.setIgnoreWarnings(true);
      metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
          createLinkfileMetadataFilename, createFaultyLinkfileFilename, NO_WARNINGS);
      TGCrudClientUtils.setIgnoreWarnings(false);
    }
    String imageUri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, imageUri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, imageUri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, imageUri);
    }
  }

  /**
   * <p>
   * Test reading an unknown resource.
   * </p>
   *
   * NOTE This is Jira TG-635! NOTE We have a workaround in TG-crud for now...
   *
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws IOException
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testReadUnknownResource() throws IOException, ObjectNotFoundFault, MetadataParseFault,
      IoFault, ProtocolNotImplementedFault, AuthFault, NoSuchAlgorithmException {

    System.out.println("Testing reading of an unknown resource");

    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, readUnknownResource);
  }

  /**
   * <p>
   * Tests creating files using test files from the TG-lab ingest, they seem not to be stored TO THE
   * GRID, but file size is correctly stored into the metadata. Seems to occur if client is using
   * MTOM. Test MTOM client here. Bug TG-1061 was fixed. Test should succeed now, as other MTOM
   * tests, too.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   * @see https://develop.sub.uni-goettingen.de/jira/browse/TG-1061
   */
  @Test
  public void testBugTG1061PlainText() throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing creation of files for Jira bug TG-1061 (plain text)");

    // Test create.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createBugTG1061MetadataFilename, createBugTG1061Filename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Test update (use update file).
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Test read.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Another TG-1061 test with XML file.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   * @see https://develop.sub.uni-goettingen.de/jira/browse/TG-1061
   */
  @Test
  public void testBugTG1061Xml() throws FileNotFoundException, IOException, MetadataParseFault,
      IoFault, ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing creation of files for Jira bug TG-1061 (XML)");

    // Test create (use create file).
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTinyXmlMetadataFilename, createTinyXmlFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Test update.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateBugTG1061XmlMetadataFilename, updateBugTG1061XmlFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Test read.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Another TG-1061 test with XSLT file.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   * @see https://develop.sub.uni-goettingen.de/jira/browse/TG-1061
   */
  @Test
  public void testBugTG1061Xslt() throws FileNotFoundException, IOException, MetadataParseFault,
      IoFault, ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing creation of files for Jira bug TG-1061 (XSLT)");

    // Test create (use create file).
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createXmlCampeAdaptorMetadataFilename, createXmlCampeAdaptorFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Test update.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateXmlCampeAdaptorMetadataFilename, updateXmlCampeAdaptorFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Test read.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Test delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test importing a 1 MB XML file.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @see https://develop.sub.uni-goettingen.de/jira/browse/TG-1158
   */
  @Test
  public void testBugTG1158XmlImport()
      throws IOException, XMLStreamException, MetadataParseFault, AuthFault, IoFault,
      ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault, RelationsExistFault {

    System.out.println("Testing TG-crud bug TG-1158 (file size: "
        + TGCrudClientUtils.getResource(createBugTG1158Filename).length() + " bytes)");

    // Do ignore the warning we get in this method.
    TGCrudClientUtils.setIgnoreWarnings(true);

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createBugTG1158MetadataFilename, createBugTG1158Filename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Check expected warning.
    // TODO Check warning message!
    if (metadata.getObject().getGeneric().getGenerated().getWarning().isEmpty()) {
      assertTrue(false);
    }

    // Re-set warning trigger.
    TGCrudClientUtils.setIgnoreWarnings(false);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test importing a 9 MB XML file.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @see https://develop.sub.uni-goettingen.de/jira/browse/TG-1171
   */
  @Test
  public void testBugTG1171XmlImport()
      throws IOException, XMLStreamException, MetadataParseFault, AuthFault, IoFault,
      ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault, RelationsExistFault {

    System.out.println("Testing TG-crud bug TG-1171 (file size: "
        + TGCrudClientUtils.getResource(createBugTG1171Filename).length() + " bytes)");

    // Do ignore the warning we get in this method.
    TGCrudClientUtils.setIgnoreWarnings(true);

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createBugTG1171MetadataFilename, createBugTG1171Filename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Check expected warning.
    // TODO Check warning message!
    if (metadata.getObject().getGeneric().getGenerated().getWarning().isEmpty()) {
      assertTrue(false);
    }

    // Re-set warning trigger.
    TGCrudClientUtils.setIgnoreWarnings(false);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Bug#277: Tests continuous update of XML file in fast loop.
   * </p>
   *
   * NOTE: Please ignore if not testing THIS issue alone!
   *
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  @Ignore
  public void testBug277XMLItemUpdate() throws IOException, MetadataParseFault, IoFault,
      ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault, RelationsExistFault,
      UpdateConflictFault, NoSuchAlgorithmException {

    int updateCount = 9000;

    System.out.println("Tests continuous update of XML file in fast loop");

    // Create aggregation file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTinyXmlMetadataFilename, createTinyXmlFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update the aggregation x times.
    String xml = IOUtils.readStringFromStream(
        new FileInputStream(TGCrudClientUtils.getResource(createTinyXmlFilename)));
    File tempXMLFile = File.createTempFile("tg-import_", "_tg-import");
    for (int i = 0; i < updateCount; i++) {
      System.out.println("--  PREPARE NEW XML (START)  --------");
      // Add a new <resource> to aggregation.
      String newObjectUri = "AUA (" + (i + 1) + ")";
      xml = xml.replace("\t<!-- Insert new entry here! -->",
          "\t<hmpf>" + newObjectUri + "</resource>\n\t<!-- Insert new entry here! -->");
      // Write new temp file with current XML content.
      try (PrintWriter pWriter = new PrintWriter(new FileWriter(tempXMLFile))) {
        pWriter.print(xml);
        pWriter.flush();
        pWriter.close();
      }

      // Output file.
      System.out.println(IOUtils.readStringFromStream(new FileInputStream(tempXMLFile)).trim());
      System.out.println("--  PREPARE NEW XML (END)  --------");
      // Do UPDATE!
      metadata = TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
          updateAggregationMetadataFilename, tempXMLFile.getAbsolutePath(),
          metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    }

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Bug#277: Tests continuous update of TIFF file in fast loop.
   * </p>
   *
   * NOTE: Please ignore if not testing THIS issue alone!
   *
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  @Ignore
  public void testBug277TIFFItemUpdate() throws IOException, MetadataParseFault, IoFault,
      ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault, RelationsExistFault,
      UpdateConflictFault, NoSuchAlgorithmException {

    int updateCount = 9000;

    System.out.println("Tests continuous update of TIFF file in fast loop");

    // Create aggregation file.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            updateTiffImageMetadataFilename, createSmallJpegImageFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update the aggregation x times.
    for (int i = 0; i < updateCount; i++) {
      System.out.println("TIFF image update No " + i);
      // Do UPDATE!
      metadata = TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
          updateTiffImageMetadataFilename, createSmallJpegImageFilename,
          metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    }

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Bug#277: Tests continuous update of aggregation file in fast loop.
   * </p>
   *
   * NOTE: Please ignore if not testing THIS issue alone!
   *
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  @Ignore
  public void testBug277AggregationUpdate() throws IOException, MetadataParseFault, IoFault,
      ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault, RelationsExistFault,
      UpdateConflictFault, NoSuchAlgorithmException {

    int updateCount = 9000;

    System.out.println("Tests continuous update of aggregation file in fast loop");

    // Create aggregation file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createAggregationMetadataFilename, createAggregationFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update the aggregation x times.
    String baseURI = "textgrid:12345";
    String aggregation = IOUtils.readStringFromStream(
        new FileInputStream(TGCrudClientUtils.getResource(updateAggregationFilename)));
    File tempAggregationFile = File.createTempFile("tg-import_", "_tg-import");
    for (int i = 0; i < updateCount; i++) {
      System.out.println("--  PREPARE NEW AGGREGATION (START)  --------");
      // Add a new <resource> to aggregation.
      String newObjectUri = baseURI + "." + (i + 1);
      System.out.println("Object added to aggregation: " + newObjectUri);
      aggregation = aggregation.replace("\t\t\t<!-- Insert resource here! -->",
          "\t\t\t<resource>" + newObjectUri + "</resource>\n\t\t\t<!-- Insert resource here! -->");
      // Write new temp file with current aggregation.
      try (PrintWriter pWriter = new PrintWriter(new FileWriter(tempAggregationFile))) {
        pWriter.print(aggregation);
        pWriter.flush();
        pWriter.close();
      }

      // Output file.
      System.out
          .println(IOUtils.readStringFromStream(new FileInputStream(tempAggregationFile)).trim());
      System.out.println("--  PREPARE NEW AGGREGATION (END)  --------");
      // Do UPDATE!
      metadata = TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
          updateAggregationMetadataFilename, tempAggregationFile.getAbsolutePath(),
          metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    }

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test importing a Werther XML file with Japanese UTF-8 chars.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   * @see https://develop.sub.uni-goettingen.de/jira/browse/TG-1157
   */
  @Test
  public void testBugTG1157Utf8Import() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud bug TG-1157 (file size: "
        + TGCrudClientUtils.getResource(createBugTG1157Filename).length() + " bytes)");

    // Ingest Werther baseline adaptor.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createXmlWertherAdaptorMetadataFilename, createXmlWertherAdaptorFilename, NO_WARNINGS);
    String adaptorUri =
        metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, adaptorUri);
    // Create Werther file.
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createBugTG1157MetadataFilename, createBugTG1157Filename, NO_URI, adaptorUri, NO_URI,
        NO_URI, NO_URI, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
    DataHandler dh = TGCrudClientUtils.simpleRead(tgcrud, rbacSessionId, logParameter, uri);

    // Set test chars.
    String testChars = "äöüÄÖÜßふぐ";

    // Check metadata.
    String title = metadata.getObject().getGeneric().getProvided().getTitle().get(0);

    System.out.print(
        "\tLooking for UTF-8 test chars '" + testChars + "' in title metadata [" + uri + "]: ");

    if (!title.contains(testChars)) {
      System.out.println("NOT FOUND!");
      System.out.println("TITLE METADATA DEBUG OUTPUT:\n" + title);
      assertTrue(false);
    } else {
      System.out.println("\tOK");
    }

    // Get original test file from disk.
    File fileFromDisk = TGCrudClientUtils.getResource(createBugTG1157Filename);
    String stringFromDisk = IOUtils.toString(new FileInputStream(fileFromDisk), "UTF-8");

    System.out
        .print("\tLooking for UTF-8 test chars '" + testChars + "' in GRID data [" + uri + "]: ");

    // Check if UTF-8 chars were successfully and correctly ingested TO THE
    // STORAGE! Check here for special äöüÄÖÜß and ふぐ chars
    // (TEST:äöüÄÖÜßふぐ)!
    String stringFromStorage = IOUtils.toString(dh.getInputStream(), "UTF-8");
    // Compare.
    if (!stringFromDisk.equals(stringFromStorage)) {
      System.out.println("NOT FOUND!");
      System.out.println("GRID DATA DEBUG OUTPUT:\n" + stringFromStorage);
      assertTrue(false);
    } else {
      System.out.println("\tOK");
    }

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, adaptorUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test importing a file with PIDs set in generated type. PIDs must not be taken over by TG-crud,
   * and be deleted/overwritten!
   * </p>
   *
   * <p>
   * PLEASE NOTE TG-1860 was a feature, not a bug!
   * </p>
   *
   * PLEASE NOTE Please IGNORE this test! All PIDs the user puts in will be taken over by TG-crud!
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @see https://develop.sub.uni-goettingen.de/jira/browse/TG-1860
   */
  @Test
  @Ignore
  public void testBugTG1860NoPids()
      throws IOException, XMLStreamException, MetadataParseFault, AuthFault, IoFault,
      ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault, RelationsExistFault {

    System.out.println("Testing TG-crud bug TG-1860 (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Check PIDs before ingest: Create metadata from file.
    MetadataContainerType metadata =
        TGCrudClientUtils.getMetadataFromFile(createBugTG1860MetadataFilename);
    List<Pid> startPids = metadata.getObject().getGeneric().getGenerated().getPid();

    System.out.println("\tPID list to ingest: ");
    for (Pid p : startPids) {
      System.out.println("\t\t" + p.getValue() + " (" + p.getPidType() + ")");
    }

    // Create file with PID in metadata.
    metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createBugTG1860MetadataFilename, createTinyTiffImageFilename, NO_URI, NO_URI, NO_URI,
        NO_URI, NO_URI, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    List<Pid> endPids = metadata.getObject().getGeneric().getGenerated().getPid();

    System.out.println("\tPID list from TG-crud: ");
    if (endPids != null) {
      for (Pid p : endPids) {
        System.out.println("\t\t" + p.getValue() + " (" + p.getPidType() + ")");
      }
    } else {
      assertTrue("endPids list must not be null!", false);
    }

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }

    // Test if returned metadata has NO PIDs in it!
    if (endPids != null && !endPids.isEmpty()) {
      System.out.println("\tPID LIST IS NOT EMPTY, BUT SHOULD BE!");
      assertTrue(false);
    }
  }

  /**
   * <p>
   * Test reading a public file with special (newly created) TextGrid account. Please see TG-1929!
   * </p>
   *
   * <p>
   * PLEASE NOTE We must use a SID from a newly created user here. We also have fixed this bug in
   * TG-auth, so no test is needed here anymore.
   * </p>
   *
   * NOTE Please IGNORE this test!
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   * @see https://develop.sub.uni-goettingen.de/jira/browse/TG-1929
   */
  @Test
  @Ignore
  public void testBugTG1929AccessDeniedOnPublicRead() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud bug TG-1929 (just read public file)");

    // Just read public URI.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, readBugTG1929ObjectUri);
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateWithSmallLoadtestFile() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with small loadtest file (file size: "
        + TGCrudClientUtils.getResource(loadtestsSmallFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, loadtestsSmallMetadataFilename, loadtestsSmallFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateWithMediumLoadtestFile() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with medium loadtest file (file size: "
        + TGCrudClientUtils.getResource(loadtestsMediumFilename).length() + " bytes)");

    // Do ignore the warning we get in this method.
    TGCrudClientUtils.setIgnoreWarnings(true);

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, loadtestsMediumMetadataFilename, loadtestsMediumFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Check expected warning.
    // TODO Check warning message!
    if (metadata.getObject().getGeneric().getGenerated().getWarning().isEmpty()) {
      assertTrue(false);
    }

    // Re-set warning trigger.
    TGCrudClientUtils.setIgnoreWarnings(false);

    // Delete.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);

      // TODO Reading after deletion is currently not possible due to Bug TG-635: Reading unknown
      // resources does NOT lead to an unknownResourceFault!
      // NOTE We have found a workaround in TG-crud for now, so it works!
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Ignore
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateWithLargeLoadtestFile() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with large loadtest file (file size: "
        + TGCrudClientUtils.getResource(loadtestsLargeFilename).length() + " bytes)");

    // Do ignore the warning we get in this method.
    TGCrudClientUtils.setIgnoreWarnings(true);

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, loadtestsLargeMetadataFilename, loadtestsLargeFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Check expected warning.
    // TODO Check warning message!
    if (metadata.getObject().getGeneric().getGenerated().getWarning().isEmpty()) {
      assertTrue(false);
    }

    // Re-set warning trigger.
    TGCrudClientUtils.setIgnoreWarnings(false);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Ingests an edition and a work and an item and a second item with all the consequences, this
   * time tests bug #9442.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testBug9442()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, RelationsExistFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud edition ingest (this time bug #9442)");

    // Ingest the work.
    MetadataContainerType metadataWork = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createWorkMetadataFilename, createWorkFilename, NO_WARNINGS);
    String workUri =
        metadataWork.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Ingest the items, get the URIs.
    MetadataContainerType metadataItem1 = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createItem1MetadataFilename, createItem1Filename, NO_WARNINGS);
    String itemUri1 =
        metadataItem1.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    MetadataContainerType metadataItem2 = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createItem2MetadataFilename, createItem2Filename, NO_WARNINGS);
    String itemUri2 =
        metadataItem2.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create the edition's aggregation file and store it as temp file.
    String itemUri1Stripped = itemUri1.substring(0, itemUri1.lastIndexOf('.'));
    String editionString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        + "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
        + "xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
        + "\t<rdf:Description rdf:about=\"\">\n" + "\t\t<ore:aggregates rdf:resource=\""
        + itemUri1Stripped + "\" />\n" + "\t\t<ore:aggregates rdf:resource=\"" + itemUri2
        + "\" />\n" + "\t</rdf:Description>\n" + "</rdf:RDF>";
    File editionFile = TGCrudClientUtils.getResource(createEditionFilename);
    editionFile.createNewFile();
    try (FileOutputStream editionStream = new FileOutputStream(editionFile)) {
      editionStream.write(editionString.getBytes("UTF-8"));
      editionStream.close();
    }

    // Finally ingest the edition.
    MetadataContainerType metadataEdition = TGCrudClientUtils.create(tgcrud, tgsearch,
        rbacSessionId, logParameter, projectId, createBug9442MetadataFilename,
        createEditionFilename, NO_URI, NO_URI, NO_URI, NO_URI, workUri, NO_WARNINGS);

    // TODO Check metadata for title and format in relations.RDF.Description.

    String editionUri =
        metadataEdition.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read again to check file and metadata sizes.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, editionUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, itemUri1);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, itemUri2);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, editionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, workUri);
    }
  }

  /**
   * <p>
   * Ingests an edition with no <edition> metadata --> #36304.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testBug36304Edition()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, RelationsExistFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud edition ingest with empty <edition> tag (#36304)");

    // No work!

    // No items!

    // Create the edition's aggregation file and store it as temp file.
    String editionString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        + "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
        + "xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
        + "\t<rdf:Description rdf:about=\"\">\n" + "\t\t<ore:aggregates rdf:resource=\"\" />\n"
        + "\t</rdf:Description>\n" + "</rdf:RDF>";
    File editionFile = TGCrudClientUtils.getResource(createEditionFilename);
    editionFile.createNewFile();
    try (FileOutputStream editionStream = new FileOutputStream(editionFile)) {
      editionStream.write(editionString.getBytes("UTF-8"));
      editionStream.close();
    }

    // Finally ingest the edition.
    MetadataContainerType metadataEdition = TGCrudClientUtils.create(tgcrud, tgsearch,
        rbacSessionId, logParameter, projectId, createBug36304EditionMetadataFilename,
        createEditionFilename, NO_URI, NO_URI, NO_URI, NO_URI, NO_URI, ONE_WARNING);

    String editionUri =
        metadataEdition.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read again to check file and metadata sizes.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, editionUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, editionUri);
    }
  }

  /**
   * <p>
   * Ingests an collection with no <collection> metadata --> #36304.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws RelationsExistFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testBug36304Collection()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, RelationsExistFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud collection ingest with empty <collection> tag (#36304)");

    // No work!

    // No items!

    // Create the edition's aggregation file and store it as temp file.
    String collectionString = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n"
        + "<rdf:RDF xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" "
        + "xmlns:ore=\"http://www.openarchives.org/ore/terms/\">\n"
        + "\t<rdf:Description rdf:about=\"\">\n" + "\t\t<ore:aggregates rdf:resource=\"\" />\n"
        + "\t</rdf:Description>\n" + "</rdf:RDF>";
    File collectionFile = TGCrudClientUtils.getResource(createEditionFilename);
    collectionFile.createNewFile();
    try (FileOutputStream editionStream = new FileOutputStream(collectionFile)) {
      editionStream.write(collectionString.getBytes("UTF-8"));
      editionStream.close();
    }

    // Finally ingest the collection.
    MetadataContainerType metadataCollection = TGCrudClientUtils.create(tgcrud, tgsearch,
        rbacSessionId, logParameter, projectId, createBug36304CollectionMetadataFilename,
        createEditionFilename, NO_URI, NO_URI, NO_URI, NO_URI, NO_URI, ONE_WARNING);

    String collectionUri =
        metadataCollection.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read again to check file and metadata sizes.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, collectionUri);

    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, collectionUri);
    }
  }

  /**
   * <p>
   * Test creating isDerivedFrom relation in metadata and in RDFDB.
   * </p>
   * 
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testFuguWaffelfuttererImageFileRelations() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    String uri = "";
    try {
      System.out.println("Testing relations (isAlternativeForamtOf) with image file (file size: "
          + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

      // Create a TextGrid image object, get metadata and URI.
      MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
          logParameter, projectId, createFuguWaffelfuttererMetadataFilename,
          createFuguWaffelfuttererFilename, NO_WARNINGS);
      uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

      // Check for isAlternativeFormatOf relation metadata.
      String alternativeFormatOf = metadata.getObject().getRelations().getIsAlternativeFormatOf();
      assertTrue(alternativeFormatOf != null && alternativeFormatOf.equals("textgrid:48673.0"));

    } finally {
      // Delete, and READ.
      if (DELETE && !uri.isEmpty()) {
        TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
        TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
      }
    }
  }

  /**
   * <p>
   * Test the lol file: https://en.wikipedia.org/wiki/Billion_laughs
   * </p>
   *
   * <p>
   * Doesn't work, but nothing crashes either :-)
   * </p>
   *
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Ignore
  @Test(expected = ObjectNotFoundFault.class)
  public void testCreateBillionLaughtsFile() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#CREATE with a billion 'lol's (file size: "
        + TGCrudClientUtils.getResource(createLolFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createLolMetadataFilename, createLolFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests creating a new revision.
   * </p>
   *
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testRevisionIngestWithBaseUri()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing revision ingest with base URI");

    // Ingest first file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Create (and read) first revision.
    String firstRevision = createRevision(uri);

    // Create (and read) second revision.
    String secondRevision = createRevision(uri);

    // Delete all of them.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, firstRevision);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, secondRevision);
    }
  }

  /**
   * <p>
   * Tests creating new revisions, and deleting some of them.
   * </p>
   *
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testRevisionIngestWithBaseUriDeleteMiddleRevision()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out
        .println("Testing revision ingest with base URI, delete the one in the middle afterwards");

    // Create first file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Create (and read) first revision.
    String firstRevision = createRevision(uri);

    // Create (and read) second revision.
    String secondRevision = createRevision(uri);

    // Create (and read) third revision.
    String thirdRevision = createRevision(uri);

    // Delete the second.
    TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, secondRevision);

    // Delete the source.
    TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);

    // Delete the others.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, firstRevision);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, thirdRevision);
    }
  }

  /**
   * <p>
   * Tests creating three new revisions, then updating second one.
   * </p>
   *
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testRevisionIngestWithBaseUriUpdateSecond() throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault,
      RelationsExistFault, UpdateConflictFault, NoSuchAlgorithmException {

    System.out.println("Testing revision ingest with base URI, update then some of them");

    // Create first file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Create (and read) first revision.
    MetadataContainerType firstRevision = createRevisionReturnMetadata(uri);
    String firstRevisionUri =
        firstRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create (and read) second revision.
    MetadataContainerType secondRevision = createRevisionReturnMetadata(uri);
    String secondRevisionUri =
        secondRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create (and read) third revision.
    MetadataContainerType thirdRevision = createRevisionReturnMetadata(uri);
    String thirdRevisionUri =
        thirdRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update second revision.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        secondRevision.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Delete all.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, firstRevisionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, secondRevisionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, thirdRevisionUri);
    }
  }

  /**
   * <p>
   * Tests creating three new revisions, then updating third one.
   * </p>
   *
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testRevisionIngestWithBaseUriUpdateThird() throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault,
      RelationsExistFault, UpdateConflictFault, NoSuchAlgorithmException {

    System.out.println("Testing revision ingest with base URI, update then some of them");

    // Create first file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Create (and read) first revision.
    MetadataContainerType firstRevision = createRevisionReturnMetadata(uri);
    String firstRevisionUri =
        firstRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create (and read) second revision.
    MetadataContainerType secondRevision = createRevisionReturnMetadata(uri);
    String secondRevisionUri =
        secondRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create (and read) third revision.
    MetadataContainerType thirdRevision = createRevisionReturnMetadata(uri);
    String thirdRevisionUri =
        thirdRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update third revision.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        thirdRevision.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Delete all.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, firstRevisionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, secondRevisionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, thirdRevisionUri);
    }
  }

  /**
   * <p>
   * Tests creating three new revisions, then updating metadata of second one.
   * </p>
   *
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testRevisionIngestWithBaseUriUpdateSecondMetadata()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, RelationsExistFault, UpdateConflictFault,
      NoSuchAlgorithmException {

    System.out.println("Testing revision ingest with base URI, update then some of them");

    // Create first file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Create (and read) first revision.
    MetadataContainerType firstRevision = createRevisionReturnMetadata(uri);
    String firstRevisionUri =
        firstRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create (and read) second revision.
    MetadataContainerType secondRevision = createRevisionReturnMetadata(uri);
    String secondRevisionUri =
        secondRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create (and read) third revision.
    MetadataContainerType thirdRevision = createRevisionReturnMetadata(uri);
    String thirdRevisionUri =
        thirdRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update metadata of second revision.
    TGCrudClientUtils.updateMetadata(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateMetadataTiffImageMetadataFilename,
        secondRevision.getObject().getGeneric().getGenerated());

    // Delete all.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, firstRevisionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, secondRevisionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, thirdRevisionUri);
    }
  }

  /**
   * <p>
   * Tests creating three new revisions, then updating metadata of third one.
   * </p>
   *
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testRevisionIngestWithBaseUriUpdateThirdMetadata()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, RelationsExistFault, UpdateConflictFault,
      NoSuchAlgorithmException {

    System.out.println("Testing revision ingest with base URI, update then some of them");

    // Create first file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Create (and read) first revision.
    MetadataContainerType firstRevision = createRevisionReturnMetadata(uri);
    String firstRevisionUri =
        firstRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create (and read) second revision.
    MetadataContainerType secondRevision = createRevisionReturnMetadata(uri);
    String secondRevisionUri =
        secondRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create (and read) third revision.
    MetadataContainerType thirdRevision = createRevisionReturnMetadata(uri);
    String thirdRevisionUri =
        thirdRevision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Update metadata of third revision.
    TGCrudClientUtils.updateMetadata(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateMetadataTiffImageMetadataFilename,
        thirdRevision.getObject().getGeneric().getGenerated());

    // Delete all.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, firstRevisionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, secondRevisionUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, thirdRevisionUri);
    }
  }

  /**
   * <p>
   * Tests creating a bunch of more than 10 new revision.
   * </p>
   *
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws IOException
   * @throws FileNotFoundException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testRevisionIngestWithBaseUriPast10()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, RelationsExistFault, NoSuchAlgorithmException {

    // Amount of revisions to test.
    int amount = 12;

    System.out.println("Testing revision ingest with base URI past " + (amount) + " revisions");

    // Ingest first file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Ingest and read a bunch of 'amount' new revision (hopefully no reading will be needed here).
    String revisionUri[] = new String[amount];
    for (int i = 0; i < amount; i++) {
      revisionUri[i] = createRevision(uri);
    }

    // Delete all of them.
    if (DELETE) {
      for (int i = 0; i < amount; i++) {
        TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, revisionUri[i]);
      }
    }
  }

  /**
   * <p>
   * Tests moving an object to the public storage location.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws AuthenticationFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testMovePublic() throws FileNotFoundException, IOException, MetadataParseFault,
      IoFault, ObjectNotFoundFault, AuthFault, ProtocolNotImplementedFault, RelationsExistFault,
      AuthenticationFault, info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault,
      NoSuchAlgorithmException {

    System.out.println("Testing moving data to the public storage location");

    if (PROPERTIES_FILE.equals(PRODUCTIVE)) {
      System.out.println("NO TESTING OF PUBLISH METHODS ON PRODUCTIVE SYSTEM!");
    } else {

      // Ingest file.
      MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
          logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
          NO_WARNINGS);
      String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

      // Move.
      TGCrudClientUtils.move(tgcrud, crudPublishSecret, logParameter, uri);

      // Testing if TECHMD were created.
      if (TESTTECHMD && !testTechmd(tgcrudHttpClientPublic, uri)) {
        assertTrue(false);
      }

      // Set the isPublic flag in TG-auth.
      System.out.println("\tCalling TG-auth#PUBLISH");
      System.out.println("\t\turi:          " + uri);
      System.out.println("\t\t----------------------------------------");

      PublishRequest request = new PublishRequest();
      request.setSecret(tgauthCrudSecret);
      request.setResource(uri);
      request.setLog(logParameter);
      request.setAuth(rbacSessionId);

      BooleanResponse result = tgauthCrud.publish(request);

      // Check if isPublic flag set.
      IsPublicRequest iRequest = new IsPublicRequest();
      iRequest.setResource(uri);
      iRequest.setLog("");
      iRequest.setAuth(rbacSessionId);
      info.textgrid.namespaces.middleware.tgauth.BooleanResponse iResult =
          tgauth.isPublic(iRequest);

      if (!result.isResult() || !iResult.isResult()) {
        System.out.println("\t\tERROR: isPublic flag NOT set!");
        assertTrue(false);
      }

      // Read and read metadata again from the static storage location.
      TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

      // No delete possible here.
    }
  }

  /**
   * <p>
   * Tests moving an object to the public storage location and then create a new revision.
   * </p>
   *
   * NOTE Please ignore ON PRODUCTION SYSTEMS, because a moved public file can not be deleted again
   * from the static data storage! Data would become inconsistent!
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws AuthenticationFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testMovePublicAndNewRevision()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, RelationsExistFault, AuthenticationFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault,
      NoSuchAlgorithmException {

    System.out.println(
        "Testing moving data to the public storage location and then create a new revision!");

    if (PROPERTIES_FILE.equals(PRODUCTIVE)) {
      System.out.println("NO TESTING OF PUBLISH METHODS ON PRODUCTIVE SYSTEM!");
    } else {

      // Ingest file.
      MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
          logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
          NO_WARNINGS);
      String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

      // Move.
      TGCrudClientUtils.move(tgcrud, crudPublishSecret, logParameter, uri);

      // Set the isPublic flag in TG-auth.
      System.out.println("\tCalling TG-auth#PUBLISH");
      System.out.println("\t\turi:          " + uri);
      System.out.println("\t\t----------------------------------------");

      PublishRequest request = new PublishRequest();
      request.setSecret(tgauthCrudSecret);
      request.setResource(uri);
      request.setLog(logParameter);
      request.setAuth(rbacSessionId);

      BooleanResponse result = tgauthCrud.publish(request);

      // Check if isPublic flag set.
      IsPublicRequest iRequest = new IsPublicRequest();
      iRequest.setResource(uri);
      iRequest.setLog("");
      iRequest.setAuth(rbacSessionId);
      info.textgrid.namespaces.middleware.tgauth.BooleanResponse iResult =
          tgauth.isPublic(iRequest);

      if (!result.isResult() || !iResult.isResult()) {
        System.out.println("\t\tERROR: isPublic flag NOT set!");
        assertTrue(false);
      }

      // Read again from the static storage location.
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

      // Create a new revision.
      MetadataContainerType revisionMetadata =
          TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
              createTestFileMetadataFilename, createTestFileFilename, uri, true, NO_WARNINGS);
      String revisionUri =
          revisionMetadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

      // Read new revision from the static storage location.
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, revisionUri);

      // No delete possible here.
    }
  }

  /**
   * <p>
   * Test mandatory metadata: tgObjectMetadata.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  @Test(expected = MetadataParseFault.class)
  public void testMetadataFile() throws FileNotFoundException, IOException, MetadataParseFault,
      IoFault, ObjectNotFoundFault, AuthFault {

    System.out.println("Testing metadata file for tgObjectMetadata");

    // Try creating a TextGrid object without format metadata.
    TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createMetadataNoMetadata, createTinyXmlFilename, NO_WARNINGS);
  }

  /**
   * <p>
   * Test mandatory metadata: GenericType.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  @Test(expected = MetadataParseFault.class)
  public void testMandatoryGenericType() throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault {

    System.out.println("Testing mandatory generic metadata type");

    // Try creating a TextGrid object without format metadata.
    TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createMetadataWithoutGenericType, createTinyXmlFilename, NO_WARNINGS);
  }

  /**
   * <p>
   * Test mandatory metadata: ProvidedType.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  @Test(expected = MetadataParseFault.class)
  public void testMandatoryProvidedType() throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault {

    System.out.println("Testing mandatory provided metadata type");

    // Try creating a TextGrid object without format metadata.
    TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createMetadataWithoutProvidedType, createTinyXmlFilename, NO_WARNINGS);
  }

  /**
   * <p>
   * Test mandatory metadata parameter: title.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  @Test(expected = MetadataParseFault.class)
  public void testMandatoryMissingTitle() throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault {

    System.out.println("Testing mandatory title metadata");

    // Try creating a TextGrid object without title metadata.
    TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createMetadataWithoutTitleFilename, createTinyTiffImageFilename, NO_WARNINGS);
  }

  /**
   * <p>
   * Test mandatory metadata parameter: format.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   */
  @Test(expected = MetadataParseFault.class)
  public void testMandatoryMissingFormat() throws FileNotFoundException, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault {

    System.out.println("Testing mandatory title metadata");

    // Try creating a TextGrid object without format metadata.
    TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
        createMetadataWithoutFormatFilename, createTinyTiffImageFilename, NO_WARNINGS);
  }

  /**
   * <p>
   * Tests a revision creation, with the old and new revision existing in different projects.
   * Project ID will be rewritten.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testRevisionDifferentProjects()
      throws FileNotFoundException, IOException, MetadataParseFault, ObjectNotFoundFault, AuthFault,
      IoFault, ProtocolNotImplementedFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing creation of a new revision (different projects)");

    // Create initial object with user1.
    MetadataContainerType initial = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String initialUri = initial.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create new revision in another project with user2.
    MetadataContainerType revision =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionI2, logParameter, projectI2,
            createTestFileMetadataFilename, createTestFileFilename, initialUri, NEW_REVISION,
            NO_WARNINGS);
    String revisionUri =
        revision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read initial and revision as user 1.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, initialUri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, revisionUri);

    // Read initial and revision as user 2.
    TGCrudClientUtils.read(tgcrud, rbacSessionI2, logParameter, initialUri);
    TGCrudClientUtils.read(tgcrud, rbacSessionI2, logParameter, revisionUri);

    // Delete both as user 1.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, initialUri);
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, revisionUri);
    }
  }

  /**
   * <p>
   * Tests a revision creation using a project ID with no access rights. Project ID will be
   * rewritten.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testRevisionInvalidProject()
      throws FileNotFoundException, IOException, MetadataParseFault, ObjectNotFoundFault, AuthFault,
      IoFault, ProtocolNotImplementedFault, RelationsExistFault {

    System.out.println("Testing creation of a new revision (invalid project ID)");

    String initialUri = "";
    try {
      // Create initial object.
      MetadataContainerType initial = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
          logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
          NO_WARNINGS);
      initialUri = initial.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

      // Try creating new revision using an invalid project ID.
      TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectIdInvalid,
          createTestFileMetadataFilename, createTestFileFilename, initialUri, NEW_REVISION,
          NO_WARNINGS);
    } finally {
      // Delete.
      if (DELETE && !initialUri.equals("")) {
        TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, initialUri);
      }
    }
  }

  /**
   * <p>
   * Tests concurrent update access, uses internal locking.
   * </p>
   *
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws AuthenticationFault
   */
  @Test
  public void testConcurrentAccessInternalLocking()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, RelationsExistFault, AuthenticationFault {

    System.out.println("Testing concurrent access to same URI");

    // Ingest initial file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Start many update threads, one update will be faster :)
    TGCrudUpdateTestThread updateThread1 =
        new TGCrudUpdateTestThread(tgcrud, tgsearch, rbacSessionId,
            logParameter, updateEmptyFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudUpdateTestThread updateThread2 =
        new TGCrudUpdateTestThread(tgcrud, tgsearch, rbacSessionId,
            logParameter, updateEmptyFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudUpdateTestThread updateThread3 =
        new TGCrudUpdateTestThread(tgcrud, tgsearch, rbacSessionId,
            logParameter, updateEmptyFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudUpdateTestThread updateThread4 =
        new TGCrudUpdateTestThread(tgcrud, tgsearch, rbacSessionId,
            logParameter, updateEmptyFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudUpdateTestThread updateThread5 =
        new TGCrudUpdateTestThread(tgcrud, tgsearch, rbacSessionId,
            logParameter, updateEmptyFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudUpdateTestThread updateThread6 =
        new TGCrudUpdateTestThread(tgcrud, tgsearch, rbacSessionId,
            logParameter, updateEmptyFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudUpdateTestThread updateThread7 =
        new TGCrudUpdateTestThread(tgcrud, tgsearch, rbacSessionId,
            logParameter, updateEmptyFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudUpdateTestThread updateThread8 =
        new TGCrudUpdateTestThread(tgcrud, tgsearch, rbacSessionId,
            logParameter, updateEmptyFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudUpdateTestThread updateThread9 =
        new TGCrudUpdateTestThread(tgcrud, tgsearch, rbacSessionId,
            logParameter, updateEmptyFileMetadataFilename, createTinyTiffImageFilename, metadata);
    updateThread1.start();
    updateThread2.start();
    updateThread3.start();
    updateThread4.start();
    updateThread5.start();
    updateThread6.start();
    updateThread7.start();
    updateThread8.start();
    updateThread9.start();

    while (updateThread1.isAlive() || updateThread2.isAlive() || updateThread3.isAlive()
        || updateThread4.isAlive() || updateThread5.isAlive() || updateThread6.isAlive()
        || updateThread7.isAlive() || updateThread8.isAlive() || updateThread9.isAlive()) {
      try {
        System.out.println("\tThreads still running: "
            + (updateThread1.isAlive() ? updateThread1.getName() + " " : "")
            + (updateThread2.isAlive() ? updateThread2.getName() + " " : "")
            + (updateThread3.isAlive() ? updateThread3.getName() + " " : "")
            + (updateThread4.isAlive() ? updateThread4.getName() + " " : "")
            + (updateThread5.isAlive() ? updateThread5.getName() + " " : "")
            + (updateThread6.isAlive() ? updateThread6.getName() + " " : "")
            + (updateThread7.isAlive() ? updateThread7.getName() + " " : "")
            + (updateThread8.isAlive() ? updateThread8.getName() + " " : "")
            + (updateThread9.isAlive() ? updateThread9.getName() + " " : ""));
        Thread.sleep(1000);
      } catch (InterruptedException ie) {
        //
      }
    }

    // Delete file if all threads have finished.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests concurrently creating new revisions on the same object.
   * </p>
   * 
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws AuthenticationFault
   */
  @Test
  public void testConcurrentCreateNewRevision()
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, RelationsExistFault, AuthenticationFault {

    System.out.println("Testing concurrent creation of new revisions to same URI");

    // Ingest initial file.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Start many create new revision threads, one create will be faster :)
    TGCrudCreateRevisionTestThread createThread1 =
        new TGCrudCreateRevisionTestThread(tgcrud, tgsearch, rbacSessionId, logParameter,
            createTestFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudCreateRevisionTestThread createThread2 =
        new TGCrudCreateRevisionTestThread(tgcrud, tgsearch, rbacSessionId, logParameter,
            createTestFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudCreateRevisionTestThread createThread3 =
        new TGCrudCreateRevisionTestThread(tgcrud, tgsearch, rbacSessionId, logParameter,
            createTestFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudCreateRevisionTestThread createThread4 =
        new TGCrudCreateRevisionTestThread(tgcrud, tgsearch, rbacSessionId, logParameter,
            createTestFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudCreateRevisionTestThread createThread5 =
        new TGCrudCreateRevisionTestThread(tgcrud, tgsearch, rbacSessionId, logParameter,
            createTestFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudCreateRevisionTestThread createThread6 =
        new TGCrudCreateRevisionTestThread(tgcrud, tgsearch, rbacSessionId, logParameter,
            createTestFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudCreateRevisionTestThread createThread7 =
        new TGCrudCreateRevisionTestThread(tgcrud, tgsearch, rbacSessionId, logParameter,
            createTestFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudCreateRevisionTestThread createThread8 =
        new TGCrudCreateRevisionTestThread(tgcrud, tgsearch, rbacSessionId, logParameter,
            createTestFileMetadataFilename, createTinyTiffImageFilename, metadata);
    TGCrudCreateRevisionTestThread createThread9 =
        new TGCrudCreateRevisionTestThread(tgcrud, tgsearch, rbacSessionId, logParameter,
            createTestFileMetadataFilename, createTinyTiffImageFilename, metadata);
    createThread1.start();
    createThread2.start();
    createThread3.start();
    createThread4.start();
    createThread5.start();
    createThread6.start();
    createThread7.start();
    createThread8.start();
    createThread9.start();

    while (createThread1.isAlive() || createThread2.isAlive() || createThread3.isAlive()
        || createThread4.isAlive() || createThread5.isAlive() || createThread6.isAlive()
        || createThread7.isAlive() || createThread8.isAlive() || createThread9.isAlive()) {
      try {
        System.out.println("\tThreads still running: "
            + (createThread1.isAlive() ? createThread1.getName() + " " : "")
            + (createThread2.isAlive() ? createThread2.getName() + " " : "")
            + (createThread3.isAlive() ? createThread3.getName() + " " : "")
            + (createThread4.isAlive() ? createThread4.getName() + " " : "")
            + (createThread5.isAlive() ? createThread5.getName() + " " : "")
            + (createThread6.isAlive() ? createThread6.getName() + " " : "")
            + (createThread7.isAlive() ? createThread7.getName() + " " : "")
            + (createThread8.isAlive() ? createThread8.getName() + " " : "")
            + (createThread9.isAlive() ? createThread9.getName() + " " : ""));
        Thread.sleep(1000);
      } catch (InterruptedException ie) {
        //
      }
    }

    System.out.println("------------------------------------------------------------");
    System.out.println("\t--> created: " + TGCrudCreateRevisionTestThread.createdList);
    System.out.println("\t-->  errors: " + TGCrudCreateRevisionTestThread.errorsList);
    System.out.println("\t--> experrs: " + TGCrudCreateRevisionTestThread.expectedErrorsList);
    System.out.println("------------------------------------------------------------");

    // Delete file and all created revisions after all threads have finished.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      for (String u : TGCrudCreateRevisionTestThread.createdList) {
        TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, u);
      }
    }
  }

  /**
   * <p>
   * Tests the nearlyPublish() method of the tgextra-crud WSDL, deletes publication again.
   * </p>
   *
   * <p>
   * Expected behaviour:
   * <ul>
   * <li>set the isPublic flag</li>
   * <li>set rights to: "read delete publish"</li>
   * <li>delete the object at the end successfully</li>
   * </ul>
   * </p>
   *
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   * @throws DatatypeConfigurationException
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testNearlyPublishAndDelete()
      throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, RelationsExistFault,
      AuthenticationFault, DatatypeConfigurationException, ProtocolNotImplementedFault,
      NoSuchAlgorithmException {

    System.out.println("Testing nearlyPublish on public TG-crud (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length()
        + " bytes), then delete again");

    // Create a first TextGrid object, with generated metadata for publication already included, get
    // metadata and URI.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter,
            projectId, createTiffImagePublicMetadataFilename, createTinyTiffImageFilename,
            NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create nearlyPublish request, call nearlyPublish().
    NearlyPublishRequest npparams = new NearlyPublishRequest();
    npparams.setSecret(tgauthCrudSecret);
    npparams.setAuth(rbacSessionId);
    npparams.setLog(logParameter);
    npparams.setResource(uri);

    boolean npresponse = tgauthCrud.nearlyPublish(npparams).isResult();

    System.out.println("\tnearlyPublish: " + npresponse);

    if (!npresponse) {
      assertTrue(false);
    }

    // Check nearlyPublish consequence: isPublic.
    IsPublicRequest ipparams = new IsPublicRequest();
    ipparams.setAuth(rbacSessionId);
    ipparams.setLog(logParameter);
    ipparams.setResource(uri);

    boolean ipresponse = tgauth.isPublic(ipparams).isResult();

    System.out.println("\tisPublic:      " + ipresponse);

    if (!ipresponse) {
      assertTrue(false);
    }

    // Check nearlyPublish consequence: rights.
    GetRightsRequest grparams = new GetRightsRequest();
    grparams.setAuth(rbacSessionId);
    grparams.setLog(logParameter);
    grparams.setResource(uri);

    List<String> grresponse = tgauth.getRights(grparams).getOperation();

    System.out.println("\trights: " + grresponse);

    if (!(grresponse.size() == 3 && grresponse.contains("read") && grresponse.contains("publish")
        && grresponse.contains("delete"))) {
      assertTrue(false);
    }

    // Test reading without SessionID!
    TGCrudClientUtils.read(tgcrud, "", logParameter, uri);

    // Check for TECHMD, using TextGrid URI.
    if (TESTTECHMD && !testTechmd(tgcrudHttpClientPublic, uri)) {
      System.out.println("ERROR! No TECHMD found for URI " + uri);
      assertTrue(false);
    }

    // Delete and check READ and READTECHMD.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter, uri);
      try {
        if (TESTTECHMD) {
          testTechmd(tgcrudHttpClientPublic, uri);
        }
      } catch (IOException e) {
        // Gut so!
      }
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests the nearlyPublish() method of the tgextra-crud WSDL, tries to create new revision of
   * published object, deletes publications again.
   * </p>
   *
   * <p>
   * Expected behaviour:
   * <ul>
   * <li>set the isPublic flag</li>
   * <li>set rights to: "read delete publish"</li>
   * <li>create new revision</li>
   * <li>delete the object(s) at the end successfully</li>
   * </ul>
   * </p>
   *
   * NOTE: Don't know if that is exactly what I want to be tested...
   *
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   * @throws DatatypeConfigurationException
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test
  public void testNewRevisionWithNearlyPublishedObject()
      throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, RelationsExistFault,
      AuthenticationFault, DatatypeConfigurationException, ProtocolNotImplementedFault,
      NoSuchAlgorithmException {

    System.out.println(
        "Testing revision handling with (nearly) published object on non-public TG-crud (file size: "
            + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length()
            + " bytes), then delete again");

    // Create a first TextGrid object, with generated metadata for publication already included, get
    // metadata and URI.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter,
            projectId, createTiffImagePublicMetadataFilename, createTinyTiffImageFilename,
            NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create nearlyPublish request, call nearlyPublish().
    NearlyPublishRequest npparams = new NearlyPublishRequest();
    npparams.setSecret(tgauthCrudSecret);
    npparams.setAuth(rbacSessionId);
    npparams.setLog(logParameter);
    npparams.setResource(uri);

    boolean npresponse = tgauthCrud.nearlyPublish(npparams).isResult();

    System.out.println("\tnearlyPublish: " + npresponse);

    if (!npresponse) {
      assertTrue(false);
    }

    // Try creating a new revision with (nearly) published object.
    MetadataContainerType revision =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createTiffImagePublicMetadataFilename, createTinyTiffImageFilename, uri, true,
            NO_WARNINGS);
    String revisionUri =
        revision.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Read initial and revision.
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, revisionUri);

    // Delete and check READ and READTECHMD.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, revisionUri);
      TGCrudClientUtils.delete(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests the nearlyPublish() method of the tgextra-crud WSDL, deletes publication again. Using
   * AGGREGATION file!
   * </p>
   *
   * <p>
   * Expected behaviour:
   * <ul>
   * <li>set the isPublic flag</li>
   * <li>set rights to: "read delete publish"</li>
   * <li>delete the object at the end successfully</li>
   * </ul>
   * </p>
   *
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   * @throws DatatypeConfigurationException
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testNearlyPublishAndDeleteAggregation()
      throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, RelationsExistFault,
      AuthenticationFault, DatatypeConfigurationException, ProtocolNotImplementedFault,
      NoSuchAlgorithmException {

    System.out.println("Testing nearlyPublish of AGGREGATION FILE on public TG-crud (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length()
        + " bytes), then delete again");

    // Create a first TextGrid object, with generated metadata for
    // publication already included, get metadata and URI.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter,
            projectId, createAggregationPublicMetadataFilename, createAggregationFilename,
            NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Create nearlyPublish request, call nearlyPublish().
    NearlyPublishRequest npparams = new NearlyPublishRequest();
    npparams.setSecret(tgauthCrudSecret);
    npparams.setAuth(rbacSessionId);
    npparams.setLog(logParameter);
    npparams.setResource(uri);

    boolean npresponse = tgauthCrud.nearlyPublish(npparams).isResult();

    System.out.println("\tnearlyPublish: " + npresponse);

    if (!npresponse) {
      assertTrue(false);
    }

    // Check nearlyPublish consequence: isPublic.
    IsPublicRequest ipparams = new IsPublicRequest();
    ipparams.setAuth(rbacSessionId);
    ipparams.setLog(logParameter);
    ipparams.setResource(uri);

    boolean ipresponse = tgauth.isPublic(ipparams).isResult();

    System.out.println("\tisPublic:      " + ipresponse);

    if (!ipresponse) {
      assertTrue(false);
    }

    // Check nearlyPublish consequence: rights.
    GetRightsRequest grparams = new GetRightsRequest();
    grparams.setAuth(rbacSessionId);
    grparams.setLog(logParameter);
    grparams.setResource(uri);

    List<String> grresponse = tgauth.getRights(grparams).getOperation();

    System.out.println("\trights: " + grresponse);

    if (!(grresponse.size() == 3 && grresponse.contains("read") && grresponse.contains("publish")
        && grresponse.contains("delete"))) {
      assertTrue(false);
    }

    // Check for TECHMD, using TextGrid URI.
    if (TESTTECHMD && !testTechmd(tgcrudHttpClientPublic, uri)) {
      System.out.println("ERROR! No TECHMD found for URI " + uri);
      assertTrue(false);
    }

    // Delete and check READ and READTECHMD.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter, uri);
      try {
        if (TESTTECHMD) {
          testTechmd(tgcrudHttpClientPublic, uri);
        }
      } catch (IOException e) {
        // Gut so!
      }
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests the nearlyPublish() method of the tgextra-crud WSDL, publishes afterwards, tries to
   * delete at the end (using a TIFF image).
   * </p>
   *
   * <p>
   * Expected behaviour:
   * <ul>
   * <li>set the isPublic flag</li>
   * <li>set rights to: "read delete publish"</li>
   * <li>publish the object at the end successfully</li>
   * <li>be NOT able to delete at the end</li>
   * <li>correct filesize and checksum!</li>
   * </ul>
   * </p>
   *
   * NOTE Be sure that you ignore this test ON THE PRODUCTION SYSTEM! Objects will be published
   * afterwards!
   *
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   * @throws DatatypeConfigurationException
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = AuthFault.class)
  public void testNearlyPublishAndPublishTiffImage()
      throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, RelationsExistFault,
      AuthenticationFault, DatatypeConfigurationException, NoSuchAlgorithmException,
      ProtocolNotImplementedFault {

    if (PROPERTIES_FILE.equals(PRODUCTIVE)) {
      System.out.println("NO TESTING OF PUBLISH METHODS ON PRODUCTIVE SYSTEM!");
      throw new AuthFault("Test ignored");
    } else {

      System.out.println("Testing nearlyPublish on public TG-crud (file size: "
          + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length()
          + " bytes), then publish");

      // Create a first TextGrid object, with generated metadata for
      // publication already included, get metadata and URI.
      MetadataContainerType metadata =
          TGCrudClientUtils.create(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter,
              projectId, createTiffImagePublicMetadataFilename, createTinyTiffImageFilename,
              NO_WARNINGS);
      String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

      // Create nearlyPublish request, call nearlyPublish().
      System.out.println("\t--> nearlyPublish()");

      NearlyPublishRequest npparams = new NearlyPublishRequest();
      npparams.setSecret(tgauthCrudSecret);
      npparams.setAuth(rbacSessionId);
      npparams.setLog(logParameter);
      npparams.setResource(uri);

      boolean npresponse = tgauthCrud.nearlyPublish(npparams).isResult();

      System.out.println("\tnearlyPublish: " + npresponse);

      if (!npresponse) {
        assertTrue(false);
      }

      // Check nearlyPublish consequence: isPublic.
      System.out.println("\t--> isPublic()");

      IsPublicRequest ipparams = new IsPublicRequest();
      ipparams.setAuth(rbacSessionId);
      ipparams.setLog(logParameter);
      ipparams.setResource(uri);

      boolean ipresponse = tgauth.isPublic(ipparams).isResult();

      System.out.println("\tisPublic:      " + ipresponse);

      if (!ipresponse) {
        assertTrue(false);
      }

      // Check nearlyPublish consequence: rights.
      System.out.println("\t--> getRights()");

      GetRightsRequest grparams = new GetRightsRequest();
      grparams.setAuth(rbacSessionId);
      grparams.setLog(logParameter);
      grparams.setResource(uri);

      List<String> grresponse = tgauth.getRights(grparams).getOperation();

      System.out.println("\trights: " + grresponse);

      if (!(grresponse.size() == 3 && grresponse.contains("read") && grresponse.contains("publish")
          && grresponse.contains("delete"))) {
        assertTrue(false);
      }

      // Publish.
      System.out.println("\t--> publish()");

      PublishRequest pparams = new PublishRequest();
      pparams.setSecret(tgauthCrudSecret);
      pparams.setAuth(rbacSessionId);
      pparams.setLog(logParameter);
      pparams.setResource(uri);

      boolean presponse = tgauthCrud.publish(pparams).isResult();

      if (!presponse) {
        assertTrue(false);
      }

      System.out.println("\tPublish: " + npresponse);

      // Check publish consequence: rights.
      System.out.println("\t--> getRights()");

      grparams = new GetRightsRequest();
      grparams.setAuth(rbacSessionId);
      grparams.setLog(logParameter);
      grparams.setResource(uri);

      try {
        grresponse = tgauth.getRights(grparams).getOperation();

      } catch (Exception e) {
        System.out
            .println("unexpected exception! " + e.getClass().getName() + ": " + e.getMessage());
      }

      System.out.println("\trights: " + grresponse);

      if (!(grresponse.size() == 1 && grresponse.contains("read"))) {
        assertTrue(false);
      }

      // Test reading without SessionID!
      TGCrudClientUtils.read(tgcrud, "", logParameter, uri);

      // Check for TECHMD, using TextGrid URI.
      if (TESTTECHMD && !testTechmd(tgcrudHttpClientPublic, uri)) {
        System.out.println("ERROR! No TECHMD found for URI " + uri);
        assertTrue(false);
      }

      // Delete.
      if (DELETE) {
        TGCrudClientUtils.delete(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter, uri);
        try {
          if (TESTTECHMD) {
            testTechmd(tgcrudHttpClientPublic, uri);
          }
        } catch (IOException e) {
          // Gut so!
        }
      }
    }
  }

  /**
   * <p>
   * Tests the nearlyPublish() method of the tgextra-crud WSDL, publishes afterwards, tries to
   * delete at the end (using an aggregation file).
   * </p>
   *
   * <p>
   * Expected behaviour:
   * <ul>
   * <li>set the isPublic flag</li>
   * <li>set rights to: "read delete publish"</li>
   * <li>publish the object at the end successfully</li>
   * <li>be NOT able to delete at the end</li>
   * <li>correct filesize and checksum!</li>
   * </ul>
   * </p>
   *
   * NOTE Be sure that you ignore this test ON THE PRODUCTION SYSTEM! Objects will be published
   * afterwards!
   *
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault
   * @throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault
   * @throws IOException
   * @throws AuthFault
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws MetadataParseFault
   * @throws RelationsExistFault
   * @throws DatatypeConfigurationException
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = AuthFault.class)
  public void testNearlyPublishAndPublishAggregation()
      throws info.textgrid.namespaces.middleware.tgauth_crud.UnknownResourceFault,
      info.textgrid.namespaces.middleware.tgauth_crud.AuthenticationFault, IOException,
      MetadataParseFault, IoFault, ObjectNotFoundFault, AuthFault, RelationsExistFault,
      AuthenticationFault, DatatypeConfigurationException, NoSuchAlgorithmException,
      ProtocolNotImplementedFault {

    if (PROPERTIES_FILE.equals(PRODUCTIVE)) {
      System.out.println("NO TESTING OF PUBLISH METHODS ON PRODUCTIVE SYSTEM!");
      throw new AuthFault("Test ignored");
    } else {

      System.out.println("Testing nearlyPublish on public TG-crud (file size: "
          + TGCrudClientUtils.getResource(createAggregationFilename).length()
          + " bytes), then publish");

      // Create a first TextGrid object, with generated metadata for publication already included,
      // get metadata and URI.
      MetadataContainerType metadata =
          TGCrudClientUtils.create(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter,
              projectId, createAggregationPublicMetadataFilename, createAggregationFilename,
              NO_WARNINGS);
      String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

      // Create nearlyPublish request, call nearlyPublish().
      NearlyPublishRequest npparams = new NearlyPublishRequest();
      npparams.setSecret(tgauthCrudSecret);
      npparams.setAuth(rbacSessionId);
      npparams.setLog(logParameter);
      npparams.setResource(uri);

      boolean npresponse = tgauthCrud.nearlyPublish(npparams).isResult();

      System.out.println("\tnearlyPublish: " + npresponse);

      if (!npresponse) {
        assertTrue(false);
      }

      // Check nearlyPublish consequence: isPublic.
      IsPublicRequest ipparams = new IsPublicRequest();
      ipparams.setAuth(rbacSessionId);
      ipparams.setLog(logParameter);
      ipparams.setResource(uri);

      boolean ipresponse = tgauth.isPublic(ipparams).isResult();

      System.out.println("\tisPublic:      " + ipresponse);

      if (!ipresponse) {
        assertTrue(false);
      }

      // Check nearlyPublish consequence: rights.
      GetRightsRequest grparams = new GetRightsRequest();
      grparams.setAuth(rbacSessionId);
      grparams.setLog(logParameter);
      grparams.setResource(uri);

      List<String> grresponse = tgauth.getRights(grparams).getOperation();

      System.out.println("\trights: " + grresponse);

      if (!(grresponse.size() == 3 && grresponse.contains("read") && grresponse.contains("publish")
          && grresponse.contains("delete"))) {
        assertTrue(false);
      }

      // Publish.
      PublishRequest pparams = new PublishRequest();
      pparams.setSecret(tgauthCrudSecret);
      pparams.setAuth(rbacSessionId);
      pparams.setLog(logParameter);
      pparams.setResource(uri);

      boolean presponse = tgauthCrud.publish(pparams).isResult();

      if (!presponse) {
        assertTrue(false);
      }

      System.out.println("\tPublish: " + npresponse);

      // Check publish consequence: rights.
      grparams = new GetRightsRequest();
      grparams.setAuth(rbacSessionId);
      grparams.setLog(logParameter);
      grparams.setResource(uri);

      grresponse = tgauth.getRights(grparams).getOperation();

      System.out.println("\trights: " + grresponse);

      if (!(grresponse.size() == 1 && grresponse.contains("read"))) {
        assertTrue(false);
      }

      // Test reading without SessionID!
      TGCrudClientUtils.read(tgcrud, "", logParameter, uri);

      // Check for TECHMD, using TextGrid URI.
      if (TESTTECHMD && !testTechmd(tgcrudHttpClientPublic, uri)) {
        System.out.println("ERROR! No TECHMD found for URI " + uri);
        assertTrue(false);
      }

      // Delete.
      if (DELETE) {
        TGCrudClientUtils.delete(tgcrudPublic, tgsearchPublic, rbacSessionId, logParameter, uri);
        try {
          if (TESTTECHMD) {
            testTechmd(tgcrudHttpClientPublic, uri);
          }
        } catch (IOException e) {
          // Gut so!
        }
      }
    }
  }

  /**
   * <p>
   * Tests the basic TG-crud locking features using a locked object.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws InterruptedException
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testSimpleLockingAndUnlocking() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, InterruptedException, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud simple locking and unlocking features (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Lock the object.
    TGCrudClientUtils.lock(tgcrud, rbacSessionId, logParameter, uri);

    // Re-lock the object.
    TGCrudClientUtils.lock(tgcrud, rbacSessionId, logParameter, uri);

    // Update and read the metadata.
    TGCrudClientUtils.updateMetadata(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateMetadataTiffImageMetadataFilename, metadata.getObject().getGeneric().getGenerated());
    metadata = TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);

    // Update and read all.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    // Unlock.
    TGCrudClientUtils.unlock(tgcrud, rbacSessionId, logParameter, uri);

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests the basic TG-crud locking feature with a base URI.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws InterruptedException
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testLockingWithBaseURI() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, InterruptedException, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud simple locking with base URI");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Lock the object with the base URI.
    String baseUri = uri.substring(0, uri.indexOf('.'));

    System.out.println(baseUri);

    try {
      TGCrudClientUtils.lock(tgcrud, rbacSessionId, logParameter, baseUri);
    } catch (ObjectNotFoundFault e) {

      System.out.println("Expected ObjectNotFoundFault locking with base URI! Deleting...");

      // Delete, and READ.
      if (DELETE) {
        TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
        TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
      }
    }
  }

  /**
   * <p>
   * Tests the TG-crud locking features using one locked object and two users in one project both
   * with role "Bearbeiter", see #12253.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws InterruptedException
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testMultipleUserLocking() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, RelationsExistFault,
      InterruptedException, UpdateConflictFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud multiple user locking and unlocking features (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI (user 1) >> OK.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Lock the object (user 1) >> OK.
    TGCrudClientUtils.lock(tgcrud, rbacSessionId, logParameter, uri);

    // Read the object (user 2) >> OK.
    TGCrudClientUtils.read(tgcrud, rbacSessionI2, logParameter, uri);

    // Update the object (user 2) >> ERROR!
    try {
      TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionI2, logParameter,
          updateTestFileMetadataFilename, updateTestFileFilename,
          metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

      // If updating does work, fail the test!
      assertTrue(false);
    } catch (IoFault e) {
      // Ignoring expected IoFault!
    }

    // Unlock the object (user 1) >> OK.
    TGCrudClientUtils.unlock(tgcrud, rbacSessionId, logParameter, uri);

    // Update the object (user 2) >> OK.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionI2, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Delete the object (user 1) >> OK, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests the TG-crud locking features using one locked object and two users in one project both
   * with role "Bearbeiter", see #12253. Use several revisions.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws RelationsExistFault
   * @throws InterruptedException
   * @throws UpdateConflictFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testMultipleUserLockingWithRevisions() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      RelationsExistFault, InterruptedException, UpdateConflictFault, NoSuchAlgorithmException {

    System.out.println(
        "Testing TG-crud multiple user locking and unlocking features with revisions (file size: "
            + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI (user 1) >> OK.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    String baseUri = uri.replace(".0", "");

    // Create many revisions, use the latest for further testing.
    int revisions = 11;
    for (int i = 0; i < revisions; i++) {
      metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
          createTiffImageMetadataFilename, createTinyTiffImageFilename, uri, NEW_REVISION,
          NO_WARNINGS);
      uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    }

    // Lock the object (user 1) >> OK.
    TGCrudClientUtils.lock(tgcrud, rbacSessionId, logParameter, uri);

    // Read the object (user 2) >> OK.
    TGCrudClientUtils.read(tgcrud, rbacSessionI2, logParameter, uri);

    // Update the object (user 2) >> ERROR!
    try {
      TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionI2, logParameter,
          updateTestFileMetadataFilename, updateTestFileFilename,
          metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

      // If updating does work, fail the test!
      assertTrue(false);
    } catch (IoFault e) {
      // Ignoring expected IoFault!
    }

    // Unlock the object (user 1) >> OK.
    TGCrudClientUtils.unlock(tgcrud, rbacSessionId, logParameter, uri);

    // Update the object (user 2) >> OK.
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionI2, logParameter,
        updateTestFileMetadataFilename, updateTestFileFilename,
        metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);

    // Delete all objects (user 1) >> OK, and READ.
    if (DELETE) {
      for (int i = 0; i < revisions + 1; i++) {
        TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, baseUri + "." + i);
      }
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Test deleting a locked object, the locker is owner.
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws InterruptedException
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testDeleteWithoutUnlocking() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, InterruptedException, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud simple locking and deleting (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Lock the object.
    TGCrudClientUtils.lock(tgcrud, rbacSessionId, logParameter, uri);

    // Do NOT unlock the object!

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * Tests if automagic locking is working with two users on one file.
   * </p>
   *
   * PLEASE NOTE Test is running for approximately 30 Minutes, depending on the automagic unlocking
   * time set in the TG-crud's config! Maybe we simply ignore it!
   *
   * @throws IOException
   * @throws XMLStreamException
   * @throws MetadataParseFault
   * @throws AuthFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws InterruptedException
   * @throws NoSuchAlgorithmException
   */
  @Ignore
  @Test(expected = ObjectNotFoundFault.class)
  public void testAutomagicLocking() throws IOException, XMLStreamException, MetadataParseFault,
      AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault, UpdateConflictFault,
      RelationsExistFault, InterruptedException, NoSuchAlgorithmException {

    System.out.println("Testing automagic locking with two users on one file (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // 1. User (a) creates a plain text file and locks it
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTestFileMetadataFilename, createTestFileFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.lock(tgcrud, rbacSessionId, logParameter, uri);

    // 2. User (b) reads that file --> should work!
    TGCrudClientUtils.read(tgcrud, rbacSessionI2, logParameter, uri);

    // 3. User (a) keeps the file locked (means: he does nothing :-)

    // 4. User (b) tries to update the file --> should NOT work until
    // automagic unlocking time has passed!

    // Wait five seconds by default.
    long wait = 10000;
    if (automagicUnlockingTime < 60000) {
      // Wait one second if automagicUnlockingTime is less than a minute.
      wait = 1000;
    }

    for (long i = 0; i < automagicUnlockingTime; i = i + wait) {
      Thread.sleep(wait);
      // Try again until we get an UpdateConflictFault.
      try {
        TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionI2, logParameter,
            updateTestFileMetadataFilename, updateTestFileFilename,
            metadata.getObject().getGeneric().getGenerated(), NO_WARNINGS);
      } catch (IoFault e) {
        System.out.println("\t\tExpected IoFault! Next try in about " + (wait / 1000) + " sec"
            + ((wait / 1000) != 1 ? "s" : "") + "...");
      } catch (UpdateConflictFault e) {
        System.out.println("\t\tExpected UpdateConflictFault! File has been updated successfully!");
        break;
      }
    }

    // 5. User (a) deletes the file again, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionI2, logParameter, uri);
    }
  }

  /**
   * <p>
   * Runs update metadata with merkwürdische namespace in RDF image metadata (bug#349). It is about
   * double defined namespace prefix n2! datatype is defined in manespace exif, but must be rdf!
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testUpdateMetadataBug349() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#UPDATEMETADATA with j.cook.up namespace prefix (file size: "
        + TGCrudClientUtils.getResource(createTinyTiffImageFilename).length() + " bytes)");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata = TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId,
        logParameter, projectId, createTiffImageMetadataFilename, createTinyTiffImageFilename,
        NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // Add complete (and invalid) relation content.
    String merkwuerdischerRdfContent =
        "<RDF xmlns=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
            + "    <ns1:Description xmlns:ns1=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
            + "        xmlns=\"http://textgrid.info/namespaces/metadata/core/2010\"\n"
            + "        xmlns:ns0=\"http://textgrid.info/namespaces/metadata/core/2010\"\n"
            + "        xmlns:ns2=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n"
            + "        xmlns:ns3=\"http://textgrid.info/namespaces/metadata/core/2010\" ns2:about=\"textgrid:3rghf.1\">\n"
            + "        <ns2:height xmlns:ns2=\"http://www.w3.org/2003/12/exif/ns#\"\n"
            + "            ns2:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">644</ns2:height>\n"
            + "        <ns2:width xmlns:ns2=\"http://www.w3.org/2003/12/exif/ns#\"\n"
            + "            ns2:datatype=\"http://www.w3.org/2001/XMLSchema#integer\">864</ns2:width>\n"
            + "    </ns1:Description>\n"
            + "</RDF>";
    String expectedWarning =
        "Error validating relations RDF due to a org.apache.jena.riot.RiotException (EXIF information will be rewritten!): [line: 4, col: 129] {E201} The attributes on this property element, are not permitted with any content; expecting end element tag. in <?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\n"
            + "<RDF xmlns=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\">\n"
            + "    <ns1:Description ns2:about=\"textgrid:3rghf.1\" xmlns=\"http://textgrid.info/namespaces/metadata/core/2010\" xmlns:tg=\"http://textgrid.info/relation-ns#\" xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ns0=\"http://textgrid.info/namespaces/metadata/core/2010\" xmlns:ns2=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ns1=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\" xmlns:ns4=\"http://textgrid.info/namespaces/metadata/core/2010\" xmlns:ns3=\"http://textgrid.info/namespaces/metadata/core/2010\">\n"
            + "        <ns2:height ns2:datatype=\"http://www.w3.org/2001/XMLSchema#integer\" xmlns:ns2=\"http://www.w3.org/2003/12/exif/ns#\">644</ns2:height>\n"
            + "        <ns2:width ns2:datatype=\"http://www.w3.org/2001/XMLSchema#integer\" xmlns:ns2=\"http://www.w3.org/2003/12/exif/ns#\">864</ns2:width>\n"
            + "    </ns1:Description>\n"
            + "</RDF>";

    RdfType newRdf = JAXB.unmarshal(new StringReader(merkwuerdischerRdfContent), RdfType.class);
    metadata.getObject().getRelations().setRDF(newRdf);

    // Add UPDATEMETADATA to title.
    metadata.getObject().getGeneric().getProvided().getTitle().set(0,
        "UPDATEMETADATA: " + metadata.getObject().getGeneric().getProvided().getTitle().get(0));

    // Write temp metadata file.
    File tempMetadata = File.createTempFile("crud_", "_junit.meta.xml");
    tempMetadata.deleteOnExit();
    JAXB.marshal(metadata, tempMetadata);

    // Update metadata with invalid RDF.
    metadata = TGCrudClientUtils.updateMetadata(tgcrud, tgsearch, rbacSessionId, logParameter,
        tempMetadata.getAbsolutePath(), metadata.getObject().getGeneric().getGenerated());

    // Check for warnings in metadata.
    List<Warning> warnings = metadata.getObject().getGeneric().getGenerated().getWarning();
    assertFalse(warnings.size() != 1);
    assertTrue(warnings.get(0).getValue().trim().equals(expectedWarning.trim()));

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  /**
   * <p>
   * </p>
   *
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws IoFault
   * @throws AuthFault
   * @throws MetadataParseFault
   * @throws XMLStreamException
   * @throws ProtocolNotImplementedFault
   * @throws UpdateConflictFault
   * @throws RelationsExistFault
   * @throws NoSuchAlgorithmException
   */
  @Test(expected = ObjectNotFoundFault.class)
  public void testUpdateMetadataBug361() throws IOException, XMLStreamException,
      MetadataParseFault, AuthFault, IoFault, ObjectNotFoundFault, ProtocolNotImplementedFault,
      UpdateConflictFault, RelationsExistFault, NoSuchAlgorithmException {

    System.out.println("Testing TG-crud#UPDATEMETADATA");

    // Create a first TextGrid object, get metadata and URI.
    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            "bugs/#361/latest-crudbug.meta.xml", createWorkFilename, NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();

    // // Add complete (and invalid) relation content.
    // String merkwuerdischerRdfContent = """
    // <ns2:RDF>
    // <ns1:Description
    // xmlns:ns1="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    // xmlns:ns0="http://textgrid.info/namespaces/metadata/core/2010"
    // xmlns:ns3="http://textgrid.info/namespaces/metadata/core/2010"
    // ns1:about="">
    // <ns2:timeSlot
    // xmlns:ns2="https://textgridrep.org/terminology/eltec/">
    // T2</ns2:timeSlot>
    // <ns2:authorGender
    // xmlns:ns2="https://textgridrep.org/terminology/eltec/" />
    // <ns2:size
    // xmlns:ns2="https://textgridrep.org/terminology/eltec/">
    // None</ns2:size>
    // <ns2:reprintCount
    // xmlns:ns2="https://textgridrep.org/terminology/eltec/" />
    // </ns1:Description>
    // </ns2:RDF>
    // """;
    // String expectedWarning = """
    //
    // """;
    //
    // RdfType newRdf = JAXB.unmarshal(new StringReader(merkwuerdischerRdfContent), RdfType.class);
    // metadata.getObject().getRelations().setRDF(newRdf);
    //
    // // Add UPDATEMETADATA to title.
    // metadata.getObject().getGeneric().getProvided().getTitle().set(0,
    // "UPDATEMETADATA: " + metadata.getObject().getGeneric().getProvided().getTitle().get(0));
    //
    // // Write temp metadata file.
    // File tempMetadata = File.createTempFile("crud_", "_junit.meta.xml");
    // tempMetadata.deleteOnExit();
    // JAXB.marshal(metadata, tempMetadata);
    //
    // // Update metadata.
    // metadata = TGCrudClientUtils.updateMetadata(tgcrud, tgsearch, rbacSessionId, logParameter,
    // tempMetadata.getAbsolutePath(), metadata.getObject().getGeneric().getGenerated());
    //
    // // Check for warnings in metadata.
    // List<Warning> warnings = metadata.getObject().getGeneric().getGenerated().getWarning();
    // assertFalse(warnings.size() != 1);
    // assertTrue(warnings.get(0).getValue().trim().equals(expectedWarning.trim()));

    // Delete, and READ.
    if (DELETE) {
      TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, logParameter, uri);
      TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);
    }
  }

  // **
  // INTERNAL METHODS AND CLASSES
  // **

  /**
   * <p>
   * Just creates and reads a new revision.
   * </p>
   *
   * @param theUri
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  private static MetadataContainerType createRevisionReturnMetadata(String theUri)
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {

    MetadataContainerType metadata =
        TGCrudClientUtils.create(tgcrud, tgsearch, rbacSessionId, logParameter, projectId,
            createTestFileMetadataFilename, createTestFileFilename, theUri, NEW_REVISION,
            NO_WARNINGS);
    String uri = metadata.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
    TGCrudClientUtils.read(tgcrud, rbacSessionId, logParameter, uri);

    return metadata;
  }

  /**
   * <p>
   * Just creates and reads a new revision.
   * </p>
   *
   * @param theUri
   * @return
   * @throws FileNotFoundException
   * @throws IOException
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ObjectNotFoundFault
   * @throws AuthFault
   * @throws ProtocolNotImplementedFault
   * @throws NoSuchAlgorithmException
   */
  private static String createRevision(String theUri)
      throws FileNotFoundException, IOException, MetadataParseFault, IoFault, ObjectNotFoundFault,
      AuthFault, ProtocolNotImplementedFault, NoSuchAlgorithmException {
    return createRevisionReturnMetadata(theUri).getObject().getGeneric().getGenerated()
        .getTextgridUri().getValue();
  }

  /**
   * <p>
   * Reads TECHMD using TextGrid URI or PID.
   * </p>
   *
   * @param theClient
   * @param theID
   * @return
   * @throws IOException
   */
  private static boolean testTechmd(WebTarget theClient, String theID) throws IOException {

    boolean result = false;

    String techmd = TGCrudClientUtils.readTechmdREST(theClient, theID);

    if (techmd != null && !techmd.equals("")) {
      result = true;
    }

    return result;
  }

  /**
   * <p>
   * Starts a new TG-crud update client as a thread.
   * </p>
   */

  private class TGCrudUpdateTestThread extends Thread {

    protected TGCrudService tgcrudPC;
    protected SearchClient tgsearchPC;
    protected String sessionIdPC;
    protected String logParameterPC;
    protected String metadataFilenamePC;
    protected String dataFilenamePC;
    protected GeneratedType metadataPC;

    /**
     * @param theTgcrud
     * @param theTgsearch
     * @param theSessionId
     * @param theLogParameter
     * @param theMetadataFilename
     * @param theDataFilename
     * @param theMetadata
     */
    public TGCrudUpdateTestThread(TGCrudService theTgcrud, SearchClient theTgsearch,
        String theSessionId, String theLogParameter, String theMetadataFilename,
        String theDataFilename, MetadataContainerType theMetadata) {

      this.tgcrudPC = theTgcrud;
      this.tgsearchPC = theTgsearch;
      this.sessionIdPC = theSessionId;
      this.logParameterPC = theLogParameter;
      this.metadataFilenamePC = theMetadataFilename;
      this.dataFilenamePC = theDataFilename;
      this.metadataPC = theMetadata.getObject().getGeneric().getGenerated();
    }

    /**
     *
     */
    @Override
    public void run() {
      // Update file with large image file.
      try {
        TGCrudClientUtils.update(this.tgcrudPC, this.tgsearchPC, this.sessionIdPC,
            this.logParameterPC, this.metadataFilenamePC, this.dataFilenamePC, this.metadataPC,
            NO_WARNINGS);
      } catch (IOException | MetadataParseFault | ObjectNotFoundFault | IoFault | AuthFault e) {
        assertTrue(false);
      } catch (UpdateConflictFault e) {
        System.out.println("\tExpexted UpdateConflictFault!");
      }
    }

  }

  /**
   * <p>
   * Starts a new TG-crud create client as a thread.
   * </p>
   */

  private static class TGCrudCreateRevisionTestThread extends Thread {

    private static List<String> createdList = new ArrayList<String>();
    private static List<String> errorsList = new ArrayList<String>();
    private static List<String> expectedErrorsList = new ArrayList<String>();

    protected TGCrudService tgcrudPC;
    protected SearchClient tgsearchPC;
    protected String sessionIdPC;
    protected String logParameterPC;
    protected String metadataFilenamePC;
    protected String dataFilenamePC;
    protected GeneratedType metadataPC;

    /**
     * @param theTgcrud
     * @param theTgsearch
     * @param theSessionId
     * @param theLogParameter
     * @param theMetadataFilename
     * @param theDataFilename
     * @param theMetadata
     */
    public TGCrudCreateRevisionTestThread(TGCrudService theTgcrud, SearchClient theTgsearch,
        String theSessionId, String theLogParameter, String theMetadataFilename,
        String theDataFilename, MetadataContainerType theMetadata) {

      this.tgcrudPC = theTgcrud;
      this.tgsearchPC = theTgsearch;
      this.sessionIdPC = theSessionId;
      this.logParameterPC = theLogParameter;
      this.metadataFilenamePC = theMetadataFilename;
      this.dataFilenamePC = theDataFilename;
      this.metadataPC = theMetadata.getObject().getGeneric().getGenerated();
    }

    /**
     *
     */
    @Override
    public void run() {
      // Update file with large image file.
      String uri = "ERROR";
      try {
        MetadataContainerType m = TGCrudClientUtils.create(this.tgcrudPC, this.tgsearchPC,
            this.sessionIdPC, this.logParameterPC, this.metadataPC.getProject().getId(),
            this.metadataFilenamePC, this.dataFilenamePC,
            this.metadataPC.getTextgridUri().getValue(), NEW_REVISION, NO_WARNINGS);
        uri = m.getObject().getGeneric().getGenerated().getTextgridUri().getValue();
        TGCrudCreateRevisionTestThread.createdList.add(uri);
      } catch (IOException | MetadataParseFault | ObjectNotFoundFault | AuthFault e) {
        TGCrudCreateRevisionTestThread.errorsList.add(uri);
        assertTrue(false);
      } catch (IoFault e) {
        TGCrudCreateRevisionTestThread.expectedErrorsList.add(uri);
        System.out.println("\tExpexted new revision related IoFault!");
      }
    }

  }

}
