package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient.tgtest;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import org.apache.cxf.helpers.IOUtils;
import info.textgrid.clients.SearchClient;
import info.textgrid.namespaces.middleware.tgcrud.clients.crudclientutils.TGCrudClientUtils;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.RelationsExistFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class DeleteThings {

  public static final String PROPERTIES_FILE = "tgcrud.test.things.properties";

  private static String rbacSessionId;

  /**
   * @param args
   * @throws FileNotFoundException
   * @throws IOException
   */
  public static void main(String[] args)
      throws FileNotFoundException, IOException {

    TGCrudService tgcrud = TGCrudClientUtilities
        .getTgcrud("https://textgridlab.org/1.0/tgcrud-public/TGCrudService", true);
    SearchClient tgsearch = new SearchClient("https://textgridlab.org/1.0/tgsearch");
    // TGCrudService tgcrud = JAXRSClientFactory.create("https://textgrid-esx2.gwdg.de/1.0/tgcrud",
    // TGCrudService.class);

    // Load RBAC Session ID from properties file.
    Properties p = new Properties();
    p.load(new FileInputStream(TGCrudClientUtils.getResource(PROPERTIES_FILE)));
    rbacSessionId = p.getProperty("crud_sessionId");

    String urgs = IOUtils
        .readStringFromStream(new FileInputStream(new File("/Users/fugu/Desktop/idiomdelete.csv")));
    String[] splittedUrgs = urgs.split("\n");

    for (String uri : splittedUrgs) {
      System.out.println("deleting " + uri);

      // DELETE
      try {
        TGCrudClientUtils.delete(tgcrud, tgsearch, rbacSessionId, "", uri);
      } catch (ObjectNotFoundFault | RelationsExistFault | IoFault | AuthFault e) {

        System.out.println("ignoring " + e.getClass().getName() + ": " + e.getMessage());

      }
    }
  }

}
