package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient.tgtest;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import jakarta.activation.DataHandler;
import jakarta.xml.ws.Holder;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.clients.crudclientutils.TGCrudClientUtils;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class CreateThings {

  public static final String PROPERTIES_FILE = "tgcrud.test.things.properties";

  private static String rbacSessionId;

  /**
   * @param args
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ProtocolNotImplementedFault
   * @throws AuthFault
   * @throws UpdateConflictFault
   */
  public static void main(String[] args) throws IOException, ObjectNotFoundFault,
      MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, UpdateConflictFault {

    TGCrudService tgcrud = TGCrudClientUtilities
        .getTgcrud("https://textgridlab.org/1.0/tgcrud/TGCrudService", true);
    TGCrudService tgcrudPublic = TGCrudClientUtilities
        .getTgcrud("https://textgridlab.org/1.0/tgcrud-public/TGCrudService", true);
    String logParameter = "";
    String projectID = "TGPR-439779b8-db54-e35b-b8e5-590ae9bd74e9";
    String uri = "textgrid:34zmq";

    // Load RBAC Session ID from properties file.
    Properties p = new Properties();
    p.load(new FileInputStream(TGCrudClientUtils.getResource(PROPERTIES_FILE)));
    rbacSessionId = p.getProperty("crud_sessionId");

    // READ
    DataHandler data = TGCrudClientUtils.simpleRead(tgcrudPublic, rbacSessionId, logParameter, uri);

    // READMETADATA
    MetadataContainerType metadata =
        TGCrudClientUtils.readMetadata(tgcrudPublic, rbacSessionId, logParameter, uri);

    // CREATE (NEW REVISION)
    tgcrud.create(rbacSessionId, logParameter, uri, true, projectID,
        new Holder<MetadataContainerType>(metadata), data);
  }

}
