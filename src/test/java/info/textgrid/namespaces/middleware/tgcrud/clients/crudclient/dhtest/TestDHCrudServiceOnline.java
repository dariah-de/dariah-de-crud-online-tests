/**
 * This software is copyright (c) 2023 by
 * 
 * DARIAH-DE Consortium (https://de.dariah.eu)
 * 
 * This is free software. You can redistribute it and/or modify it under the terms described in the
 * GNU Lesser General Public License v3 of which you should have received a copy. Otherwise you can
 * download it from
 *
 * http://www.gnu.org/licenses/lgpl-3.0.txt
 *
 * @copyright DARIAH-DE Consortium (https://de.dariah.eu)
 * @license Lesser General Public License v3 (http://www.gnu.org/licenses/lgpl-3.0.txt GNU)
 * @author Stefan E. Funk (funk@sub.uni-goettingen.de)
 **/

package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient.dhtest;

import static org.junit.Assert.assertFalse;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Properties;
import org.apache.cxf.jaxrs.client.Client;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.cxf.jaxrs.client.WebClient;
import org.apache.cxf.transport.http.HTTPConduit;
import org.apache.cxf.transports.http.configuration.HTTPClientPolicy;
import org.apache.http.HttpHeaders;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import info.textgrid.namespaces.middleware.tgcrud.clients.crudclientutils.TGCrudClientUtils;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.DHCrudService;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.MultivaluedMap;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;
import jakarta.ws.rs.core.Response.StatusType;

/**
 * TODOLOG
 * 
 * TODO Implement tests for checking filename <--> mimetype!
 * 
 **
 * CHANGELOG
 *
 * 2023-02-16 - Funk - Add new test for new timestamps with timezone.
 *
 * 2021-06-29 - Funk - Add test for Tika mimetype extraction (HTTP responses). Collections now have
 * mimetype text/plain for HTTP delivery. Hope that is not a problem...
 * 
 * 2021-04-13 - Funk - Add deleted objects tests.
 * 
 * 2020-12-02 - Funk - Add properties file settings.
 *
 * 2020-06-18 - Funk - Add test for XML streaming, remove special header test.
 *
 * 2018-08-17 - Funk - Refactor testing methods. Checking mimetypes also now.
 *
 * 2014-10-31 - Funk - Copy first version from TestTGCrudServiceOnline.
 *
 **/

/**
 * <p>
 * This class is an online DH-crud function test class.
 * </p>
 * 
 * @author Stefan E. Funk, SUB Göttingen
 * @version 2023-11-10
 * @since 2018-04-27
 **/

// @Ignore
public class TestDHCrudServiceOnline {

  // **
  // STATIC FINALS
  // **

  // ** SET PROPERTIES FILE BELOW!
  //
  // private static final String propertiesFile = "dhcrud.test.repository-de-dariah-eu.properties";
  // private static final String propertiesFile = "dhcrud.test.properties";
  // private static final String propertiesFile = "dhcrud.test.trep-de-dariah-eu.properties";
  private static final String propertiesFile = "dhcrud.test.dhrepworkshop-de-dariah-eu.properties";
  //
  // ** SET PROPERTIES FILE ABOVE!

  private static final String LOGID = "DHCrudJunit_" + System.currentTimeMillis();
  private static final String MIMETYPE_NULL = null;
  private static final String MIMETYPE_ALL = "*/*";
  private static final String MIMETYPE_ZIP = "application/x-zip-compressed";

  private static final String METADATA = "/metadata";
  private static final String METADATA_TTL = "/metadata/ttl";
  private static final String METADATA_XML = "/metadata/xml";
  private static final String METADATA_JSON = "/metadata/json";
  private static final String METADATA_JSONLD = "/metadata/jsonld";
  private static final String METADATA_NTRIPLES = "/metadata/ntriples";

  private static final String ADMMD = "/adm";
  private static final String ADMMD_TTL = "/adm/ttl";
  private static final String ADMMD_XML = "/adm/xml";
  private static final String ADMMD_JSON = "/adm/json";
  private static final String ADMMD_JSONLD = "/adm/jsonld";
  private static final String ADMMD_NTRIPLES = "/adm/ntriples";

  private static final String PATH_VERSION = "version";
  private static final String PATH_DATA = "data";
  private static final String PATH_TECHMD = "tech";
  private static final String PATH_PROVMD = "prov";
  private static final String PATH_LANDING = "landing";
  private static final String PATH_BAG = "bag";
  private static final String PATH_BAGPACK = "bag/pack";
  private static final String PATH_ROOT = "";

  private static final String X_CLACKS_OVERHEAD = "X-Clacks-Overhead";

  // TODO Test offset? Is it implemented yet?
  // private static final long OFFSET = 0;
  private static final int dhcrudClientTimeout = 120000;

  private static boolean DO_CHECK_RESPONSE_HEADERS = true;
  private static boolean DO_NOT_CHECK_RESPONSE_HEADERS = false;

  // **
  // STATICS
  // **

  private static DHCrudService JAXRSClient;
  private static Client dhcrudWebClient;

  private static String dhcrudEndpoint;
  private static String doiPrefix;
  private static String hdlPrefix;
  private static String collectionSuffix;
  private static String bigSuffix;
  private static String bigSuffixExpMime;
  private static String xmlObjectSuffix;
  private static String xmlObjectSuffixExpMime;
  private static String pdfObjectSuffix;
  private static String pdfObjectSuffixExpMime;
  private static String bug25068Suffix;
  private static String bug25068SuffixExpMime;
  private static String bug32946Suffix;
  private static String bug32946SuffixExpMime;
  private static String bug35556Suffix;
  private static String bug35556SuffixExpMime;
  private static String deletedObject;
  private static String notExistingObjectSuffix;
  private static String newTimestampSuffix;
  private static String newTimestamp;

  // **
  // PREPARATIONS
  // **

  /**
   * @throws IOException
   * @throws FileNotFoundException
   * @throws Exception
   */
  @BeforeClass
  public static void setUpBeforeClass() throws FileNotFoundException, IOException {

    System.out.println("System's default charset: " + Charset.defaultCharset().displayName());

    // Load properties file.
    Properties p = new Properties();
    p.load(new FileInputStream(TGCrudClientUtils.getResource(propertiesFile)));

    System.out.println("Properties file: " + propertiesFile);

    // Get properties.
    dhcrudEndpoint = p.getProperty("DHCRUD_ENDPOINT");

    doiPrefix = p.getProperty("DOI_PREFIX");
    hdlPrefix = p.getProperty("HDL_PREFIX");
    collectionSuffix = p.getProperty("COLLECTION_SUFFIX");
    bigSuffix = p.getProperty("BIG_SUFFIX");
    bigSuffixExpMime = p.getProperty("BIG_SUFFIX_EXP_MIME");
    xmlObjectSuffix = p.getProperty("XML_OBJECT_SUFFIX");
    xmlObjectSuffixExpMime = p.getProperty("XML_OBJECT_SUFFIX_EXP_MIME");
    pdfObjectSuffix = p.getProperty("PDF_OBJECT_SUFFIX");
    pdfObjectSuffixExpMime = p.getProperty("PDF_OBJECT_SUFFIX_EXP_MIME");
    bug25068Suffix = p.getProperty("BUG25068_SUFFIX");
    bug25068SuffixExpMime = p.getProperty("BUG25068_SUFFIX_EXP_MIME");
    bug32946Suffix = p.getProperty("BUG32946_SUFFIX");
    bug32946SuffixExpMime = p.getProperty("BUG32946_SUFFIX_EXP_MIME");
    bug35556Suffix = p.getProperty("BUG35556_SUFFIX");
    bug35556SuffixExpMime = p.getProperty("BUG35556_SUFFIX_EXP_MIME");
    deletedObject = p.getProperty("DELETED_OBJECT");
    notExistingObjectSuffix = p.getProperty("FALSE_PID_CHECKSUM");
    newTimestampSuffix = p.getProperty("NEW_TIMESTAMP_SUFFIX");
    newTimestamp = p.getProperty("NEW_TIMESTAMP");

    // Get DH-crud REST endpoint and HTTP client.
    System.out.println("Getting DH-crud HTTP client...");

    // Get proxy first, set policy.
    JAXRSClient = JAXRSClientFactory.create(dhcrudEndpoint, DHCrudService.class);
    HTTPConduit conduit = WebClient.getConfig(JAXRSClient).getHttpConduit();
    HTTPClientPolicy policy = new HTTPClientPolicy();
    policy.setReceiveTimeout(dhcrudClientTimeout);
    conduit.setClient(policy);

    // Create Web Client from Web Proxy.
    dhcrudWebClient = WebClient.client(JAXRSClient);
  }

  /**
   * @throws Exception
   */
  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @Before
  public void setUp() throws Exception {
    //
  }

  /**
   * @throws Exception
   */
  @After
  public void tearDown() throws Exception {
    //
  }

  // **
  // TESTS
  // **

  /**
   * <p>
   * Testing REST call --> /version
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testGetVersion() {

    System.out.println("testGetVersion");

    WebClient w = WebClient.fromClient(dhcrudWebClient).path(PATH_VERSION);

    System.out.println("\tHTTP GET " + w.getCurrentURI());

    Response r = w.get();
    String version = r.readEntity(String.class);

    assertFalse(version == null || version.equals(""));

    System.out.println("\t" + version);
  }

  /**
   * <p>
   * Test a simple #CREATE and #UPDATE and #READ and #DELETE.
   * </p>
   * 
   */
  @Test
  @Ignore
  public void testDHCrudBasics() {

    // String name = "testDHCrudBasics()";

    // Do create.
    // create(name, doiURI, xmlObjectSuffixExpMime, tok);

    // Do read metadata.

    // Do read.

    // Do update.

    // Do read.

    // Do delete.

  }

  /**
   * <p>
   * Testing REST call --> /data
   * </p>
   */
  @Test
  public void testReadData() {

    String name = "testReadData()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, PATH_DATA, xmlObjectSuffixExpMime, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, PATH_DATA, xmlObjectSuffixExpMime, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /data
   * </p>
   */
  @Test
  public void testReadCollectionData() {

    String name = "testReadCollectionData()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + collectionSuffix);
    read(name, doiURI, PATH_DATA, MediaType.TEXT_PLAIN, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + collectionSuffix);
    read(name, hdlURI, PATH_DATA, MediaType.TEXT_PLAIN, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /metadata
   * </p>
   */
  @Test
  public void testReadMetadata() {

    String name = "testReadMetadata()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, METADATA, MediaType.TEXT_PLAIN, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, METADATA, MediaType.TEXT_PLAIN, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /metadata
   * </p>
   * <p>
   * <strong>NOTE</strong> Should work with all requests! Just testing this two here!
   * </p>
   * 
   */
  @Test
  public void testReadMetadataWithHDLAndDOIPrefixes() {

    String name = "testReadMetadataWithHDLAndDOIPrefixes()";

    // Test DOI.
    URI doiURI = URI.create("doi:" + doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, METADATA, MediaType.TEXT_PLAIN, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create("hdl:" + hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, METADATA, MediaType.TEXT_PLAIN, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /metadata/ttl
   * </p>
   */
  @Test
  public void testReadMetadataTTL() {

    String name = "testReadMetadataTTL()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, METADATA_TTL, MediaType.TEXT_PLAIN, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, METADATA_TTL, MediaType.TEXT_PLAIN, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /metadata with invalid PID syntax (missing "/"!)
   * </p>
   */
  @Test
  public void testReadMetadataInvalidPIDSyntax() {

    String name = "testReadMetadataInvalidPIDSyntax()";

    // NOTE: Here the regexp in the @pathParam is FALSE (not a valid PID syntax)! So we get no
    // dhcrud method calls, but the DHCrudServiceGenericExceptionMapper() is called instantly.
    // There is no mapping for a ClientErrorException in the error mapping, and we can't assume
    // every ClientErrorException is a 404! We have to live with the 500, I presume??

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + xmlObjectSuffix);
    read(name, doiURI, METADATA, MediaType.TEXT_PLAIN, Status.INTERNAL_SERVER_ERROR,
        DO_NOT_CHECK_RESPONSE_HEADERS, MIMETYPE_NULL);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + xmlObjectSuffix);
    read(name, hdlURI, METADATA, MediaType.TEXT_PLAIN, Status.INTERNAL_SERVER_ERROR,
        DO_NOT_CHECK_RESPONSE_HEADERS, MIMETYPE_NULL);
  }

  /**
   * <p>
   * Testing REST call --> /metadata with incorrect Handle checksum number.
   * </p>
   */
  @Test
  public void testReadMetadataNonExistingPID() {

    String name = "testReadMetadataNonExistingPID()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + notExistingObjectSuffix);
    read(name, doiURI, METADATA, MediaType.TEXT_PLAIN, Status.NOT_FOUND,
        DO_NOT_CHECK_RESPONSE_HEADERS, MIMETYPE_NULL);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + notExistingObjectSuffix);
    read(name, hdlURI, METADATA, MediaType.TEXT_PLAIN, Status.NOT_FOUND,
        DO_NOT_CHECK_RESPONSE_HEADERS, MIMETYPE_NULL);
  }

  /**
   * <p>
   * Testing REST call --> /metadata/xml
   * </p>
   */
  @Test
  public void testReadMetadataXML() {

    String name = "testReadMetadataXML()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, METADATA_XML, MediaType.TEXT_XML, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, METADATA_XML, MediaType.TEXT_XML, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /metadata/ntriples
   * </p>
   */
  @Test
  public void testReadMetadataNTRIPLES() {

    String name = "testReadMetadataNTRIPLES()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, METADATA_NTRIPLES, MediaType.TEXT_PLAIN, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, METADATA_NTRIPLES, MediaType.TEXT_PLAIN, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /metadata/json
   * </p>
   */
  @Test
  public void testReadMetadataJSON() {

    String name = "testReadMetadataJSON()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, METADATA_JSON, MediaType.APPLICATION_JSON, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, METADATA_JSON, MediaType.APPLICATION_JSON, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /metadata/jsonld U(using DOI)
   * </p>
   */
  @Test
  public void testReadMetadataJSONLD() {

    String name = "testReadMetadataJSONLD()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, METADATA_JSONLD, MediaType.APPLICATION_JSON, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, METADATA_JSONLD, MediaType.APPLICATION_JSON, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /adm
   * </p>
   */
  @Test
  public void testReadAdministrativeMetadata() {

    String name = "testReadAdministrativeMetadata()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, ADMMD, MediaType.TEXT_PLAIN, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, ADMMD, MediaType.TEXT_PLAIN, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /adm
   * </p>
   */
  @Test
  public void testReadAdministrativeMetadataNewTimestamp() {

    String name = "testReadAdministrativeMetadataNewTimestamp()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + newTimestampSuffix);
    read(name, doiURI, ADMMD, MediaType.TEXT_PLAIN, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + newTimestampSuffix);
    read(name, hdlURI, ADMMD, MediaType.TEXT_PLAIN, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /adm/ttl
   * </p>
   */
  @Test
  public void testReadAdministrativeMetadataTTL() {

    String name = "testReadAdministrativeMetadataTTL()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, ADMMD_TTL, MediaType.TEXT_PLAIN, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, ADMMD_TTL, MediaType.TEXT_PLAIN, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /adm/xml
   * </p>
   */
  @Test
  public void testReadAdministrativeMetadataXML() {

    String name = "testReadAdministrativeMetadataXML()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, ADMMD_XML, MediaType.TEXT_XML, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, ADMMD_XML, MediaType.TEXT_XML, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /adm/json
   * </p>
   */
  @Test
  public void testReadAdministrativeMetadataJSON() {

    String name = "testReadAdministrativeMetadataJSON()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, ADMMD_JSON, MediaType.APPLICATION_JSON, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, ADMMD_JSON, MediaType.APPLICATION_JSON, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /adm/jsonld
   * </p>
   */
  @Test
  public void testReadAdministrativeMetadataJSONLD() {

    String name = "testReadAdministrativeMetadataJSONLD()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, ADMMD_JSONLD, MediaType.APPLICATION_JSON, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, ADMMD_JSONLD, MediaType.APPLICATION_JSON, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /adm/ntriples
   * </p>
   */
  @Test
  public void testReadAdministrativeMetadataNTRIPLES() {

    String name = "testReadAdministrativeMetadataNTRIPLES()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, ADMMD_NTRIPLES, MediaType.TEXT_PLAIN, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, ADMMD_NTRIPLES, MediaType.TEXT_PLAIN, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /tech
   * </p>
   */
  @Test
  public void testReadTechnicalMetadata() {

    String name = "testReadTechnicalMetadata()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, PATH_TECHMD, MediaType.TEXT_XML, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, PATH_TECHMD, MediaType.TEXT_XML, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /prov
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testReadProvenanceMetadata() throws IOException {

    String name = "testReadProvenanceMetadata()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, PATH_PROVMD, MIMETYPE_NULL, Status.NOT_IMPLEMENTED,
        DO_NOT_CHECK_RESPONSE_HEADERS, MIMETYPE_NULL);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, PATH_PROVMD, MIMETYPE_NULL, Status.NOT_IMPLEMENTED,
        DO_NOT_CHECK_RESPONSE_HEADERS, MIMETYPE_NULL);
  }

  /**
   * <p>
   * Testing REST call --> /landing
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testReadLandingPage() throws IOException {

    String name = "testReadLandingPage()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, PATH_LANDING, MediaType.TEXT_HTML, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, PATH_LANDING, MediaType.TEXT_HTML, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /bag
   * </p>
   */
  @Test
  public void testReadBagSMALL() {

    String name = "testReadBagSMALL()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, PATH_BAG, MIMETYPE_ZIP, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, PATH_BAG, MIMETYPE_ZIP, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /bag
   * </p>
   */
  @Test
  public void testReadBagBIG() {

    String name = "testReadBagBIG()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + bigSuffix);
    read(name, doiURI, PATH_BAG, MIMETYPE_ZIP, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + bigSuffix);
    read(name, hdlURI, PATH_BAG, MIMETYPE_ZIP, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /bag/pack
   * </p>
   * 
   * @throws IOException
   */
  @Test
  public void testReadBagPackSMALL() throws IOException {

    String name = "testReadBagPackSMALL()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + xmlObjectSuffix);
    read(name, doiURI, PATH_BAGPACK, MIMETYPE_ZIP, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + xmlObjectSuffix);
    read(name, hdlURI, PATH_BAGPACK, MIMETYPE_ZIP, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> /bag/pack
   * </p>
   * 
   * @throws IOException
   */
  @Test
  @Ignore // for the time being...
  public void testReadBagPackBIG() throws IOException {

    String name = "testReadBagPackBIG()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + bigSuffix);
    read(name, doiURI, PATH_BAGPACK, MIMETYPE_ZIP, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + bigSuffix);
    read(name, hdlURI, PATH_BAGPACK, MIMETYPE_ZIP, Status.OK);
  }

  /**
   * <p>
   * Testing REST call --> / (Accept: NULL)
   * </p>
   */
  @Test
  public void testReadRootNULLAcceptHeader() {

    String name = "testReadRootNULLAcceptHeader()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + pdfObjectSuffix);
    read(name, doiURI, PATH_ROOT, pdfObjectSuffixExpMime, Status.OK, DO_CHECK_RESPONSE_HEADERS,
        MIMETYPE_NULL);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + pdfObjectSuffix);
    read(name, hdlURI, PATH_ROOT, pdfObjectSuffixExpMime, Status.OK, DO_CHECK_RESPONSE_HEADERS,
        MIMETYPE_NULL);
  }

  /**
   * <p>
   * Testing REST call --> / (Accept: ALL)
   * </p>
   */
  @Test
  public void testReadRootALLAcceptHeader() {

    String name = "testReadRootALLAcceptHeader()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + pdfObjectSuffix);
    read(name, doiURI, PATH_ROOT, pdfObjectSuffixExpMime, Status.OK, DO_CHECK_RESPONSE_HEADERS,
        MIMETYPE_ALL);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + pdfObjectSuffix);
    read(name, hdlURI, PATH_ROOT, pdfObjectSuffixExpMime, Status.OK, DO_CHECK_RESPONSE_HEADERS,
        MIMETYPE_ALL);
  }

  /**
   * <p>
   * Testing REST call --> / (Accept: HTML)
   * </p>
   */
  @Test
  public void testReadRootHTMLHeader() {

    String name = "testReadRootHTMLHeader()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + pdfObjectSuffix);
    read(name, doiURI, PATH_ROOT, MediaType.TEXT_HTML, Status.OK, DO_CHECK_RESPONSE_HEADERS,
        MediaType.TEXT_HTML);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + pdfObjectSuffix);
    read(name, hdlURI, PATH_ROOT, MediaType.TEXT_HTML, Status.OK, DO_CHECK_RESPONSE_HEADERS,
        MediaType.TEXT_HTML);
  }

  /**
   * <p>
   * Testing REST call --> / (Accept: ZIP)
   * </p>
   */
  @Test
  public void testReadRootZIPHeader() {

    String name = "testReadRootZIPHeader()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + pdfObjectSuffix);
    read(name, doiURI, PATH_ROOT, MIMETYPE_ZIP, Status.OK, DO_CHECK_RESPONSE_HEADERS, MIMETYPE_ZIP);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + pdfObjectSuffix);
    read(name, hdlURI, PATH_ROOT, MIMETYPE_ZIP, Status.OK, DO_CHECK_RESPONSE_HEADERS, MIMETYPE_ZIP);
  }

  /**
   * <p>
   * Testing REST call --> /data
   * </p>
   */
  @Test
  public void testReadDataBIG() {

    String name = "testReadDataBIG()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + bigSuffix);
    read(name, doiURI, PATH_DATA, bigSuffixExpMime, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + bigSuffix);
    read(name, hdlURI, PATH_DATA, bigSuffixExpMime, Status.OK);
  }

  /**
   * <p>
   * Testing BUG#25068 --> /data
   * 
   * </p>
   * 
   * @see https://projects.gwdg.de/projects/dariah-de-repository/work_packages/25068
   */
  @Test
  public void testBUG25068() {

    String name = "testBUG25068()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + bug25068Suffix);
    read(name, doiURI, PATH_DATA, bug25068SuffixExpMime, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + bug25068Suffix);
    read(name, hdlURI, PATH_DATA, bug25068SuffixExpMime, Status.OK);
  }

  /**
   * <p>
   * Testing BUG#35556 --> /data
   * 
   * </p>
   * 
   * @see https://projects.gwdg.de/projects/dariah-de-repository/work_packages/35556
   */
  @Test
  public void testBUG35556() {

    String name = "testBUG35556()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + bug35556Suffix);
    read(name, doiURI, PATH_DATA, bug35556SuffixExpMime, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + bug35556Suffix);
    read(name, hdlURI, PATH_DATA, bug35556SuffixExpMime, Status.OK);
  }

  /**
   * <p>
   * Testing BUG#32946 --> /data
   * 
   * </p>
   * 
   * @see https://projects.gwdg.de/projects/dariah-de-repository/work_packages/32946
   */
  @Test
  public void testBUG32946() {

    String name = "testBUG32946()";

    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + bug32946Suffix);
    read(name, doiURI, PATH_DATA, bug32946SuffixExpMime, Status.OK);

    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + bug32946Suffix);
    read(name, hdlURI, PATH_DATA, bug32946SuffixExpMime, Status.OK);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testDeletedCollectionCallsRoot() throws IOException {

    String name = "testDeletedCollectionCalls() --> root HTML";
    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + deletedObject);
    read(name, doiURI, PATH_ROOT, MediaType.TEXT_HTML, Status.GONE, DO_NOT_CHECK_RESPONSE_HEADERS,
        MediaType.TEXT_HTML);
    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    read(name, hdlURI, PATH_ROOT, MediaType.TEXT_HTML, Status.GONE, DO_NOT_CHECK_RESPONSE_HEADERS,
        MediaType.TEXT_HTML);

    name = "testDeletedCollectionCalls() --> root ZIP";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    read(name, doiURI, PATH_ROOT, MIMETYPE_NULL, Status.GONE, DO_NOT_CHECK_RESPONSE_HEADERS,
        MIMETYPE_ZIP);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    read(name, hdlURI, PATH_ROOT, MIMETYPE_NULL, Status.GONE, DO_NOT_CHECK_RESPONSE_HEADERS,
        MIMETYPE_ZIP);

    name = "testDeletedCollectionCalls() --> root COLLECTION";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    read(name, doiURI, PATH_ROOT, MIMETYPE_NULL, Status.GONE, DO_NOT_CHECK_RESPONSE_HEADERS,
        MediaType.TEXT_PLAIN);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    read(name, hdlURI, PATH_ROOT, MIMETYPE_NULL, Status.GONE, DO_NOT_CHECK_RESPONSE_HEADERS,
        MediaType.TEXT_PLAIN);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testDeletedCollectionCallsHTML() throws IOException {

    String name = "testDeletedCollectionCalls() --> landing";
    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, PATH_LANDING, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, PATH_LANDING, MIMETYPE_NULL, Status.GONE);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testDeletedCollectionCallsMetadata() throws IOException {

    String name = "testDeletedCollectionCalls() --> metadata";
    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, METADATA, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, METADATA, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> metadata/ttl";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, METADATA_TTL, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, METADATA_TTL, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> metadata/xml";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, METADATA_XML, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, METADATA_XML, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> metadata/json";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, METADATA_JSON, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, METADATA_JSON, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> metadata/jsonld";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, METADATA_JSONLD, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, METADATA_JSONLD, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> metadata/ntriples";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, METADATA_NTRIPLES, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, METADATA_NTRIPLES, MIMETYPE_NULL, Status.GONE);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testDeletedCollectionCallsADMMD() throws IOException {

    String name = "testDeletedCollectionCalls() --> adm";
    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, ADMMD, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, ADMMD, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> adm/ttl";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, ADMMD_TTL, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, ADMMD_TTL, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> adm/xml";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, ADMMD_XML, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, ADMMD_XML, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> adm/json";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, ADMMD_JSON, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, ADMMD_JSON, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> adm(jsonld";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, ADMMD_JSONLD, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, ADMMD_JSONLD, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> adm/ntriples";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, ADMMD_NTRIPLES, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, ADMMD_NTRIPLES, MIMETYPE_NULL, Status.GONE);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testDeletedCollectionCallsTECHMD() throws IOException {

    String name = "testDeletedCollectionCalls() --> tech";
    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, PATH_TECHMD, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, PATH_TECHMD, MIMETYPE_NULL, Status.GONE);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testDeletedCollectionCallsData() throws IOException {

    String name = "testDeletedCollectionCalls() --> data";
    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, PATH_DATA, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, PATH_DATA, MIMETYPE_NULL, Status.GONE);
  }

  /**
   * @throws IOException
   */
  @Test
  public void testDeletedCollectionCallsBags() throws IOException {

    String name = "testDeletedCollectionCalls() --> bag";
    // Test DOI.
    URI doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, PATH_BAG, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    URI hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, PATH_BAG, MIMETYPE_NULL, Status.GONE);

    name = "testDeletedCollectionCalls() --> bagpack";
    // Test DOI.
    doiURI = URI.create(doiPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, doiURI, PATH_BAGPACK, MIMETYPE_NULL, Status.GONE);
    // Test HDL.
    hdlURI = URI.create(hdlPrefix + "/" + deletedObject);
    readNoHeaderTesting(name, hdlURI, PATH_BAGPACK, MIMETYPE_NULL, Status.GONE);
  }

  // **
  // INTERNAL METHODS AND CLASSES
  // **

  /**
   * TODO Only possible for internal crud!
   * 
   * @param theName
   * @param theURI
   * @param theExpectedMimetype
   * @param theToken
   */
  private static void create(String theName, URI theURI, String theExpectedMimetype,
      String theToken) {

    System.out.println(theName);
    System.out.println("\tHTTP GET " + dhcrudEndpoint + theURI + "/data");

    // long start = System.currentTimeMillis();

    // Response r = dhcrud.create(metadata, data, pid, storageToken, seafileToken, logID);
    //
    // long end = System.currentTimeMillis();
    //
    // StatusType status = r.getStatusInfo();
    // String statusString = status.getStatusCode() + " " + status.getReasonPhrase();
    //
    // System.out.println("\t" + statusString + " (request took " + (end - start) + " millis)");
    //
    // if (status.getStatusCode() != Status.OK.getStatusCode()) {
    // assertTrue(statusString, false);
    // }
    //
    // testGeneralHTTPHeaders(r.getHeaders(), theExpectedMimetype);

  }

  /**
   * @param theName
   * @param theURI
   * @param thePath
   * @param theExpectedMimetype
   * @param theExpectedStatus
   */
  private static void read(String theName, URI theURI, String thePath, String theExpectedMimetype,
      Status theExpectedStatus) {
    read(theName, theURI, thePath, theExpectedMimetype, theExpectedStatus, true, null);
  }

  /**
   * @param theName
   * @param theURI
   * @param thePath
   * @param theExpectedMimetype
   * @param theExpectedStatus
   */
  private static void readNoHeaderTesting(String theName, URI theURI, String thePath,
      String theExpectedMimetype, Status theExpectedStatus) {
    read(theName, theURI, thePath, theExpectedMimetype, theExpectedStatus, false, null);
  }

  /**
   * @param theName
   * @param theURI
   * @param thePath
   * @param theExpectedMimetype
   * @param theExpectedStatus
   * @param doTestHeaders
   * @param theAcceptHeaderValue
   */
  private static void read(String theName, URI theURI, String thePath, String theExpectedMimetype,
      Status theExpectedStatus, boolean doTestHeaders, String theAcceptHeaderValue) {

    System.out.println(theName + "  -->  " + theURI);

    WebClient w = WebClient.fromClient(dhcrudWebClient).path(theURI + "/" + thePath)
        .header(DHCrudService.TRANSACTION_ID, LOGID);

    if (theAcceptHeaderValue != null && !theAcceptHeaderValue.isEmpty()) {
      w.header("Accept", theAcceptHeaderValue);
    }

    System.out.println("\tHTTP HEADERS: " + w.getHeaders());

    System.out.println("\tHTTP GET " + w.getCurrentURI());

    Response r = w.get();

    StatusType status = r.getStatusInfo();
    String statusString = status.getStatusCode() + " " + status.getReasonPhrase();

    System.out.println("\t" + statusString);

    assertFalse(status.getStatusCode() + " != " + theExpectedStatus.getStatusCode() + " (expected)",
        status.getStatusCode() != theExpectedStatus.getStatusCode());

    if (doTestHeaders) {
      testGeneralHTTPHeaders(r.getHeaders(), theExpectedMimetype);
    }

    // Check for correct new timestamp, if new timestamp is tested!
    if (thePath.equals(ADMMD) && theURI.toString().contains(newTimestampSuffix)) {
      String admMD = r.readEntity(String.class);
      assertFalse("missing " + newTimestamp + " in ADMMD!", !admMD.contains(newTimestamp));
      System.out.println("\tTimestamp OK (" + newTimestamp + ")");
    }
  }

  /**
   * @param theHeaders
   * @param theExpectedContentType
   */
  private static void testGeneralHTTPHeaders(MultivaluedMap<String, Object> theHeaders,
      String theExpectedContentType) {

    System.out.println("\tAll HTTP headers: " + theHeaders);

    Object contentTypeHeader = theHeaders.get(HttpHeaders.CONTENT_TYPE);
    boolean contentType = contentTypeHeader != null && !contentTypeHeader.toString().isEmpty();
    assertFalse(HttpHeaders.CONTENT_TYPE + " missing or empty: " + theHeaders, !contentType);
    if (contentTypeHeader != null) {
      System.out.println("\t" + HttpHeaders.CONTENT_TYPE + ": " + contentTypeHeader.toString());
    }

    if (theExpectedContentType != null && contentTypeHeader != null) {
      String type = contentTypeHeader.toString();
      assertFalse(type + " is NOT the expected one: " + theExpectedContentType,
          theExpectedContentType.equals(type));
    }

    Object contentLengthHeader = theHeaders.get(HttpHeaders.CONTENT_LENGTH);
    boolean contentLength =
        contentLengthHeader != null && !contentLengthHeader.toString().isEmpty();
    assertFalse(HttpHeaders.CONTENT_LENGTH + " missing or empty: " + theHeaders, !contentLength);
    if (contentLengthHeader != null) {
      System.out.println("\t" + HttpHeaders.CONTENT_LENGTH + ": " + contentLengthHeader.toString());
    }

    Object lastModifiedHeader = theHeaders.get(HttpHeaders.LAST_MODIFIED);
    boolean lastModified = lastModifiedHeader != null && !lastModifiedHeader.toString().isEmpty();
    assertFalse(HttpHeaders.LAST_MODIFIED + " missing or empty: " + theHeaders, !lastModified);
    if (lastModifiedHeader != null) {
      System.out.println("\t" + HttpHeaders.LAST_MODIFIED + ": " + lastModifiedHeader.toString());
    }

    Object xclacksHeader = theHeaders.get(X_CLACKS_OVERHEAD);
    boolean xclacks = xclacksHeader != null && !xclacksHeader.toString().isEmpty();
    assertFalse(X_CLACKS_OVERHEAD + " missing or empty: " + theHeaders, !xclacks);
    if (xclacksHeader != null) {
      System.out.println("\t" + X_CLACKS_OVERHEAD + ": " + xclacksHeader.toString());
    }

    System.out.println("\tHTTP headers OK");
  }

}
