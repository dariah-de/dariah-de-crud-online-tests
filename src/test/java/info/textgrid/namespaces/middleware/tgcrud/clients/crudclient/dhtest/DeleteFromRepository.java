package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient.dhtest;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.zip.Adler32;
import javax.xml.stream.XMLStreamException;
import org.apache.cxf.helpers.IOUtils;
import org.apache.cxf.jaxrs.client.JAXRSClientFactory;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.riot.lang.extra.javacc.ParseException;
import info.textgrid.middleware.common.LTPUtils;
import info.textgrid.middleware.common.RDFConstants;
import info.textgrid.middleware.common.RDFUtils;
import info.textgrid.middleware.common.TextGridMimetypes;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.DHCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.core.Response.Status;

/**
 * <p>
 * This class prepares deleting from the Repository. Use in emergency cases only! Belongs to the
 * DARIAH-DE Repository delete workflow!
 * </p>
 * 
 * @author Stefan E Funk (SUB Göttingen)
 */

public class DeleteFromRepository {

  private static final String OK = "ok";
  private static DHCrudService dhcrudClient;
  private static Adler32 adler = new Adler32();
  private static Random rn = new Random();

  /**
   * @param args
   * @throws IoFault
   * @throws IOException
   * @throws XMLStreamException
   * @throws ParseException
   */
  public static void main(String[] args)
      throws IoFault, IOException, XMLStreamException, ParseException {

    // ERRORNEOUS OBJECT. REPOSITORY.
    // URL crudEndpoint = new URL(
    // "https://repository.de.dariah.eu/1.0/dhcrud/");
    // URI collectionURI = URI.create("21.11113/0000-000B-C8EC-A");

    // PUBLISHED OBJECT. REPOSITORY.
    // URL crudEndpoint = new URL(
    // "https://repository.de.dariah.eu/1.0/dhcrud/");
    // URI collectionURI = URI.create("21.T11991/0000-0005-E223-4");

    // PUBLISHED OBJECT. TREP.
    URL crudEndpoint = new URL("https://trep.de.dariah.eu/1.0/dhcrud/");
    URI collectionURI = URI.create("21.T11991/0000-0006-0FEE-F");

    String logID = "ADMIN_DELETE_" + createRandomLogId();

    System.out.println("crud:  " + crudEndpoint);
    System.out.println("pid:   " + collectionURI);
    System.out.println("logid: " + logID);

    // Get crud client.
    dhcrudClient = JAXRSClientFactory.create(crudEndpoint.toString(), DHCrudService.class);

    // Retrieve collection object.
    Response response = fetchCollectionObject(collectionURI, logID);

    // Check mimetype of root collection.
    System.out.print("checking if collection... ");
    String mimetype = response.getMediaType().toString();
    if (!mimetype.equals(TextGridMimetypes.DARIAH_COLLECTION)) {
      System.out.println("NO COLLECTION");
    }

    System.out.print("[" + mimetype + "]... ");

    // Create new PID list, add root PID to beginning of list.
    List<String> pids = new ArrayList<String>();
    pids.add(0, collectionURI.toString());

    System.out.println("added... " + OK);

    // Get collection objects, add to list.
    pids.addAll(getCollectionObjects(response, collectionURI, logID));

    System.out.println("got " + pids.size() + " objects!");

  }

  /**
   * @param theDataResponse
   * @param theURI
   * @param theLogID
   * @return
   * @throws XMLStreamException
   * @throws IOException
   * @throws ParseException
   */
  private static List<String> getCollectionObjects(Response theDataResponse, URI theURI,
      String theLogID) throws XMLStreamException, IOException, ParseException {

    List<String> result = new ArrayList<String>();

    Model model = RDFUtils.readModel(
        IOUtils.readStringFromStream((InputStream) theDataResponse.getEntity()),
        RDFConstants.TURTLE);
    result = RDFUtils.getProperties(model, LTPUtils.resolveHandlePid(theURI),
        RDFConstants.DCTERMS_PREFIX, RDFConstants.ELEM_DCTERMS_HASPART);

    // Do Loop.
    List<String> sublist = new ArrayList<String>();
    for (String pid : result) {
      URI p = URI.create(getPIDFromURL(pid));
      // Retrieve mimetype.
      String pidmime = fetchAdmObject(p, theLogID);
      sublist.add(p.toString());
      System.out.println(" added... " + OK);
      // Check for collection.
      if (pidmime.equals(TextGridMimetypes.DARIAH_COLLECTION)) {
        // Get new data response for reading subcollections.
        Response dataResponse = fetchCollectionObject(p, theLogID);
        sublist.addAll(getCollectionObjects(dataResponse, p, theLogID));
      }
    }
    result.addAll(sublist);

    return result;
  }

  /**
   * @param theURI
   * @param theLogID
   * @return
   */
  private static Response fetchCollectionObject(URI theURI, String theLogID) {

    System.out.print("fetching object... ");

    Response result = dhcrudClient.read(theURI, 0l, theLogID);

    if (result.getStatus() != Status.OK.getStatusCode()) {
      System.out.println(result.getStatus() + " " + result.getStatusInfo().getReasonPhrase());
      System.exit(1);
    }

    System.out.println(OK);

    return result;
  }

  /**
   * @param theURI
   * @param theLogID
   * @return
   */
  private static String fetchAdmObject(URI theURI, String theLogID) {

    String result = "";

    System.out.print("fetching adm_md for " + theURI + "... ");

    Response response = dhcrudClient.readAdmMD(theURI, theLogID);
    if (response.getStatus() != Status.OK.getStatusCode()) {
      System.out.println(response.getStatus() + " " + response.getStatusInfo().getReasonPhrase());
      System.exit(3);
    }
    result = response.getMediaType().toString();

    System.out.print("[" + result + "]...");

    return result;
  }

  /**
   * @param theURL
   * @return
   */
  private static String getPIDFromURL(String theURL) {
    return theURL.substring(theURL.lastIndexOf("/", theURL.lastIndexOf("/") - 1) + 1);
  }

  /**
   * <p>
   * Used for generating IDs for log messages.
   * </p>
   * 
   * @return
   */
  private static String createRandomLogId() {

    adler.update(rn.nextInt());

    return String.valueOf(adler.getValue());
  }

}
