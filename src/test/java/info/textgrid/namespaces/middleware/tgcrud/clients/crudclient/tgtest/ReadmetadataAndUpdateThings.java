package info.textgrid.namespaces.middleware.tgcrud.clients.crudclient.tgtest;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import jakarta.xml.bind.JAXB;
import info.textgrid.clients.SearchClient;
import info.textgrid.namespaces.metadata.core._2010.MetadataContainerType;
import info.textgrid.namespaces.middleware.tgcrud.clients.crudclientutils.TGCrudClientUtils;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.AuthFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.IoFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.MetadataParseFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ObjectNotFoundFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.ProtocolNotImplementedFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.TGCrudService;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.UpdateConflictFault;
import info.textgrid.namespaces.middleware.tgcrud.services.tgcrudservice.tgcrudclient.TGCrudClientUtilities;

/**
 * @author Stefan E. Funk, SUB Göttingen
 */
public class ReadmetadataAndUpdateThings {

  public static final String PROPERTIES_FILE = "tgcrud.test.things.properties";

  private static String rbacSessionId;

  /**
   * @param args
   * @throws IOException
   * @throws ObjectNotFoundFault
   * @throws MetadataParseFault
   * @throws IoFault
   * @throws ProtocolNotImplementedFault
   * @throws AuthFault
   * @throws UpdateConflictFault
   */
  public static void main(String[] args) throws IOException, ObjectNotFoundFault,
      MetadataParseFault, IoFault, ProtocolNotImplementedFault, AuthFault, UpdateConflictFault {

    TGCrudService tgcrud = TGCrudClientUtilities
        .getTgcrud("https://textgridlab.org/1.0/tgcrud-public/TGCrudService", true);
    SearchClient tgsearch = new SearchClient("https://textgridlab.org/1.0/tgsearch");
    String logParameter = "";
    String uri = "";
    String filename = "/Users/fugu/Desktop/Veronica II/Import NEU/o--Yd7820/Bl.32r.21zdh.0.xml";

    // Load RBAC Session ID from properties file.
    Properties p = new Properties();
    p.load(new FileInputStream(TGCrudClientUtils.getResource(PROPERTIES_FILE)));
    rbacSessionId = p.getProperty("crud_sessionId");

    // READMETADATA
    MetadataContainerType metadata =
        TGCrudClientUtils.readMetadata(tgcrud, rbacSessionId, logParameter, uri);

    // CHANGE JPEG TO XML
    metadata.getObject().getGeneric().getProvided().setFormat("text/xml");
    JAXB.marshal(metadata, System.out);

    // MARSHAL METADATA FILE
    File metadataFile = File.createTempFile("veronica_", ".meta.xml");
    JAXB.marshal(metadata, metadataFile);

    // UPDATE
    TGCrudClientUtils.update(tgcrud, tgsearch, rbacSessionId, logParameter,
        metadataFile.getAbsolutePath(), filename, metadata.getObject().getGeneric().getGenerated(),
        0);
  }

}
