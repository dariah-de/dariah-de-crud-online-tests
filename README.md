# README

## Preparations

To test a TG or DH-crud service, you would need a valid properties file in `/src/test/resources/` (see examples) and add session ID and other secrets, and put it's name
into the test classes.

Then un-ignore the test annotation in `TestTGCrudServiceOnline` or `TestDHCrudServiceOnline` classes.

After that just call

    mvn test

## Badges

[![Dependency Track Status](https://deps.sub.uni-goettingen.de/api/v1/badge/vulns/project/dariah-de-crud-online-tests/develop)](https://deps.sub.uni-goettingen.de/projects/db05dd3a-0bd4-4604-afbb-9db13594c949)
