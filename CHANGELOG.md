## [0.0.3](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/compare/v0.0.2...v0.0.3) (2024-06-05)


### Bug Fixes

* add some aggregation data fixes ([22f952f](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/22f952fd333d2a3123e957a4d7f06e562a7f18ea))
* add tests for new metadata validation warnings to metadata warnings ([81e0615](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/81e06153c54f68cecf1a6209e2e461663e7fee85))
* add tgcrud fixes for [#340](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/issues/340) ([b7bf2fa](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/b7bf2fac9b3b3a761dadd23677f51a6f21642b92))
* fix some warnings, comment in new tests ([2742870](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/2742870a64569553a2fbc161d92324bb0255b9bb))
* mc ([d918343](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/d918343d40d5b637de863b711e92b5da31a8e65f))
* new tests added for jaxb validation and metadata ([00079c6](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/00079c6e6c87a34fd47d72dd2565af38ae8f1f0b))
* refine some aggregation tests ([3d7ca71](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/3d7ca716e8414a2a3241313b15eafec621653dfd))
* remove rdfdb pw and username ([947fb5d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/947fb5d543aa6b681951dabcddd7a1d69384b3be))

## [0.0.2](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/compare/v0.0.1...v0.0.2) (2024-03-06)


### Bug Fixes

* increase  commons codes version ([88d106d](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/88d106dda47cc9578e99b34bbce4b9d75ad6b134))
* new version..... for first release ([a15ccf9](https://gitlab.gwdg.de/dariah-de/dariah-de-crud-online-tests/commit/a15ccf952db78953364a8d5d38e17dc3224b6ccc))

# 2024.03.06

first version from dariah-de crud code
